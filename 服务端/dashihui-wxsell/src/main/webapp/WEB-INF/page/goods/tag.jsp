<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>${tag.tagName}</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_goods_index.css" />
	</head>
	<body>
	    <!-- 商品列表 -->
	    <div class="app-part app-part-flow">
			<div id="goodsList" class="am-g app-goods-list"></div>
		</div>
		
		<!-- 购物车+1的动画效果，需要app-navbar设置z-index小于本控件 -->
		<span id="cartNumAnimate" style="color:red;font-weight:bold;font-size:18px;position:fixed;left:7%;bottom:50px;z-index:2;display:none;">+1</span>
		
		<div class="app-cart" >
			<span id="cartNum" class="am-badge am-round" style="display:none;"></span>
			<a href="${BASE_PATH }/cart"><img src="${BASE_PATH}/static/app/img/breakfast_bottom_left.png" style="width:50px;height:50px;"/></a>
		</div>
		
		<div data-am-widget="gotop" class="am-gotop am-gotop-fixed" >
			<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
		</div>
				
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as goods}}
			<div class="am-u-sm-6 am-u-md-4 am-u-lg-3 app-goods-item">
            	<dl>
            		<dt><a href="${BASE_PATH}/goods/detail/{{goods.id}}"><img data-original="${FTP_PATH}{{goods.thumb}}" class="am-center lazyload" width="100%"></a></dt>
            		<dd class="app-item-title">{{goods.name}}</dd>
            		<dd>
            			<span class="app-item-price-normal"><i class="am-icon-rmb"></i> {{goods.sellPrice}}</span>
            			<span class="app-item-price-del"><i class="am-icon-rmb"></i> {{goods.marketPrice}}</span>
						<span>
							<a onclick="javascript:addCart('{{goods.id}}','{{goods.isSelf}}','{{goods.name}}','{{goods.spec}}','{{goods.thumb}}','{{goods.sellPrice}}','{{goods.marketPrice}}');">
								<img src="${BASE_PATH }/static/app/img/add_car.png" style="width:30px;heigt:30px;padding-top:6px;"/>
							</a>
						</span>
            		</dd>
            	</dl>
            </div>
			{{/each}}
		</script>
		<script type="text/javascript">
			var pageNum = 0, totalPage = 1;
			var loading = false;
			
			$(function(){
				Kit.util.onPageEnd(function(){
	               	if(pageNum<totalPage&&!loading){
	               		loading = true;
	               		$("#goodsList").append("<div class=\"app-loading\">正在加载</div>");
	                	Kit.ajax.post("${BASE_PATH}/goods/pageByTag",{pageNum:pageNum+1,pageSize:10,code:'${tag.code}'},function(result){
							$("#goodsList").append(template("dataTpl",result.object));
							//amazeui要求在最后一个元素上添加am-u-end样式，否则默认为右浮动
							$(".app-goods-item","#goodsList").removeClass("am-u-end").last().addClass("am-u-end");
							//图片延迟加载
							$("img.lazyload","#goodsList").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							$(".app-loading","#goodsList").remove();
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
							//重置加载标识
							loading = false;
						});
	               	}
				});
				//查询购物车状态
				Kit.ajax.post("${BASE_PATH}/cart/state",{},function(result){
					if(result.object.CART_COUNTER!=0)
						$("#cartNum").text(result.object.CART_COUNTER).show();
				});
			});
			function addCart(id,isSelf,name,spec,thumb,sellPrice,marketPrice){
				var goods = {
					"id":id,
					"isSelf":isSelf,
					"name":name,
					"spec":spec,
					"thumb":thumb,
					"sellPrice":sellPrice,
					"marketPrice":marketPrice
				};
				Kit.ajax.post("${BASE_PATH}/cart/add",goods,function(result){
					$("#cartNum").show().text(result.object.counter);
					$("#cartNumAnimate").show().animate({bottom:70,opacity:0},500,"linear",function(){
						$("#cartNumAnimate").css({"bottom":50+"px","opacity":150}).hide();
					});
				});
			}
		</script>
	</body>
</html>