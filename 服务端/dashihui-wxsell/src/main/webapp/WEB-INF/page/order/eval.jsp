<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>评价</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_order_eval.css" />
	</head>
	<body>
		<header class="am-header app-header">
			<div class="am-header-left am-header-nav">
          		<a href="javascript:history.go(-1);" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title app-title">评价</h1>
		</header>
		
		<div class="app-eval-wrapper">
			<div class="am-g app-m-t-20 app-eval-item">
				<span class="am-u-sm-5 am-u-md-5 am-u-lg-5">商品满意度</span>
				<ul id="eval1" class="am-u-sm-7 am-u-md-7 am-u-lg-7 am-avg-sm-5">
					<li><a href="javascript:doEval(1,1)" class="app-star star star1 star2 star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></li>
					<li><a href="javascript:doEval(1,2)" class="app-star star star2 star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(1,3)" class="app-star star star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(1,4)" class="app-star star star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(1,5)" class="app-star star star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
				</ul>
				<input type="hidden" id="inptEval1" value="5">
			</div>
			<div class="am-g app-m-t-20 app-eval-item">
				<span class="am-u-sm-5 am-u-md-5 am-u-lg-5">配送速度满意度</span>
				<ul id="eval2" class="am-u-sm-7 am-u-md-7 am-u-lg-7 am-avg-sm-5">
					<li><a href="javascript:doEval(2,1)" class="app-star star star1 star2 star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(2,2)" class="app-star star star2 star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(2,3)" class="app-star star star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(2,4)" class="app-star star star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(2,5)" class="app-star star star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
				</ul>
				<input type="hidden" id="inptEval2" value="5">
			</div>
			<div class="am-g app-m-t-20 app-eval-item">
				<span class="am-u-sm-5 am-u-md-5 am-u-lg-5">服务质量满意度</span>
				<ul id="eval3" class="am-u-sm-7 am-u-md-7 am-u-lg-7 am-avg-sm-5">
					<li><a href="javascript:doEval(3,1)" class="app-star star star1 star2 star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(3,2)" class="app-star star star2 star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(3,3)" class="app-star star star3 star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(3,4)" class="app-star star star4 star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
					<li><a href="javascript:doEval(3,5)" class="app-star star star5 active"><img class="normal" src="${BASE_PATH}/static/app/img/star.png" width="100%"><img class="active" src="${BASE_PATH}/static/app/img/star_active.png" width="100%"></a></a></li>
				</ul>
				<input type="hidden" id="inptEval3" value="5">
			</div>
			<div class="am-cf"></div>
			<hr class="app-m-t-15"/>
			<textarea id="textContent" class="app-form-textarea app-m-t-15" maxlength="500" placeholder="您的评价对我们很重要，请输入您对本次购物的评价，长度在1-500字~"></textarea>
		</div>
		
		<div class="am-navbar am-cf am-no-layout">
			<a href="javascript:submitEval();" class="am-btn app-btn-bottom app-btn-red">提交评价</a>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
		</script>
		<script type="text/javascript">
			function doEval(t,n){
				$(".star","#eval"+t).removeClass("active");
				$(".star"+n,"#eval"+t).addClass("active");
				$("#inptEval"+t).val(n);
			}
			function submitEval(){
				var eval1 = $("#inptEval1").val(),eval2 = $("#inptEval2").val(),eval3 = $("#inptEval3").val(),content = $("#textContent").val();
				if(Kit.validate.isBlank(content)){
					Kit.ui.toast("您还未对本次购物进行评价！");
					return;
				}
				Kit.ajax.post("${BASE_PATH}/order/doEval",{orderNum:'${order.orderNum}',eval1:eval1,eval2:eval2,eval3:eval3,content:content},function(result){
					if(result.flag==0){
						Kit.ui.toast("评价成功，感谢您的评价，我们会继续加油~");
						Kit.render.redirect("${BASE_PATH}/order/detail/${order.orderNum}");
					}else{
						Kit.ui.alert(result.message);
					}
				});
			}
		</script>
	</body>
</html>