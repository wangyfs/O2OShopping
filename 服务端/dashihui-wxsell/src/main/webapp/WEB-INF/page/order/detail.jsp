<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>订单详情</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_order_detail.css" />
	</head>
	<body class="app-navbar-body">
		<header class="am-header app-header">
			<div class="am-header-left am-header-nav">
          		<a href="javascript:history.go(-1);" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title app-title">订单详情</h1>
		</header>
		
		<div id="panel" class="app-m-b-10"></div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<script type="text/javascript">
			var order;
			$(function(){
				//初始化显示订单信息
				Kit.ajax.post("${BASE_PATH}/order/getDetail",{orderNum:'${orderNum}'},function(result){
					if(result.flag==0){
						//渲染订单信息
						render(result.object);
						
						//判断如果订单正常、在线支付、未付款时，启动倒计时
						if(result.object.orderState==<%=OrderCode.OrderState.NORMAL%>
							&&result.object.payType==<%=OrderCode.OrderPayType.ON_LINE%>
							&&result.object.payState==<%=OrderCode.OrderPayState.NO_PAY%>){
							timer(parseInt(result.object.remain/1000));
						}
					}else{
						Kit.ui.toast(result.message);
						setTimeout(function(){
							window.history.go(-1);
						},2000);
					}
				});
			});
			function render(obj){
				order = obj;
				$("#panel").empty().append(template("dataTpl",order));
				//图片延迟加载
				$("img.lazyload").not(".lazyload-binded").lazyload({
					failurelimit : 100,
					effect : "show"
				}).addClass("lazyload-binded");
			}
			function timer(remain){
				var min = parseInt((remain-1)/60), sec = (remain-1)%60;
				$(".remain_min","#panel").text(min);
				$(".remain_sec","#panel").text(sec);
				if(min>0||sec>0){
					//设置下一秒再执行
					setTimeout(function(){
						timer(remain-1);
					}, 1000);
				}else{
					//倒计时结束，刷新页面
					Kit.render.refresh();
				}
			}
			function toCancel(){
				Kit.ui.confirm("确定要取消订单？",function(){
					//弹出备注编辑弹出层
					/* var popup = '<div class="desc-dialog">'
							+ '<form class="am-form">'
							+ '<div class="am-form-group">'
							+ '<textarea id="reasonInpt" rows="4" placeholder="请输入取消理由，仅限100字以内" maxlength="100"></textarea>'
							+ '</div>'
							+ '<a href="javascript:doCancel();" class="am-btn am-btn-danger am-btn-block">确定</a>'
							+ '</form>'
							+ '</div>';
					Kit.ui.popup(popup); */
					doCancel();
				});
			}
			function doCancel(){
				var tip = "取消订单后，您的优惠也将一并取消，是否仍继续？";
				if(order.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&order.payState==<%=OrderCode.OrderPayState.HAD_PAY%>){
					tip = "取消订单后，您的退款进度请进入<br/>“我的-我的订单-退款”查看";
				}
				Kit.ui.confirm(tip,function(){
					var reason = $("#reasonInpt").val();
					Kit.ui.popdown();
					Kit.ajax.post("${BASE_PATH}/order/cancel",{orderNum:'${orderNum}',reason:reason},function(result){
						if(result.flag==0){
							Kit.ui.toast("订单取消成功！");
							//重新渲染订单信息
							render(result.object);
						}else{
							Kit.ui.alert(result.message);
						}
					});
				});
			}
			function doDelete(){
				Kit.ui.confirm("确定要删除订单？",function(){
					Kit.ajax.post("${BASE_PATH}/order/delete",{orderNum:'${orderNum}'},function(result){
						if(result.flag==0){
							Kit.ui.toast("订单删除成功！");
							setTimeout(function(){
								Kit.render.redirect("${BASE_PATH}/order/list");
							},2000);
						}else{
							Kit.ui.alert(result.message);
						}
					});
				});
			}
			function doReceive(){
				Kit.ui.confirm("确定签收订单？",function(){
					Kit.ajax.post("${BASE_PATH}/order/receive",{orderNum:'${orderNum}'},function(result){
						if(result.flag==0){
							Kit.render.redirect("${BASE_PATH}/order/tradeComplete/${orderNum}");
						}else{
							Kit.ui.alert(result.message);
						}
					});
				});
			}
			function doUrge(){
				Kit.ajax.post("${BASE_PATH}/order/urge",{orderNum:'${orderNum}'},function(result){
					if(result.flag==0){
						Kit.ui.toast("催单成功，您的订单将很快安排配送");
					}else{
						Kit.ui.alert(result.message);
					}
				});
			}
		</script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			<div class="app-list app-text-13px">
				<div class="app-list-item">
					<a href="${BASE_PATH}/order/track/{{orderNum}}" class="am-u-sm-9 am-u-md-8 am-u-lg-8 app-text-black">
						<label>订单号：</label><span>{{orderNum}}</span>
					</a>
					<a href="${BASE_PATH}/order/track/{{orderNum}}" class="am-u-sm-3 am-u-md-4 am-u-lg-4 am-text-right app-text-blue">
						{{if orderState==<%=OrderCode.OrderState.EXPIRE%>}}
						<span>已过期 <i class="am-icon-angle-right"></i></span>
						{{else if orderState==<%=OrderCode.OrderState.CANCEL%>}}
						<span>已取消 <i class="am-icon-angle-right"></i></span>
						{{else if orderState==<%=OrderCode.OrderState.FINISH%>}}
						<span>交易完成 <i class="am-icon-angle-right"></i></span>
						{{else if payType==<%=OrderCode.OrderPayType.ON_LINE%>&&takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
						<!-- 状态3：在线支付+送货上门 -->
							{{if payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
							<span>等待付款 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							<span>付款成功 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
							<span>打包中 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.PACKING%>}}
							<span>打包中 <i class="am-icon-angle-right"></i></span>
							{{else if deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
							<span>待配送 <i class="am-icon-angle-right"></i></span>
							{{else if deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
							<span>配送中 <i class="am-icon-angle-right"></i></span>
							{{/if}}
						{{else if payType==<%=OrderCode.OrderPayType.ON_LINE%>&&takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
						<!-- 状态4：在线支付+门店自取 -->
							{{if payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
							<span>等待付款 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							<span>付款成功 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
							<span>打包中 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.PACKING%>}}
							<span>打包中 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
							<span>等待取货 <i class="am-icon-angle-right"></i></span>
							{{/if}}
						{{else if payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
						<!-- 状态5：货到付款+送货上门 -->
							{{if packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							<span>处理中 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
							<span>打包中 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.PACKING%>}}
							<span>打包中 <i class="am-icon-angle-right"></i></span>
							{{else if deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
							<span>待配送 <i class="am-icon-angle-right"></i></span>
							{{else if deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
							<span>配送中 <i class="am-icon-angle-right"></i></span>
							{{/if}}
						{{else if payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
						<!-- 状态5：货到付款+门店自取 -->
							{{if packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							<span>处理中 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
							<span>打包中 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.PACKING%>}}
							<span>打包中 <i class="am-icon-angle-right"></i></span>
							{{else if packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
							<span>等待取货 <i class="am-icon-angle-right"></i></span>
							{{/if}}
						{{/if}}
					</a>
					<span class="am-cf"></span>
				</div>
			</div>

			<div class="app-list app-m-t-10">
				<div class="app-list-item">
					<span>{{linkName}}</span>
					<span class="app-m-l-10">{{sex}}</span>
					<span class="app-m-l-10">{{tel}}</span>
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					<span>{{address}}</span>
				</div>
			</div>
			
			<div class="app-list app-m-t-10">
				<div class="app-list-item">
					<div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
						{{if isBlank(storeName)}}大实惠直营
						{{else}}{{storeName}}
						{{/if}}
					</div>
				</div>
				{{each goodsList as goods}}
				<hr/>
				<div class="am-g">
					<div class="am-u-sm-2 am-u-md-3 am-u-lg-3" style="padding:2px;">
						<img data-original="${FTP_PATH}{{goods.thumb}}" width="100%" class="lazyload">
					</div>
					<div class="am-u-sm-10 am-u-md-9 am-u-lg-9" style="padding:2px;">
						<div class="am-u-sm-12 am-u-md-12 am-u-lg-12">{{goods.name}}</div>
						<div class="am-u-sm-1 am-u-md-2 am-u-lg-1 am-fr">x {{goods.count}}</div>
						<div class="am-u-sm-3 am-u-md-2 am-u-lg-1">
							<i class="am-icon-rmb"></i> {{goods.price}}
						</div>
						<span class="am-cf"></span>
					</div>
				</div>
				{{/each}}
				<hr/>
				<div class="app-list-item app-m-t-10">
					<div>
						<span class="am-fl">商品总计</span>
						<span class="am-fr"><i class="am-icon-rmb"></i> {{amount}}</span>
						<span class="am-cf"></span>
					</div>
				</div>
				{{if realPay == null}}
					<div class="app-list-item">
						<div>
							<span class="am-fl">-实惠币</span>
							<span class="am-fr"><i class="am-icon-rmb"></i> 0</span>
							<span class="am-cf"></span>
						</div>
					</div>
					<div class="app-list-item">
						<div>
							<span class="am-fl">实付款</span>
							<span class="am-fr" style="color:#e63f17;"><i class="am-icon-rmb"></i> {{amount}}</span>
							<span class="am-cf"></span>
						</div>
				{{else}}
					<div class="app-list-item">
						<div>
							<span class="am-fl">-实惠币</span>
							<span class="am-fr"><i class="am-icon-rmb"></i> {{offsetMoney}}</span>
							<span class="am-cf"></span>
						</div>
					</div>
					<div class="app-list-item">
						<div>
							<span class="am-fl">实付款</span>
							<span class="am-fr" style="color:#e63f17;"><i class="am-icon-rmb"></i> {{realPay}}</span>
							<span class="am-cf"></span>
						</div>
					</div>
				{{/if}}
			</div>

			{{if !isBlank(describe)}}
			<div class="app-list app-m-t-10">
				<div class="app-list-item">
					<label>备注：</label><span>{{describe}}</span>
				</div>
			</div>
			{{/if}}
			
			<div class="app-list app-m-t-10">
				<div class="app-list-item">
					{{if payType==<%=OrderCode.OrderPayType.ON_LINE%>}}
					<label>支付方式：</label><span>在线支付</span>
					{{else}}
					<label>支付方式：</label><span>货到付款</span>
					{{/if}}
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					<label>下单时间：</label><span>{{createDate}}</span>
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					{{if takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
					<label>配送方式：</label><span>门店配送</span>
					{{else}}
					<label>配送方式：</label><span>上门自取</span>
					{{/if}}
				</div>
				<hr/>
				{{if isSelf == 0}}
					<div class="app-list-item app-m-t-10">
						{{if takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
						<label>配送说明：</label><span>${currentStore.deliverydes}</span>
						{{else}}
						<label>营业时间：</label><span>${currentStore.beginTime} - ${currentStore.endTime}</span>
						{{/if}}
					</div>
				{{/if}}
			</div>

			<div class="am-navbar am-cf am-no-layout app-navbar">
				{{if orderState==<%=OrderCode.OrderState.EXPIRE%>}}
				<div class="am-fr app-p-10"><a href="javascript:doDelete('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">删除订单</a></div>
				{{else if orderState==<%=OrderCode.OrderState.CANCEL%>}}
				<div class="am-fr app-p-10"><a href="javascript:doDelete('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">删除订单</a></div>
				{{else if orderState==<%=OrderCode.OrderState.FINISH%>}}
				<div class="am-fr app-p-10">
					<a href="javascript:doDelete('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">删除订单</a>
					{{if evalState==0}}
					<a href="${BASE_PATH}/order/eval/{{orderNum}}" class="am-btn am-btn-red am-radius app-btn-order">去评价</a>
					{{/if}}
				</div>
				{{else if payType==<%=OrderCode.OrderPayType.ON_LINE%>&&takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<!-- 状态3：在线支付+送货上门 -->
					{{if payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
					<div class="am-fl app-p-5 app-p-l-10 app-text-12px">付款剩余时间：<br/><span class="remain_min app-text-16px">29</span> 分钟 <span class="remain_sec app-text-16px">29</span> 秒</div>
					<div class="am-fr app-p-10">
						<a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">取消订单</a>
						<a href="${BASE_PATH}/order/prepay/{{orderNum}}" class="am-btn am-btn-red app-m-l-10 am-radius app-btn-order">去支付</a>
					</div>
					{{else if packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<div class="am-fr app-p-10">
						<a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">取消订单</a>
						<a href="javascript:doUrge('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">催单</a>
					</div>
					{{else if packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<div class="am-fr app-p-10"><a href="javascript:doUrge('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">催单</a></div>
					{{else if packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<div class="am-fr app-p-10"><a href="javascript:doUrge('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">催单</a></div>
					{{else if deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
					<div class="am-fr app-p-10"><a href="javascript:doUrge('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">催单</a></div>
					{{else if deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
					<div class="am-fr app-p-10"><a href="javascript:doReceive('{{orderNum}}');" class="am-btn am-btn-red am-radius app-btn-order">确认收货</a></div>
					{{/if}}
				{{else if payType==<%=OrderCode.OrderPayType.ON_LINE%>&&takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
				<!-- 状态4：在线支付+门店自取 -->
					{{if payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
					<div class="am-fl app-p-5 app-p-l-10 app-text-12px">付款剩余时间：<br/><span class="remain_min app-text-16px">29</span> 分钟 <span class="remain_sec app-text-16px">29</span> 秒</div>
					<div class="am-fr app-p-10">
						<a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">取消订单</a>
						<a href="${BASE_PATH}/order/prepay/{{orderNum}}" class="am-btn am-btn-red app-m-l-10 am-radius app-btn-order">去支付</a>
					</div>
					{{else if packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<div class="am-fr app-p-10"><a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">取消订单</a></div>
					{{else if packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					{{else if packState==<%=OrderCode.OrderPackState.PACKING%>}}
					{{else if packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
					<div class="am-fr app-p-10"><a href="javascript:doReceive('{{orderNum}}');" class="am-btn am-btn-red am-radius app-btn-order">确认取货</a></div>
					{{/if}}
				{{else if payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<!-- 状态5：货到付款+送货上门 -->
					{{if deliverState!=<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
					<div class="am-fr app-p-10">
						<a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">取消订单</a>
						<a href="javascript:doUrge('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">催单</a>
					</div>
					{{else if packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<div class="am-fr app-p-10">
						<a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">取消订单</a>
						<a href="javascript:doUrge('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">催单</a>
					</div>
					{{else if packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<div class="am-fr app-p-10">
						<a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">取消订单</a>
						<a href="javascript:doUrge('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">催单</a>
					</div>
					{{else if deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
					<div class="am-fr app-p-10">
						<a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">取消订单</a>
						<a href="javascript:doUrge('{{orderNum}}');" class="am-btn am-btn-white am-radius app-btn-order">催单</a>
					</div>
					{{else if deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
					<div class="am-fr app-p-10"><a href="javascript:doReceive('{{orderNum}}');" class="am-btn am-btn-red am-radius app-btn-order">确认收货</a></div>
					{{/if}}
				{{else if payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
				<!-- 状态5：货到付款+门店自取 -->
					{{if packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<div class="am-fr app-p-10"><a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white app-m-l-10 am-radius app-btn-order">取消订单</a></div>
					{{else if packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<div class="am-fr app-p-10"><a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white app-m-l-10 am-radius app-btn-order">取消订单</a></div>
					{{else if packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<div class="am-fr app-p-10"><a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white app-m-l-10 am-radius app-btn-order">取消订单</a></div>
					{{else if packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
					<div class="am-fr app-p-10">
						<a href="javascript:doCancel('{{orderNum}}');" class="am-btn am-btn-white app-m-l-10 am-radius app-btn-order">取消订单</a>
						<a href="javascript:doReceive('{{orderNum}}');" class="am-btn am-btn-red am-radius app-m-l-10 app-btn-order">确认取货</a>
					</div>
					{{/if}}
				{{/if}}
				<div class="am-cf"></div>
			</div>
		</script>
	</body>
</html>