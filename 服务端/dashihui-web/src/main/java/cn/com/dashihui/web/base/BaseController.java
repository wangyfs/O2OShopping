package cn.com.dashihui.web.base;

import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;

import cn.com.dashihui.web.domain.AjaxResult;

public class BaseController extends Controller{
	
	/**
	 * 返回json格式内容
	 */
	protected void renderResult(int flag){
		this.renderText(JsonKit.toJson(new AjaxResult(flag)));
	}
	
	protected void renderResult(int flag,String message){
		this.renderText(JsonKit.toJson(new AjaxResult(flag,message)));
	}
	
	protected void renderResult(int flag,String message,Object object){
		this.renderText(JsonKit.toJson(new AjaxResult(flag,message,object)));
	}
	
	protected void renderResult(int flag,Object object){
		this.renderText(JsonKit.toJson(new AjaxResult(flag,object)));
	}
	
	protected void renderSuccess(){
		renderResult(AjaxResult.FLAG_SUCCESS);
	}
	
	protected void renderSuccess(String message){
		renderResult(AjaxResult.FLAG_SUCCESS,message);
	}
	
	protected void renderSuccess(Object object){
		renderResult(AjaxResult.FLAG_SUCCESS,object);
	}
	
	protected void renderSuccess(String message,Object object){
		renderResult(AjaxResult.FLAG_SUCCESS,message,object);
	}
	
	protected void renderFailed(){
		renderResult(AjaxResult.FLAG_FAILED);
	}
	
	protected void renderFailed(String message){
		renderResult(AjaxResult.FLAG_FAILED,message);
	}
}
