/**
 * 封装一些常用方法<br/>
 * 将mui和jquery等第三方类库的方法进行一个浅封装，目的是减少耦合，方便替换ui和js框架
 */
(function(window){
	window.Kit = {};
	//数据库操作类
	Kit.dao = {
		/**
		 * 保存数据
		 * @param {Object} key
		 * @param {Object} value
		 * @param {Object} objToJsonStr 是否需要转换为json字符串，true或false
		 */
		save : function(key,value,objToJsonStr){
			if(objToJsonStr){
				plus.storage.setItem(key,JSON.stringify(value));
			}else{
				plus.storage.setItem(key,value);
			}
		},
		/**
		 * 取出数据
		 * @param {Object} key 
		 * @param {Object} jsonStrToObj 是否需要将数据转换为对象取出，true或false
		 */
		get : function(key,jsonStrToObj){
			var value = plus.storage.getItem(key);
			if(jsonStrToObj&&value){
				return JSON.parse(value);
			}
			return value;
		},
		/** 
		 * 删除
		 * @param {Object} key
		 */
		del : function(key){
			plus.storage.removeItem(key);
		},
		/**
		 * 清除所有
		 */
		clear : function(){
			plus.storage.clear();
		},
		/**
		 * 验证数据是否存在
		 * @param {Object} key
		 */
		has : function(key){
			var value = Kit.dao.get(key);
			if(value!=null&&typeof(value)!="undefined"){
				return true;
			}
			return false;
		}
	};
	//异步类
	Kit.ajax = {
		/**
		 * post异步请求
		 * @param {Object} url
		 * @param {Object} params
		 * @param {Object} callback
		 */
		post : function(url,params,callback){
			//请求
			$.post(url,params,function(data){
				if(data.STATE==Kit.api.code.SUCCESS){
					callback(data.OBJECT);
				}else{
					Kit.ui.alert(data.MSG);
				}
			},"json");
		},
		/**
		 * get异步请求
		 * @param {Object} url
		 * @param {Object} callback
		 */
		get : function(url,callback){
			$.get(url,function(data){
				callback(data);
			},"json");
		}
	};
	//界面操作类
	Kit.ui = {
		/**
		 * 显示加载中提示框
		 * tip:提醒信息
		 * 
		 */
		showLoading : function(tip,waitingParam){
			return plus.nativeUI.showWaiting(tip,waitingParam);
		},
		/**
		 * 关闭加载中提示框
		 */
		closeLoading : function(){
			plus.nativeUI.closeWaiting();
		},
		/**
		 * toast提示框
		 * @param {Object} message
		 */
		alert : function(message){
			plus.nativeUI.toast(message);
		},
		/**
		 * alert提示框，只能确定
		 * @param {Object} message
		 */
		modal : function(message){
			plus.nativeUI.alert(message);
		},
		/**
		 * 确定提示框
		 * @param {Object} title
		 * @param {Object} message
		 * @param {Object} okFn 用户确认时回调事件
		 * @param {Object} cancelFn 用户取消认时回调事件
		 */
		confirm : function(title,message,okFn,cancelFn){
			plus.nativeUI.confirm(message, function(e){
				if(e.index==0){
					okFn();
				}else{
					if(cancelFn){
						cancelFn();
					}
				}
			}, title, ['确定','取消']);
		},
		/**
		 * 输入框
		 * @param {Object} title 标题
		 * @param {Object} message 显示内容
		 * @param {Object} placeholder 输入框默认提示
		 * @param {Object} okCallback 用户确认时回调事件
		 * @param {Object} cancelCallback 用户取消回调事件
		 */
		prompt:function(title,message,placeholder,okCallback,cancelCallback){
			plus.nativeUI.prompt(message, function(e){
				if(e.index==0){
					okCallback(e.value);
				}else{
					if(cancelCallback){
						cancelCallback();
					}
				}
			},title, placeholder, ["确定","取消"]); 
		}
	};
	//页面跳转类
	Kit.render = {
		/**
		 * 重定向
		 * @param {Object} href
		 */
		redirect : function(href){
			window.location.href = href;
		}
		
	};
	//验证类
	Kit.validate = {
		/**
		 * 验证是否为空，可接收对象或字符串，对象时只验证是否undefined，字符串时再验证是否为空串
		 * @param {Object} obj
		 */
		isBlank : function (obj) {
			if(typeof obj === "undefined"){
				return true;
			}
			if(typeof obj === "string"){
				return obj==="";
			}
			return true;
		}
	};
	//日期时间类
	Kit.date = {
		/**
		 * 获得当前时间
		 * 格式 yyyy-mm-dd hh:mm:ss
		 */
		getCurrentDate:function(){
				var date = new Date();
			    var seperator1 = "-";
			    var seperator2 = ":";
			    var month = date.getMonth() + 1;
			    var strDate = date.getDate();
			    if (month >= 1 && month <= 9) {
			        month = "0" + month;
			    }
			    if (strDate >= 0 && strDate <= 9) {
			        strDate = "0" + strDate;
			    }
			    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
			            + " " + date.getHours() + seperator2 + date.getMinutes()
			            + seperator2 + date.getSeconds();
			    return currentdate;
		}
	}
	//调试类
	Kit.debug = {
		/**
		 * 调试日志
		 * @param {Object} message
		 */
		log : function(message){
			console.log(message);
		},
		/**
		 * 输出所有保存的数据以调试
		 */
		storage : function () {
			var numKeys=plus.storage.getLength();
			for(var i=0; i<numKeys; i++) {
				var key = plus.storage.key(i);
				console.log(key+":"+plus.storage.getItem(key));
			}
		}
	}
	
	/**
	 * 下载文件
	 */
	Kit.download={
		/**
		 * url：下载文件地址
		 * force：true：强制更新，false：非强制更新
		 */
		create:function (url,force) {
			//创建下载任务
			var task = plus.downloader.createDownload(url, {}, function (d, status) {
				// 下载完成
				if (status == 200) {
					 //下载完成后，安装下载的apk文件
					 plus.runtime.install(d.filename);
					if(!force){
						//非强制更新时，自动关闭loading，让用户可以继续操作
						Kit.ui.closeLoading();
					}
				}else{
					Kit.ui.alert("下载失败");
				}
			});
			//开始下载
			task.start();
		}
	}
	
	//即时执行的页面初始化代码
	if(window.$||window.jQuery){
		//设置jQuery默认配置
		$.ajaxSetup({
			//不开启缓存
			cache:false,
			//默认数据类型为JSON
			dataType:"json",
			//异步发出前统一执行的动作
			beforeSend:function(){
				Kit.ui.showLoading();
				//异步开始时记录时间
				window.ajaxStartTime = (new Date()).getTime();
			},
			//异步结束后统一执行的动作
			complete:function(response,status){
				//异步结束后，统一判断登录状态
				//if(status=="success"){
				//	if(response.responseJSON&&response.responseJSON.flag==-2){
				//		//dataType为json时
				//		window.location.href=BASE_PATH+"/login";
				//	}else if(response.responseText&&response.responseText.indexOf("\"flag\":-2")==1){
				//		//dataType不是json时
				//		window.location.href=BASE_PATH+"/login";
				//	}
				//}
				//异步结束时计算共用时长
				var ajaxStartTime = window.ajaxStartTime;
				var ajaxEndTime = (new Date()).getTime();
				var diff = ajaxEndTime-ajaxStartTime;
				//当时长小于1秒时，为达到效果，不让用户看到闪屏现象，此时要设置延迟清除loading效果
				var reduce = 1000-diff;
				if(reduce>0){
					setTimeout(function(){
						Kit.ui.closeLoading();
					}, reduce);
				}else{
					//当时长大于等于1秒时，直接清除loading效果
					Kit.ui.closeLoading();
				}
			}
		});
	}
})(window);