<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'/>
<script type="text/javascript">
window.BASE_PATH = '${BASE_PATH}';
window.FTP_PATH = '${FTP_PATH}';
</script>
<!--[if lt IE 9]>
<script src='${BASE_PATH}/static/js/html5shiv.js' type='text/javascript'></script>
<script src='${BASE_PATH}/static/js/respond.min.js' type='text/javascript'></script>
<![endif]-->
<!-- bootstrap -->
<link href='${BASE_PATH}/static/plugins/bootstrap/css/bootstrap.min.css' media='all' rel='stylesheet' type='text/css' />
<!-- bootstrap theme -->
<link href='${BASE_PATH}/static/plugins/bootstrap/css/bootstrap-theme.min.css' media='all' rel='stylesheet' type='text/css' />
<!-- bootstrap dialog -->
<link href='${BASE_PATH}/static/plugins/bootstrap-dialog/css/bootstrap-dialog.min.css' media='all' rel='stylesheet' type='text/css' />
<!-- font awesome -->
<link href='${BASE_PATH}/static/fonts/css/font-awesome.min.css' media='all' rel='stylesheet' type='text/css' />
<!-- icheck -->
<link href='${BASE_PATH}/static/plugins/bootstrap-icheck/skin/minimal/blue.css' media='all' rel='stylesheet' type='text/css' />
<!-- 折叠菜单 -->
<link href='${BASE_PATH}/static/plugins/metisMenu/css/metisMenu.min.css' media='all' rel='stylesheet' type='text/css' />
<!-- bootstrap bootstrap-datetimepicker -->
<link href='${BASE_PATH}/static/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
<!-- bootstrap select -->
<link href='${BASE_PATH}/static/plugins/bootstrap-select/css/bootstrap-select.min.css' media='all' rel='stylesheet' type='text/css' />
<!-- bootstrap fileinput -->
<link href="${BASE_PATH}/static/plugins/bootstrap-fileinput/css/fileinput.ext.min.css" rel="stylesheet" media="screen">
<!-- ui -->
<link href='${BASE_PATH}/static/css/ui.css' media='all' rel='stylesheet' type='text/css' />
<!-- 自定义 -->
<link href='${BASE_PATH}/static/css/custom.css' media='all' rel='stylesheet' type='text/css' />