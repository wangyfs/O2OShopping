<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">首页轮播</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-offset-6 col-lg-6">
						<div class="pull-right">
							<shiro:hasPermission name="bus:ad:add">
							<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
							</shiro:hasPermission>
							<shiro:hasPermission name="bus:ad:sort">
							<button type="button" class="btn btn-link" onclick="doSort()"><span class="fa fa-save"></span> 保存排序</button>
							</shiro:hasPermission>
						</div>
					</div>
				</div>
				<hr/>
				<form id="sortForm" action="<c:url value="/bus/ad/doSort"/>" method="post">
				<input type="hidden" name="sortKey" value="orderNo">
				<div id="dataList" class="row">
					<c:if test="${adList!=null&&fn:length(adList)!=0}">
					<c:forEach items="${adList}" var="item">
					<div id="item${item.id}" class="col-lg-2 col-md-2 col-sm-2">
						<div class="thumbnail">
							<c:if test="${item.link!=null&&fn:length(item.link)!=0}">
							<a href="${item.link}" target="_blank"><img src="${FTP_PATH}${item.thumb}"></a>
							</c:if>
							<c:if test="${item.link==null||fn:length(item.link)==0}">
							<a href="#"><img src="${FTP_PATH}${item.thumb}"></a>
							</c:if>
							<div class="caption">
								<shiro:hasPermission name="bus:ad:sort">
								<input name="orderNo${item.id}" value="${item.orderNo}" size="3" title="排序" class="order form-control width50 pull-left" required>
								</shiro:hasPermission>
								<shiro:hasPermission name="bus:ad:delete">
								<button type="button" class="btn btn-sm btn-danger pull-right" onclick="toDelete('${item.id}')" title="删除"><span class="fa fa-remove fa-lg"></span> 删除</button>
								</shiro:hasPermission>
								<shiro:hasPermission name="bus:ad:edit">
								<button type="button" class="btn btn-sm btn-primary pull-right m-r-10" onclick="toEdit('${item.id}')" title="编辑"><span class="fa fa-edit fa-lg"></span> 编辑</button>
								</shiro:hasPermission>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					</c:forEach>
					</c:if>
				</div>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	<div id="item{{id}}" class="col-lg-2 col-md-2 col-sm-2">
		<div class="thumbnail">
			{{if !link}}
			<a href="#"><img src="${FTP_PATH}{{thumb}}"></a>
			{{else}}
			<a href="{{link}}" target="_blank"><img src="${FTP_PATH}{{thumb}}"></a>
			{{/if}}
			<div class="caption">
				<shiro:hasPermission name="bus:ad:sort">
				<input name="orderNo{{id}}" value="0" size="3" title="排序" class="order form-control width50 pull-left" required>
				</shiro:hasPermission>
				<shiro:hasPermission name="bus:ad:delete">
				<button type="button" class="btn btn-sm btn-danger pull-right" onclick="toDelete('{{id}}')" title="删除"><span class="fa fa-remove fa-lg"></span> 删除</button>
				</shiro:hasPermission>
				<shiro:hasPermission name="bus:ad:edit">
				<button type="button" class="btn btn-sm btn-primary pull-right m-r-10" onclick="toEdit('{{id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span> 编辑</button>
				</shiro:hasPermission>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</script>
<script type="text/javascript">
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/bus/ad/toAdd").open();
}
function onAddSuccess(newObject){
	$("#dataList").prepend(template("dataTpl",newObject));
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/bus/ad/toEdit/"+id).open();
}
function onEditSuccess(newObject){
	$("#item"+newObject.id).replaceWith(template("dataTpl",newObject));
}
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/bus/ad/doDelete/"+id,function(result){
			$("#item"+id).remove();
		});
	});
}
function doSort(){
	$("#sortForm").ajaxSubmit({
		success:function(data){
			switch(data.flag){
			case -1:
				Kit.alert("系统异常，请重试");return;
			case 0:
				window.location.reload();
			}
		}
	});
}
</script>
</body>
</html>