<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		rules:{link:{url:true}},
		messages:{
			link: {required: "请输入正确的链接地址"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 1:
						Kit.alert("请上传图片");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						onAddSuccess(data.object);
						addDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="addForm" action="${BASE_PATH}/bus/ad/doAdd" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">
		<label class="col-lg-2 control-label">图片</label>
		<div class="col-lg-9">
			<input type="file" id="thumb" name="thumb" accept="image/jpg" title="选择文件">
		</div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">链接</label>
	    <div class="col-lg-9">
        	<input type="text" name="link" value="" class="form-control" placeholder="请输入链接地址" maxlength="200">
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>