<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#editRebateForm").validate({
		rules: {
			percent1: {isPercent: true},
			percent2: {isPercent: true},
			percent3: {isPercent: true},
			percent4: {isPercent: true},
			percent5: {isPercent: true}
		},
		messages:{
			percent1: {isPercent: "请填写正确的比例，最多保留两位小数位"},
			percent2: {isPercent: "请填写正确的比例，最多保留两位小数位"},
			percent3: {isPercent: "请填写正确的比例，最多保留两位小数位"},
			percent4: {isPercent: "请填写正确的比例，最多保留两位小数位"},
			percent5: {isPercent: "请填写正确的比例，最多保留两位小数位"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("修改失败");return;
					case 1:
						Kit.alert("参数isRebate不正确");return;
					case 2:
						Kit.alert("比例必须在1~99之间(包含1和99)");return;
					case 0:
						onEditSuccess(data.object);
						editRebateDialog.close();
						return;
					}
				}
			});
		}
	});
});
function onRebateSelected(v){
	if(v==1){
		$(".onRebate1").show();
	} else {
		$(".onRebate1").hide();
	}
}
</script>
<form id="editRebateForm" action="${BASE_PATH}/bus/goods/store/doEditRebate" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-3 control-label">标题</label>
	    <div class="col-lg-7">
        	<input type="text" name="name" class="form-control" value="${object.name}" disabled="disabled">
	    </div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label">是否返利</label>
		<div class="col-lg-7">
	    	<ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="isRebate" value="0" data-callback="onRebateSelected" <c:if test="${object.isRebate==0}">checked</c:if>> 不是</li>
	    		<li><input type="radio" class="iCheck" name="isRebate" value="1" data-callback="onRebateSelected" <c:if test="${object.isRebate==1}">checked</c:if>> 是</li>
	    	</ul>
	    </div>
	</div>
	<div class="onRebate1 form-group" style="display:none;">
		<label class="col-lg-3 control-label">推荐人返现比例</label>
		<div class="col-lg-7">
			<div class="input-group">
	        	<input type="text" name="percent1" class="form-control" placeholder="请输入推荐人返利比例" value="${object.percent1 }" maxlength="5" isPercent/>
	        	<div class="input-group-addon">%</div>
			</div>
	    </div>
	</div>
	<div class="onRebate1 form-group" style="display:none;">
		<label class="col-lg-3 control-label">DSH返现比例</label>
		<div class="col-lg-7">
			<div class="input-group">
        		<input type="text" name="percent2" class="form-control" placeholder="请输入DSH返利比例" value="${object.percent2 }" maxlength="5" isPercent/>
	        	<div class="input-group-addon">%</div>
			</div>
	    </div>
	</div>
	<div class="onRebate1 form-group" style="display:none;">
		<label class="col-lg-3 control-label">社区负责人返现比例</label>
		<div class="col-lg-7">
			<div class="input-group">
        		<input type="text" name="percent3" class="form-control" placeholder="社区负责人返利比例" value="${object.percent3 }" maxlength="5" isPercent/>
	        	<div class="input-group-addon">%</div>
			</div>
	    </div>
	</div>
	<div class="onRebate1 form-group" style="display:none;">
		<label class="col-lg-3 control-label">一级分销返现比例</label>
		<div class="col-lg-7">
			<div class="input-group">
        		<input type="text" name="percent4" class="form-control" placeholder="一级分销返利比例" value="${object.percent4 }" maxlength="5" isPercent/>
	        	<div class="input-group-addon">%</div>
			</div>
	    </div>
	</div>
	<div class="onRebate1 form-group" style="display:none;">
		<label class="col-lg-3 control-label">二级分销返现比例</label>
		<div class="col-lg-7">
			<div class="input-group">
        		<input type="text" name="percent5" class="form-control" placeholder="二级分销返利比例" value="${object.percent5 }" maxlength="5" isPercent/>
	        	<div class="input-group-addon">%</div>
			</div>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left"  type="button" onclick="javascript:editRebateDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="goodsid" value="${object.id}"/>
</form>