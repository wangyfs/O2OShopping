<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style>
<!--
.temp-title{
	border-left:4px solid #e9423a;
	font-size:20px;
}
-->
</style>
<script type="text/javascript">
<!--
$(function(){
	$.post("${BASE_PATH}/api/logistics",function(result){
		$("#setLogiDataList").empty().append(template("setLogiDataTpl",{"list":result}));
	});
});
function doSetLogi(){
	var logiList = $("[name=logiid]:checked");
	if(logiList.length!=1){
		alert("请选择物流公司");
	}else{
		var logi = $(logiList.get(0));
		var logiOrderNum = $("[name=logiOrderNum]");
		if(logiOrderNum.val().trim()==""){
			Kit.alert("请填写快递单号！");
		}else{
			onLogiChoosed('${orderNum}',logi.val(),logi.data("title"),logiOrderNum.val());
			setLogiDialog.close();
		}
	}
}
//-->
</script>
<script type="text/html" id="setLogiDataTpl">
	{{each list as item index}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.title}}</td>
		<td><input type="radio" class="iCheck" name="logiid" value="{{item.id}}" data-title="{{item.title}}" {{if index==0}}checked{{/if}}></td>
	</tr>
	{{/each}}
</script>
<form id="logiForm" class="form-horizontal">
	<div class="row">
		<div class="col-lg-12 temp-title">
			<span class="">选择快递公司</span>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
		            <thead>
		                <tr>
		                    <th>物流公司</th>
		                    <th width="15%"></th>
		                </tr>
		            </thead>
		            <tbody id="setLogiDataList"></tbody>
		        </table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 temp-title">
			<span class="">填写物流单号</span>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="form-group" style="margin-top:8px;">
		<div class="col-lg-12">
       		<input type="text" name="logiOrderNum" class="form-control" placeholder="请输入物流单号" required maxlength="21">
       	</div>
       	<div class="clearfix"></div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="button" onclick="javascript:doSetLogi();" autocomplete="off">确定</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left"  type="button" onclick="javascript:setLogiDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>