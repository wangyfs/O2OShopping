<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">用户</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
						<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                    <th width="30%">真实姓名</th>
			                    <th width="30%">用户名</th>
			                    <th width="15%">状态</th>
			                    <th width="10%">添加时间</th>
			                    <th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.trueName}}</td>
		<td>{{item.username}}</td>
		<td>{{item.enabled | flagTransform:1,'可用',0,'禁用'}}</td>
		<td>{{item.createDate | dateFormat:'yyyy-MM-dd'}}</td>
		<td>{{if ${currUser.id}!=item.id}}<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toEditPwd('{{item.id}}')" title="修改密码"><span class="fa fa-lock fa-lg"></span></button> {{/if}}<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button>{{if ${currUser.id}!=item.id}} <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button>{{/if}} <button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toAccredit('{{item.id}}')" title="授权"><span class="fa fa-info fa-lg"></span></button></td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(function(){
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/auth/admin/page",{"pageSize":20},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
});
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/auth/admin/toAdd").open();
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/auth/admin/toEdit/"+id).open();
}
function onEditSuccess(id,newObject){
	$("#item"+id).replaceWith(template("list-tpl",{"list":[newObject]}));
}
var editPwdDialog;
function toEditPwd(id){
	editPwdDialog = Kit.dialog("修改密码","${BASE_PATH}/auth/admin/toEditPwd/"+id).open();
}
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/auth/admin/doDelete/"+id,function(result){
			$("#item"+id).remove();
		});
	});
}
var sRoleDialog;
function toAccredit(id){
	sRoleDialog = Kit.dialog("授权","${BASE_PATH}/auth/admin/toAccredit/"+id).open();
}
</script>
</body>
</html>