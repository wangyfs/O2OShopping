<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">文章发布</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-offset-2 col-lg-9">
				<label class="search-label">模板路径：</label><input type="text" id="ftlPath" value="F:\dashihui\apache-tomcat-8.0.28\webapps\dashihui-admin\WEB-INF\page\wx\article\output\ftl" class="form-control search-input width300" <shiro:lacksRole name="admin">readonly</shiro:lacksRole>>
				<label class="search-label">输出路径：</label><input type="text" id="outputPath" value="F:\dashihui\apache-tomcat-8.0.28\webapps\dashihui-article\<c:out value="${mp}"/>" class="form-control search-input width300" <shiro:lacksRole name="admin">readonly</shiro:lacksRole>>
				<button class="btn btn-primary search-btn" onclick="doOutput();">发布</button>
				<button class="btn btn-success search-btn" onclick="doRefresh();">刷新</button>
				<a href="/dashihui-article/<c:out value="${mp}"/>" class="btn btn-info search-btn" style="width:auto;" target="_blank">新窗口打开</a>
				<div class="clearfix"></div>
			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="col-lg-offset-3 col-lg-8">
				<iframe id="webview" src="" width="740" style="border:1px solid #e3e3e3;min-height:630px;"></iframe>
			</div>
		</div>
	</div>
	<jsp:include page="../../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/javascript">
var url = "/dashihui-article/<c:out value="${mp}"/>";
$(doRefresh);
function doOutput(){
	var params = {"ftlPath":$("#ftlPath").val(),"outputPath":$("#outputPath").val(),"mp":"${mp}"}
	$.post("${BASE_PATH}/wx/article/output/output",params,function(result){
		if(result.flag==0){
			doRefresh();
		}else{
			Kit.alert("生成失败！");
		}
	});
}
function doRefresh(){
	$("#webview").attr("src",url+"?t="+(new Date()).getTime());
}
</script>
</body>
</html>