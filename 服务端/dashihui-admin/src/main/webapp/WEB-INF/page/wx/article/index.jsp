<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">文章管理</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-2 col-lg-9">
				<label class="search-label">关键字：</label><input type="text" id="sKeyword" value="" class="form-control search-input width200" maxlength="21">
				<label class="search-label">添加时间：</label><input type="text" id="sBeginDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" maxlength="10">
				<label class="search-label">至</label><input type="text" id="sEndDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" maxlength="10">
				<label class="search-label">分类：</label>
				<div class="search-input">
					<select id="sCategoryid" class="selectpicker form-control" data-url="${BASE_PATH}/wx/article/category/all?mp=${mp}" data-val="${object.categoryid}" data-issingle="true" data-key="id:name"></select>
				</div>
				<label class="search-label">置顶：</label>
				<div class="search-input">
					<select id="sIsTop" class="selectpicker form-control">
						<option value="" selected="selected">请选择</option>
						<option value="1">是</option>
						<option value="0">否</option>
					</select>
				</div>
				<button class="btn btn-success search-btn" onclick="query();">确定</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
						<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                    <th>标题</th>
			                    <th width="10%">分类</th>
			                    <th width="5%">置顶</th>
			                    <th width="10%">添加时间</th>
			                    <th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.title}}</td>
		<td>{{item.categoryName}}</td>
		<td>{{item.isTop | flagTransform:1,'是',0,'否'}}</td>
		<td>{{item.createDate | dateFormat:'yyyy-MM-dd'}}</td>
		<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button> <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button></td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(query);
function query(){
	var params = {
			pageSize:12,
			keyword:$("#sKeyword").val(),
			mp:"${mp}",
			beginDate:$("#sBeginDate").val(),
			endDate:$("#sEndDate").val(),
			categoryid:$("#sCategoryid").val(),
			isTop:$("#sIsTop").val()
		};
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/wx/article/page",params,function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/wx/article/toAdd?mp=${mp}",{size:"size-wide"}).open();
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/wx/article/toEdit/"+id+"?mp=${mp}",{size:"size-wide"}).open();
}
function onEditSuccess(id,newObject){
	$("#item"+id).replaceWith(template("list-tpl",{"list":[newObject]}));
}
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/wx/article/doDelete/"+id,function(result){
			$("#item"+id).remove();
		});
	});
}
</script>
</body>
</html>