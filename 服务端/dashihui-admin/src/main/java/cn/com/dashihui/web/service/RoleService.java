package cn.com.dashihui.web.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Role;

public class RoleService {
	
	public boolean addRole(Role newObject){
		return newObject.save();
	}
	
	public boolean delRole(final int id){
		//事务处理，同时删除角色记录，并删除相应的用户角色关联记录，以及相应的角色权限关联记录
		return Db.tx(new IAtom(){
			public boolean run() throws SQLException {
				boolean ret1 = Role.me().deleteById(id);
				Db.update("DELETE FROM t_auth_admin_roles WHERE roleid=?",id);
				Db.update("DELETE FROM t_auth_role_resources WHERE roleid=?",id);
				return ret1;
			}
		});
	}
	
	public boolean editRole(Role object){
		return object.update();
	}
	
	public Role findById(int id){
		return Role.me().findFirst("SELECT * FROM t_auth_roles WHERE id=?",id);
	}
	
	public Page<Record> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_auth_roles A");
		List<Object> params = new ArrayList<Object>();
		sqlExcept.append(" ORDER BY A.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(), params.toArray());
	}
	
	public List<Record> findAllRoles(){
		return Db.find("SELECT * FROM t_auth_roles");
	}
	
	public List<Record> findRolesByUser(int userid){
		return Db.find("SELECT * FROM v_admin_roles WHERE userid=?",userid);
	}
	
	public boolean saveRoleResource(int roleid,String[] resources){
		List<String> sqlList = new ArrayList<String>();
		int batchSize = 0;
		if(resources!=null&&resources.length!=0){
			batchSize = resources.length;
			//插入角色-权限关系表SQL
			for(int i=0;i<batchSize;i++){
				sqlList.add("INSERT INTO t_auth_role_resources(roleid,resourceid) VALUES("+roleid+","+resources[i]+")");
			}
		}
		//使用add(0,sql)方法，将“删除旧角色-权限关系表SQL”放在第一条执行
		sqlList.add(0,"DELETE FROM t_auth_role_resources WHERE roleid="+roleid);batchSize++;
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public boolean saveUserRole(int userid,String[] roles){
		List<String> sqlList = new ArrayList<String>();
		int batchSize = 0;
		if(roles!=null&&roles.length!=0){
			batchSize = roles.length;
			//插入用户-角色关系表SQL
			for(int i=0;i<batchSize;i++){
				sqlList.add("INSERT INTO t_auth_admin_roles(userid,roleid) VALUES("+userid+","+roles[i]+")");
			}
		}
		//使用add(0,sql)方法，将“删除旧用户-角色关系表SQL”放在第一条执行
		sqlList.add(0,"DELETE FROM t_auth_admin_roles WHERE userid="+userid);batchSize++;
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
}
