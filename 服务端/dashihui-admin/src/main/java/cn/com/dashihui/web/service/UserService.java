package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.User;

public class UserService {
	
	public boolean addUser(User newObject){
		return newObject.save();
	}
	
	public boolean delUser(int id){
		return User.me().deleteById(id);
	}
	
	public boolean editUser(User object){
		return object.update();
	}
	
	public User findById(int id){
		return User.me().findFirst("SELECT * FROM t_bus_user WHERE id=?",id);
	}
	
	public Page<Record> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_bus_user");
		sqlExcept.append(" ORDER BY createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT * ", sqlExcept.toString());
	}
	
	public boolean findExistsByUsername(String username){
		return User.me().findFirst("SELECT * FROM t_bus_user WHERE username=?",username)!=null;
	}
	
	public boolean findExistsByUsernameWithout(String username,int userid){
		return User.me().findFirst("SELECT * FROM t_bus_user WHERE username=? AND id!=?",username,userid)!=null;
	}
}
