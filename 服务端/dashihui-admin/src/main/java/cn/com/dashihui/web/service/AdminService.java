package cn.com.dashihui.web.service;

import java.sql.SQLException;
import java.util.List;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordService;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.web.dao.Admin;

public class AdminService {
	
	public boolean findExistsByUsername(String username){
		return Admin.me().findFirst("SELECT * FROM t_auth_admin WHERE username=?",username)!=null;
	}
	
	public boolean addUser(Admin newObject){
		return newObject.save();
	}
	
	public boolean delUser(final int id){
		//事务处理，同时删除用户记录，并删除相应的用户角色关联记录
		return Db.tx(new IAtom(){
			public boolean run() throws SQLException {
				boolean ret1 = Admin.me().deleteById(id);
				Db.update("DELETE FROM t_auth_admin_roles WHERE userid=?",id);
				return ret1;
			}
		});
	}
	
	public boolean editUser(Admin object){
		return object.update();
	}
	
	@Before(Tx.class)
	public boolean editMinePwd(Admin currentUser,String passwordOld,String passwordNew) throws Exception{
		Admin user = Admin.me().findById(currentUser.getInt("id"));
		if(user==null){
			return false;
		}
		PasswordService svc = new DefaultPasswordService();
		if(!svc.passwordsMatch(passwordOld, user.getStr("password"))){
			throw new Exception("原密码输入不正确");
		}else{
			return user.set("password", svc.encryptPassword(passwordNew)).update();
		}
	}
	
	public Admin findEnabledByUsername(String username){
		return Admin.me().findFirst("SELECT * FROM t_auth_admin WHERE username=? AND enabled=1",username);
	}
	
	public Admin findById(int id){
		return Admin.me().findFirst("SELECT a.* FROM t_auth_admin a WHERE a.id=?",id);
	}
	
	public Page<Record> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_auth_admin A");
		sqlExcept.append(" ORDER BY A.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString());
	}
	
	public Admin findByUsername(String username){
		return Admin.me().findFirst("SELECT * FROM t_auth_admin WHERE username=?",username);
	}
	
	public List<Record> findRolesByUser(int userid){
		return Db.find("SELECT * FROM v_admin_roles WHERE userid=?",userid);
	}
}
