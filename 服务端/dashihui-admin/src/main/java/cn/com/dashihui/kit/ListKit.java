package cn.com.dashihui.kit;

import java.util.ArrayList;
import java.util.List;

public class ListKit<T> {
	private List<T> list = new ArrayList<T>();
	
	public ListKit<T> add(T item){
		list.add(item);
		return this;
	}
	
	public List<T> getList(){
		return list;
	}
	
	public Object[] toArray(){
		return list.toArray();
	}
}
