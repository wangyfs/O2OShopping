package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Brand;

public class BrandService {
	
	public boolean addBrand(Brand newObject){
		return newObject.save();
	}
	
	public boolean delBrand(int id){
		return Brand.me().deleteById(id);
	}
	
	public boolean editBrand(Brand object){
		return object.update();
	}
	
	public Brand findById(int id){
		return Brand.me().findFirst("SELECT a.* FROM t_dict_brand a WHERE a.id=?",id);
	}
	
	public Page<Record> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_dict_brand A");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString());
	}
}
