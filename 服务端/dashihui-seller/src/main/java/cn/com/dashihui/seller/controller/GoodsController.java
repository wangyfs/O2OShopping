package cn.com.dashihui.seller.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.StringUtils;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.seller.dao.Goods;
import cn.com.dashihui.seller.kit.LuceneKit;
import cn.com.dashihui.seller.service.GoodsService;

public class GoodsController extends BaseController{

private GoodsService service = new GoodsService();
	HttpServletRequest request;
	HttpServletResponse response;
	
	void init(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}
	
	/**
	 * 商品列表
	 */
	public void index(){
		setAttr("tagList", service.getAllEnabledTag(0));
		render("index.jsp");
	}
	
	/**
	 * 查询所有分类
	 */
	public void getAllCategory() {
		renderSuccess(service.findAllCategory().toArray());
	}
	
	/**
	 * 跳转到商品列表页面
	 */
	public void goToList() {
		StringBuffer goodsType = new StringBuffer();
		String[] goodsTypeArr = getParaValues("goodsType");
		if(goodsTypeArr != null){
			for(int i = 0; i < goodsTypeArr.length; i++) {
				goodsType.append(goodsTypeArr[i]);
				if(i != goodsTypeArr.length - 1) {
					goodsType.append(",");
				}
			}
		}
		setAttr("goodsType", goodsType.toString());
		setAttr("categoryonid", getPara("categoryonid"));
		setAttr("categorytwid", getPara("categorytwid"));
		setAttr("categorythid", getPara("categorythid"));
		setAttr("categoryfoid", getPara("categoryfoid"));
		setAttr("state", getPara("state"));
		setAttr("keyword", getPara("keyword"));
		String[] tags = getParaValues("tag");
		String tagsid = "";
		if(tags != null) {
			if(tags.length > 0) {
				for(int i = 0; i < tags.length; i++) {
					if(i == tags.length - 1) {
						tagsid += tags[i];
					} else {
						tagsid += tags[i] + ",";
					}
				}
			}
		}
		setAttr("tagsid", tagsid);
		render("list.jsp");
	}
	
	/**
	 * 修改商品的状态
	 */
	public void updateState() {
		String state = getPara("state");
		String id = getPara("goodsid");
		if(StringUtils.equals(state, "1")) {
			state = "2";
		} else {
			state = "1";
		}
		Goods goods = new Goods()
				.set("id", id)
				.set("state", state);
		if(goods.update()) {
			renderSuccess();
		} else {
			renderFailed();
		}
	}
	
	/**
	 * 异步查询商品分页列表
	 * @param goodsType 优惠类型，1：普通，2：推荐，3：限量，4：一元购
	 * @param categoryonid 一级分类
	 * @param categorytwid 二级分类
	 * @param categorythid 三级分类
	 * @param categoryfoid 四级分类
	 * @param state 商品状态，1：已上架，2：已下架
	 * @param keyword 关键字
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public void page(){
		
		String goodsType = getPara("goodsType");
		String categoryonid = getPara("categoryonid");
		String categorytwid = getPara("categorytwid");
		String categorythid = getPara("categorythid");
		String categoryfoid = getPara("categoryfoid");
		String state = getPara("state");
		String keyword = getPara("keyword");
		String tagsid = getPara("tagsid");
		
		int pageNum = getParaToInt("pageNum",1);
		int pageSize = getParaToInt("pageSize",PropKit.getInt("constants.pageSize"));
		renderSuccess(service.findByPage(getCurrentStoreid(),goodsType,categoryonid,categorytwid,categorythid,categoryfoid,state,keyword,pageNum,pageSize,tagsid));
	}
	
	/**
	 * 跳转到编辑商品页面
	 */
	public void toEdit() {
		int goodsid = getParaToInt(0,0);
		if(goodsid!=0){
			setAttr("tagList", service.getAllEnabledTag(goodsid));
			setAttr("goods", service.findById(goodsid));
		}
		render("edit.jsp");
	}
	
	/**
	 * 修改商品信息
	 */
	public void doEdit() {
		//商品名称
		String id = getPara("goodsid");
		//商品名称
		String name = getPara("name");
		//商品规格说明
		String spec = getPara("spec");
		//短简介
		String shortInfo = getPara("shortInfo");
		//市场价格
		String marketPrice = getPara("marketPrice");
		//销售价格
		String sellPrice = getPara("sellPrice");
		//商品标签
		String[] tags = getParaValues("tags[]");
		//优惠类型
		int type = getParaToInt("type");
		//限购量
		String urv = getPara("urv");
		if(StrKit.isBlank(id)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(StrKit.isBlank(spec)){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(marketPrice)){
			renderResult(4);
			return;
		}else if(StrKit.isBlank(sellPrice)){
			renderResult(5);
			return;
		}else if((type==3||type==4)&&StrKit.isBlank(urv)){
			renderResult(6);
			return;
		}else{
			Goods goods=new Goods()
					.set("id", id)
					.set("name", name)
					.set("spec", spec)
					.set("shortInfo", shortInfo)
					.set("marketPrice", marketPrice)
					.set("sellPrice", sellPrice)
					.set("type", type);
			if(type==3||type==4){//限量或者是一元购
				goods.set("urv", Integer.valueOf(urv));
			}
			//保存
			if(service.update(goods,tags)){
				//生成索引
				LuceneKit.createIndex();
				renderSuccess(service.findById(Integer.valueOf(id)));
				return;
			}
		}
		renderFailed();
	}
	
}
