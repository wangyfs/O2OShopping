package cn.com.dashihui.seller.controller;

import org.apache.log4j.Logger;

import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;

import cn.com.dashihui.seller.common.Constants;
import cn.com.dashihui.seller.dao.Seller;
import cn.com.dashihui.seller.domain.AjaxResult;

public class BaseController extends Controller{
	private Logger logger = Logger.getLogger(getClass());
	
	/**
	 * 获取当前登录用户
	 */
	public Seller getCurrentSeller(){
		return getSessionAttr(Constants.USER);
	}
	public int getCurrentSellerid(){
		return ((Seller)getSessionAttr(Constants.USER)).getInt("id");
	}
	public int getCurrentStoreid(){
		return ((Seller)getSessionAttr(Constants.USER)).getInt("storeid");
	}
	
	/**
	 * 返回json格式内容
	 */
	public void renderResult(int flag){
		logger.error(JsonKit.toJson(new AjaxResult(flag)));
		this.renderText(JsonKit.toJson(new AjaxResult(flag)));
	}
	
	public void renderResult(int flag,String message){
		logger.error(JsonKit.toJson(new AjaxResult(flag,message)));
		this.renderText(JsonKit.toJson(new AjaxResult(flag,message)));
	}
	
	public void renderResult(int flag,String message,Object object){
		logger.error(JsonKit.toJson(new AjaxResult(flag,message,object)));
		this.renderText(JsonKit.toJson(new AjaxResult(flag,message,object)));
	}
	
	public void renderResult(int flag,Object object){
		logger.error(JsonKit.toJson(new AjaxResult(flag,object)));
		this.renderText(JsonKit.toJson(new AjaxResult(flag,object)));
	}
	
	public void renderSuccess(){
		renderResult(AjaxResult.FLAG_SUCCESS);
	}
	
	public void renderSuccess(String message){
		renderResult(AjaxResult.FLAG_SUCCESS,message);
	}
	
	public void renderSuccess(Object object){
		renderResult(AjaxResult.FLAG_SUCCESS,object);
	}
	
	public void renderSuccess(String message,Object object){
		renderResult(AjaxResult.FLAG_SUCCESS,message,object);
	}
	
	public void renderFailed(){
		renderResult(AjaxResult.FLAG_FAILED);
	}
	
	public void renderFailed(String message){
		renderResult(AjaxResult.FLAG_FAILED,message);
	}
}
