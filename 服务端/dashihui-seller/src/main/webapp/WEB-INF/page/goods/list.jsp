<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.seller.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<title>商品管理</title>
	<!-- amazeui -->
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
	<!-- app -->
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
	<style type="text/css">
		.app-cart-list{
			padding:5px 0;
			background-color:#fff;
		}
		.app-cart-item{
			padding:5px 0;
		}
	</style>
</head>
<body>
		<header class="am-header am-header-default app-header" data-am-sticky>
			<h1 class="am-header-title">商品管理</h1>
			<input type="hidden" id="goodsType" value="${goodsType }"/>
			<input type="hidden" id="categoryonid" value="${categoryonid }"/>
			<input type="hidden" id="categorytwid" value="${categorytwid }"/>
			<input type="hidden" id="categorythid" value="${categorythid }"/>
			<input type="hidden" id="categoryfoid" value="${categoryfoid }"/>
			<input type="hidden" id="state" value="${state }"/>
			<input type="hidden" id="keyword" value="${keyword }"/>
			<input type="hidden" id="tagsid" value="${tagsid }"/>
			<div class="am-header-left am-header-nav">
				<a href="javascript:history.go(-1);"><i class="am-icon-angle-left am-icon-lg"></i></a>
			</div>
		</header>
		<div class="emptyPart" style="display: none;"></div>
		<div class="notEmptyPart">
			<div class="app-cart-list" id="goodsList"></div>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as goods}}
				<div id="item{{goods.id}}" class="app-cart-item am-g">
					<div class="am-u-sm-3 am-u-md-4 am-u-lg-4">
						<a href="javascript:void(0);">
							<img data-original="${FTP_PATH}{{goods.thumb}}" class="am-img-thumbnail lazyload" width="100%">
						</a>
					</div>
					<div class="am-u-sm-9 am-u-md-8 am-u-lg-8 app-p-l-5 app-p-r-5">
						<div class="app-cart-item-title">{{goods.name}}</div>
						<div class="app-m-t-5">
							<div class="am-fl">市场价：<i class="am-icon-rmb">  {{goods.sellPrice}}</i></div>
							<div class="am-fl app-m-l-10">原价：<i class="am-icon-rmb"></i>  {{goods.marketPrice}}</div>
							<div class="am-cf"></div>
						</div>
						<div class="app-m-t-5">
							<div class="am-fl">
								<input type="button" value="编辑" class="am-btn am-btn-primary am-radius" onclick="toEdit('{{goods.id}}');"/>
							</div>
							<div class="am-fl app-m-l-10">
								{{if goods.state == 1}}
									<input type="button" value="下架" onclick="changeState(this,'{{goods.id}}');" id="stateId" lang="{{goods.state}}" class="am-btn am-btn-secondary am-radius"/>
								{{else}}
									<input type="button" value="上架" onclick="changeState(this,'{{goods.id}}');" id="stateId" lang="{{goods.state}}" class="am-btn am-btn-secondary am-radius"/>
								{{/if}}
							</div>
							<div class="am-cf"></div>
						</div>
					</div>
				</div>
				<hr/>
			{{/each}}
		</script>
		<script type="text/html" id="noData">
			<div>
				<div>
					<div class="am-text-center app-m-t-10">未找到您要查询的商品!</div>
				</div>
			</div>
		</script>
		<script type="text/javascript">
			var pageNum = 0,totalPage = 1;
			//加载标识，表示当前是否有请求未完成，防止同时多个请求
			var goodsType = $("#goodsType").val();
			var categoryonid = $("#categoryonid").val();
			var categorytwid = $("#categorytwid").val();
			var categorythid = $("#categorythid").val();
			var categoryfoid = $("#categoryfoid").val();
			var state = $("#state").val();
			var keyword = $("#keyword").val();
			var tagsid = $("#tagsid").val();
			var loading = false;
			$(function(){
	       		var params = {pageSize:10,goodsType:goodsType,categoryonid:categoryonid,categorytwid:categorytwid,categorythid:categorythid,categoryfoid:categoryfoid,state:state,keyword:keyword,tagsid:tagsid};
				Kit.util.onPageEnd(function(){
	               	if(pageNum < totalPage && !loading){
	               		loading = true;
	               		$("#goodsList").append("<div class=\"app-loading\">正在加载</div>");
	                	Kit.ajax.post("${BASE_PATH}/goods/page",$.extend(params,{pageNum:pageNum+1}),function(result){
							$("#goodsList").append(template("dataTpl",result.object));
							//图片延迟加载
							$("img.lazyload","#goodsList").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							$(".app-loading","#goodsList").remove();
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
							//没找到数据
							if(totalPage==0) {
								$(".emptyPart").append(template("noData")).show();
							}
							//重置加载标识
							loading = false;
						});
	               	}
				});
			});
			function changeState(obj,goodsid) {
				Kit.ui.confirm("确定要下架/上架该商品吗？", function(){
					var state = obj.lang;
					if(state == '' || state == null || state == undefined){
						return;
					}
					Kit.ajax.post("${BASE_PATH}/goods/updateState",{state:state,goodsid:goodsid},function(result){
						if(result.flag == 0) {
							Kit.ui.alert("修改成功!", function(){
								history.go(0);
							});
						} else {
							Kit.ui.toast("修改失败，请重试!");
						}
					});
				},null);
			}
			function toEdit(id){
				window.location.href="${BASE_PATH}/goods/toEdit/"+id;
			}
		</script>
</body>
</html>