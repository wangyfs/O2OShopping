<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>登录</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<style type="text/css">
			.app-logo{
				margin-top:30px;
				margin-bottom:40px;
			}
			.app-btn-link{
				color:#555;
				font-size:12px;
			}
		</style>
	</head>
	<body>
		<img src="${BASE_PATH}/static/app/img/logo.png" class="am-center app-logo" width="170">
		<div class="app-p-10 app-m-t-10">
			<form class="am-form">
				<div class="am-form-group am-form-icon">
					<i class="am-icon-mobile"></i>
					<input id="phoneInpt" type="text" class="am-form-field" placeholder="手机号" maxlength="11">
				</div>
				<div class="am-form-group am-form-icon">
					<i class="am-icon-lock"></i>
					<input id="passwordInpt" type="password" class="am-form-field" placeholder="密码" maxlength="18">
				</div>
			    <input type="button" class="am-btn am-btn-danger am-btn-block" value="登录" onclick="doLogin()">
				<div class="am-cf"></div>
			</form>
		</div>

		<!-- include -->
		<%@include file="include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript">
			function doLogin(){
				var phone = $("#phoneInpt").val();
				var password = $("#passwordInpt").val();
				Kit.ajax.post("${BASE_PATH}/doLogin",{phone:phone,password:password},function(result){
					if(result.flag==1){
						Kit.ui.alert("手机号不能为空！");
					}else if(result.flag==2){
						Kit.ui.alert("手机号不正确！");
					}else if(result.flag==3){
						Kit.ui.alert("密码不能为空！");
					}else if(result.flag==4){
						Kit.ui.alert("用户不存在或密码错误！");
					}else if(result.flag==0){
						Kit.render.redirect("${BASE_PATH}/index");
					}
				});
			}
		</script>
	</body>
</html>