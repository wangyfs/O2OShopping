<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.seller.common.OrderCode,cn.com.dashihui.seller.common.SellerState.DispatchState" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>订单详情</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<style type="text/css">
			.goods-list .app-list-item{
				line-height:23px;
			}
		</style>
	</head>
	<body>
		<header class="am-header app-header">
			<h1 class="am-header-title app-title">订单详情</h1>
			<div class="am-header-left am-header-nav">
				<a href="javascript:history.go(-1);"><i class="am-icon-angle-left am-icon-lg"></i></a>
			</div>
		</header>
		
		<div id="panel"></div>

		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			<div class="app-list app-m-t-10">
				<div class="app-list-item">
					<label>收货人：</label><span>{{linkName}}</span>
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					<label>电话：</label><span>{{tel}}</span>
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					<label>地址：</label><span>{{address}}</span>
				</div>
			</div>
			
			<div class="goods-list app-list app-m-t-10">
				<div class="app-list-item">
					<div class="am-u-sm-12 am-u-md-12 am-u-lg-12">商品清单</div>
				</div>
				{{each goodsList as goods}}
				<hr/>
				<div class="app-list-item app-m-t-10">
					<div class="am-u-sm-3 am-u-md-4 am-u-lg-6" style="padding:2px;">
						<a href="javascript:void(0)" onclick="photo('${FTP_PATH}{{goods.thumb}}')"><img data-original="${FTP_PATH}{{goods.thumb}}" width="100%" class="lazyload"/></a>
					</div>
					<div class="am-u-sm-9 am-u-md-8 am-u-lg-6" style="padding:2px;">
						<div class="am-u-sm-12 am-u-md-12 am-u-lg-12">{{goods.name}}</div>
						<div class="am-u-sm-4 am-u-md-4 am-u-lg-4">单：<i class="am-icon-rmb"></i> {{goods.price}}</div>
						<div class="am-u-sm-4 am-u-md-4 am-u-lg-4">量： {{goods.count}}</div>
						<div class="am-u-sm-4 am-u-md-4 am-u-lg-4">计：<i class="am-icon-rmb"></i> {{goods.amount}}</div>
						{{if orderState==<%=OrderCode.OrderState.NORMAL%> && goods.payType==<%=OrderCode.OrderPayType.ON_LINE%> && goods.payState==<%=OrderCode.OrderPayState.HAD_PAY%> && packState!=<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							{{if goods.refundState==0}}
								<div class="am-fr">
									<input type="button" id="btnRefund" onclick="doRefundSingle('{{goods.id}}');" value="退款" class="am-btn am-btn-danger am-radius am-btn-xs"/>
								</div>
							{{else if goods.refundState==<%=OrderCode.RefundState.VERIFYING%>}}
								<div class="am-fr app-text-13px" style="color:#a2a2a2;">审核中</div>
							{{else if goods.refundState==<%=OrderCode.RefundState.VERIFIED%>}}
								<div class="am-fr app-text-13px" style="color:#a2a2a2;">审核通过</div>
							{{else if goods.refundState==<%=OrderCode.RefundState.VERIFY_FAILED%>}}
								<div class="am-fr app-text-13px" style="color:#a2a2a2;">审核不通过</div>
							{{else if goods.refundState==<%=OrderCode.RefundState.REFUNDED%>}}
								<div class="am-fr app-text-13px" style="color:#a2a2a2;">已退款</div>
							{{/if}}
						{{/if}}
						<div class="am-cf"></div>
					</div>
					<div class="am-cf"></div>
				</div>
				{{/each}}
				<hr/>
				<div class="app-list-item app-m-t-10">
					<div class="am-text-right">总计：<i class="am-icon-rmb"></i> {{amount}}</div>
				</div>
			</div>

			<div class="app-list app-m-t-10">
				<div class="app-list-item">
					<label>备注：</label><span>{{describe}}</span>
				</div>
			</div>
			
			<div class="app-list app-m-t-10">
				<div class="app-list-item">
					<label>订单号：</label><span>{{orderNum}}</span>
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					<label>下单时间：</label><span>{{createDate}}</span>
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					{{if payType==<%=OrderCode.OrderPayType.ON_LINE%>}}
					<label>支付方式：</label><span>在线支付</span>
					{{else}}
					<label>支付方式：</label><span>货到付款</span>
					{{/if}}
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					{{if takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
					<label>收货方式：</label><span>门店配送</span>
					{{else}}
					<label>收货方式：</label><span>上门自取</span>
					{{/if}}
				</div>
				<hr/>
				<div class="app-list-item app-m-t-10">
					<label>在线支付：</label><span><i class="am-icon-rmb"></i> {{amount}}</span>
				</div>
				{{if singleRefundAmount!=0&&singleRefundAmount!=null}}
				<hr/>
				<div class="app-list-item app-m-t-10">
					<label>缺货退款：</label><span><i class="am-icon-rmb"></i> {{singleRefundAmount}}</span>
				</div>
				{{/if}}
				{{if changeRefundAmount!=0&&changeRefundAmount!=null}}
				<hr/>
				<div class="app-list-item app-m-t-10">
					<label>零钱退款：</label><span><i class="am-icon-rmb"></i> {{changeRefundAmount}}</span>
				</div>
				{{/if}}
				<hr/>
				<div class="app-list-item app-m-t-10">
					{{if orderState==<%=OrderCode.OrderState.CANCEL%>}}
					<!-- 状态0：已取消 -->
					<label>当前状态：</label><span class="app-state">已取消</span>
					{{else if state==<%=DispatchState.UNACCEPT%>}}
					<!-- 状态1：未接单 -->
					<label>当前状态：</label><span class="app-state">待接单</span>
					{{else if state==<%=DispatchState.WAIT_PACK%>}}
					<!-- 状态2：待配货 -->
					<label>当前状态：</label><span class="app-state">待配货</span>
					{{else if state==<%=DispatchState.PACKING%>}}
					<!-- 状态3：配货中 -->
					<label>当前状态：</label><span class="app-state">配货中</span>
					{{else if state==<%=DispatchState.PACK_FINISH%> && takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
					<!-- 状态4：配货完成 且 订单要求门店配送 -->
					<label>当前状态：</label><span class="app-state">配货完成</span>
					{{else if state==<%=DispatchState.DISPATCHING%>}}
					<!-- 状态5：配送中 -->
					<label>当前状态：</label><span class="app-state">配送中</span>
					{{else if state==<%=DispatchState.WAIT_GET%>}}
					<!-- 状态6：待取货 -->
					<label>当前状态：</label><span class="app-state">待取货</span>
					{{else if state==<%=DispatchState.FINISH%>}}
					<!-- 状态7：已完成 -->
					<label>当前状态：</label><span class="app-state-finish">已完成</span>
					{{/if}}
				</div>
			</div>
			
			<div class="app-p-10">
				{{if orderState==<%=OrderCode.OrderState.CANCEL%>}}
				<a href="javascript:doHide(true);" class="am-btn am-btn-primary am-radius am-btn-block">隐藏</a>
				{{else if state==<%=DispatchState.UNACCEPT%>}}
				<!-- 状态1：未接单，显示接单按钮 -->
				<a href="javascript:doDeal('doAccept');" class="am-btn am-btn-primary am-radius am-btn-block">接单</a>
				{{else if state==<%=DispatchState.WAIT_PACK%>}}
				<!-- 状态2：待配货，显示开始配货按钮 -->
				{{if payType==<%=OrderCode.OrderPayType.ON_LINE%> && payState==<%=OrderCode.OrderPayState.HAD_PAY%>}}
				<a href="javascript:doDeal('doPack');" class="am-btn am-btn-secondary am-radius am-fl" style="width:45%;">开始配货</a>
				<a href="javascript:toRefundChange();" class="am-btn am-btn-warning am-radius am-fr" style="width:45%;">退零钱</a>
				{{else}}
				<a href="javascript:doDeal('doPack');" class="am-btn am-btn-secondary am-radius am-btn-block">开始配货</a>
				{{/if}}
				{{else if state==<%=DispatchState.PACKING%>}}
				<!-- 状态3：配货中，显示配货完成按钮 -->
				{{if payType==<%=OrderCode.OrderPayType.ON_LINE%> && payState==<%=OrderCode.OrderPayState.HAD_PAY%>}}
				<a href="javascript:doDeal('doPackFinish');" class="am-btn am-btn-secondary am-radius am-fl" style="width:45%;">配货完成</a>
				<a href="javascript:toRefundChange();" class="am-btn am-btn-warning am-radius am-fr" style="width:45%;">退零钱</a>
				{{else}}
				<a href="javascript:doDeal('doPackFinish');" class="am-btn am-btn-secondary am-radius am-btn-block">配货完成</a>
				{{/if}}
				{{else if state==<%=DispatchState.PACK_FINISH%> && takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<!-- 状态4：配货完成 且 订单要求门店配送，显示送货按钮 -->
				{{if payType==<%=OrderCode.OrderPayType.ON_LINE%> && payState==<%=OrderCode.OrderPayState.HAD_PAY%>}}
				<a href="javascript:doDeal('doSend');" class="am-btn am-btn-warning am-radius am-fl" style="width:45%;">送货</a>
				<a href="javascript:toRefundChange();" class="am-btn am-btn-warning am-radius am-fr" style="width:45%;">退零钱</a>
				{{else}}
				<a href="javascript:doDeal('doSend');" class="am-btn am-btn-warning am-radius am-btn-block">送货</a>
				{{/if}}
				{{else if state==<%=DispatchState.DISPATCHING%> || state==<%=DispatchState.WAIT_GET%>}}
				<!-- 状态5：配送中 或 待取货，显示确认收货按钮 -->
				{{if payType==<%=OrderCode.OrderPayType.ON_LINE%> && payState==<%=OrderCode.OrderPayState.HAD_PAY%>}}
				<a href="javascript:doDeal('doFinish');" class="am-btn am-btn-success am-radius am-fl" style="width:45%;">确认收货</a>
				<a href="javascript:toRefundChange();" class="am-btn am-btn-warning am-radius am-fr" style="width:45%;">退零钱</a>
				{{else}}
				<a href="javascript:doDeal('doFinish');" class="am-btn am-btn-success am-radius am-btn-block">确认收货</a>
				{{/if}}
				{{/if}}
				<div class="am-cf"></div>
			</div>
		</script>
		<script type="text/javascript">
			$(function(){
				//初始化显示订单信息
				Kit.ajax.post("${BASE_PATH}/order/getDetail",{orderNum:'${orderNum}'},function(result){
					if(result.flag==0){
						//显示订单数据
						render(result.object);
					}else{
						Kit.ui.toast(result.message);
						setTimeout(function(){
							window.history.go(-1);
						},2000);
					}
				});
			});
			function render(order){
				$("#panel").empty().append(template("dataTpl",order));
				//图片延迟加载
				$("img.lazyload","#panel").not(".lazyload-binded").lazyload({
					failurelimit : 100,
					effect : "show"
				}).addClass("lazyload-binded");
			}
			//各种处理
			function doDeal(action){
				Kit.ajax.post("${BASE_PATH}/order/"+action,{orderNum:'${orderNum}'},function(result){
					if(result.flag==0){
						//操作成功后重新显示订单数据
						render(result.object);
					}else if(result.flag==1){
						Kit.ui.confirm("该订单已取消，是否隐藏?",function(){
							doHide('${orderNum}',false);
						},function(){
							$("#panel").replaceWith(template("dataTpl",result.object));
						});
					}else{
						Kit.ui.toast(result.message);
					}
				});
			}
			//弹出显示图片
			function photo(src){
				var popup = '<div class="desc-dialog"><img src='+src+' width="100%"></div>';
				Kit.ui.popup(popup,"padding:0; border:none; padding:5px;");
			}
			//单个商品退款操作
			function doRefundSingle(orderListid) {
				Kit.ajax.post("${BASE_PATH}/order/refundSingle",{orderNum:'${orderNum}',orderListid:orderListid},function(result){
					//-1：退款失败，1：退款成功，2：退款成功+订单已取消，3：订单已取消或该商品已存在退款记录，4：剩余可退金额不足
					if(result.flag == -1){
						Kit.ui.toast("退款失败");
					} else if (result.flag == 1){
						Kit.ui.toast(result.message);
					}else if(result.flag == 2){
						//操作成功后重新显示订单数据
						render(result.object);
						Kit.ui.confirm("退款申请成功，订单所有金额已退清并已取消，是否隐藏?",function(){
							doHide(false);
						});
					}else if(result.flag == 3){
						Kit.ui.toast("订单已取消或该商品已存在退款记录");
					}else if(result.flag == 4){
						Kit.ui.toast("剩余可退金额不足");
					}else if(result.flag == 5){
						Kit.ui.toast("退款申请成功");
						//操作成功后重新显示订单数据
						render(result.object);
					}
				});
			}
			//弹出退零钱窗口
			function toRefundChange(available) {
				Kit.ajax.post("${BASE_PATH}/order/getAvailable",{orderNum:'${orderNum}'},function(result){
					var popup = '<div class="desc-dialog">'
						+ '<form class="am-form">'
						+ '<div class="am-form-group"><div>可退金额：<i class="am-icon-rmb"></i> '+result.object+'</div></div>'
						+ '<div class="am-form-group am-form-icon"><i class="am-icon-cny"></i>'
						+ '<input type="text" id="change" placeholder="请输入退款金额(只能输入两位小数)" maxlength="10" onkeyup="changeVerify(this);" value="" class="am-form-field"></input>'
						+ '</div>'
						+ '<a href="javascript:doRefundChange(\''+result.object+'\');" class="am-btn am-btn-danger am-btn-block" style="margin-bottom:10px;">确定</a>'
						+ '</form>'
						+ '</div>';
					Kit.ui.popup(popup);
				});
			}
			//零钱退款
			function doRefundChange(available) {
				var change = $("#change").val();
				if(Kit.validate.isBlank(change)) {
					Kit.ui.toast("请输入退款金额");
					return;
				}
				var reg = /^(([0-9]{1,5})|([0-9]{1,5}\.[0-9]{1,2}))?$/;
				if(!reg.test(change)){
					Kit.ui.toast("请输入正确的金额");
					return;
				}
				if(parseFloat(change)>parseFloat(available)) {
					Kit.ui.toast("超出可退金额范围");
					return;
				}
				Kit.ajax.post("${BASE_PATH}/order/refundChange",{orderNum:'${orderNum}',change:change},function(result){
					//-1：退款失败，1：退款成功，2：退款成功+订单已取消，3：剩余可退金额不足
					if(result.flag == -1){
						Kit.ui.toast("退款失败");
					}else if(result.flag == 1){
						Kit.ui.toast("退款申请成功");
						//操作成功后重新显示订单数据
						render(result.object);
						Kit.ui.popdown();
					}else if(result.flag == 2){
						//操作成功后重新显示订单数据
						render(result.object);
						Kit.ui.confirm("退款申请成功，订单所有金额已退清并已取消，是否隐藏?",function(){
							doHide('${orderNum}',false);
						});
						Kit.ui.popdown();
					}else if(result.flag == 3){
						Kit.ui.toast("超出可退金额范围");
					}else if(result.flag == 4){
						Kit.ui.toast("退款申请成功");
						render(result.object);
						Kit.ui.popdown();
					}
				});
			}
			//隐藏订单
			function doHide(isNeedConfirm){
				var doHideCallback = function(){
					Kit.ajax.post("${BASE_PATH}/order/doHide",{orderNum:'${orderNum}'}, function(result){
						if(result.flag == 0) {
							window.history.go(-1);
						}else{
							Kit.ui.alert(result.message);
						}
					});
				}
				if(isNeedConfirm){
					Kit.ui.confirm("确定要隐藏该订单吗？",doHideCallback);
				}else{
					doHideCallback();
				}
			}
			//验证零钱只能输入正整数或小数点后两位
			function changeVerify(obj){
				//先把非数字的都替换掉，除了数字和.
				obj.value = obj.value.replace(/[^\d.]/g, "");
				//必须保证第一个为数字而不是.
				obj.value = obj.value.replace(/^\./g, "");
				//保证只有出现一个.而没有多个.
				obj.value = obj.value.replace(/\.{2,}/g, ".");
				//保证.只出现一次，而不能出现两次以上
				obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
				//保证.只后面只能出现两位有效数字
				obj.value = obj.value.replace(/([0-9]+\.[0-9]{2})[0-9]*/, "$1");
			}
		</script>
	</body>
</html>