package cn.com.dashihui.api.controller;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.dao.Store;
import cn.com.dashihui.api.service.StoreService;

public class StoreController extends BaseController{
	
	private StoreService service = new StoreService();
	
	/**
	 * 查询店铺最新一条通知
	 * @param STOREID 店铺ID
	 */
	public void tip(){
		String storeId = getPara("STOREID");
		if(StrKit.isBlank(storeId)){
    		renderFailed("参数ID不能为空");
    		return;
		}
		Record tip = service.findLastTip(Integer.valueOf(storeId));
		if(tip!=null){
			renderSuccess(tip);
			return;
		}else{
    		renderFailed("没有通知信息");
    		return;
		}
	}
	
	/**
	 * 根据ID查找出商铺信息
	 * @param STOREID 店铺ID
	 */
	public void detail(){
		String storeId = getPara("STOREID");
		if(StrKit.isBlank(storeId)){
    		renderFailed("参数ID不能为空");
    		return;
		}
		Store store = service.findById(Integer.valueOf(storeId));
		if(store!=null){
			renderSuccess(store);
			return;
		}else{
    		renderFailed("店铺不存在");
    		return;
		}
	}
}
