package cn.com.dashihui.api.controller;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.dao.User;
import cn.com.dashihui.api.service.SearchService;
import cn.com.dashihui.kit.LuceneKit;

public class SearchController extends BaseController{
	private SearchService service = new SearchService();
	
	/**
	 * 查询指定数量的关键字列表
	 * @param SIZE 要查询的数量
	 */
	public void keywords(){
		String storeidStr = getPara("STOREID");
		int size = getParaToInt("SIZE",5);
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}
		renderSuccess(service.findAllKeyword(Integer.valueOf(storeidStr),size));
	}
	
	/**
	 * 根据关键字分页搜索商品列表
	 * @param STOREID 店铺ID
	 * @param KEYWORDID 关键字ID
	 * @param KEYWORD 关键字
	 * @param PAGENUM 页码
	 * @param PAGESIZE 数量
	 * @param ORDERBY 排序依据，1：默认，2：销售价降序，3：销售价升序
	 */
	public void doSearch(){
		String storeidStr = getPara("STOREID");
		String keywordidStr = getPara("KEYWORDID");
		String keyword = getPara("KEYWORD");
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		int isSelf = getParaToInt("ISSELF",0);
		int orderBy = getParaToInt("ORDERBY",0);
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else if(StrKit.isBlank(keyword)){
    		renderFailed("参数KEYWORD不能为空");
    		return;
		}else{
			if(!StrKit.isBlank(keywordidStr)){
				//如果传的有关键字ID参数，则说明用户是点击系统推荐的关键字进行的搜索，此时要修改对应关键字点击量加1
				service.updateKeywordAmount(Integer.valueOf(keywordidStr));
			}else{
				//相反则为用户自定义搜索关键字，此时要进行记录，如果用户登录了，则同时记录用户ID
				User user = getCurrentUser();
				service.logKeyword(Integer.valueOf(storeidStr),user!=null?user.getInt("id"):0, keyword);
			}
			renderSuccess(LuceneKit.search(Integer.valueOf(storeidStr), isSelf, keyword, pageNum, pageSize, orderBy));
		}
	}
}
