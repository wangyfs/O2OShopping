package cn.com.dashihui.api.dao;

import com.jfinal.plugin.activerecord.Model;

/**
 * 服务订单接口日志对象
 */
public class ServiceOrderPayAPIRecord extends Model<ServiceOrderPayAPIRecord>{
	private static final long serialVersionUID = 1L;
	private static ServiceOrderPayAPIRecord me = new ServiceOrderPayAPIRecord();
	public static ServiceOrderPayAPIRecord me(){
		return me;
	}

}
