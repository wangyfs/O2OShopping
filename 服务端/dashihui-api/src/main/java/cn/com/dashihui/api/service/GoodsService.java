package cn.com.dashihui.api.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.common.OrderCode;

public class GoodsService {
	
	/**
	 * 根据优惠类型查询各类型一件商品
	 * @param type 优惠类型，1：普通，2：推荐，3：限量，4：一元购
	 * @param storeid 店铺ID
	 */
	public Record topByType(int storeid,int type){
		return Db.findFirst("SELECT g.id,g.name,g.thumb,g.spec,g.marketPrice,g.sellPrice,g.urv,g.isSelf FROM t_bus_goods g WHERE g.state=1 AND g.type=? AND g.storeid=? ORDER BY rand()",type,storeid);
	}
	
	/**
	 * 查询商品列表
	 * @param storeid 店铺ID
	 * @param categoryCode 主分类代码
	 * @param categorytwCode 二级分类代码
	 * @param type 优惠类型
	 * @param pageNum 页码
	 * @param pageSize 数量
	 * @param ORDERBY 排序，1：默认排序，2：销量从高到低，3：价格从低到高，4：价格从高到低
	 */
	public Page<Record> findByPage(int storeid,String categoryonCode,String categorytwCode,int type,int pageNum,int pageSize,int orderBy){
		String sqlSelect = "SELECT g.id,g.name,g.thumb,g.spec,g.marketPrice,g.sellPrice,g.urv,g.isSelf,g.isRebate ";
		StringBuffer sqlFromSelect = new StringBuffer("FROM t_bus_goods g");
		StringBuffer sqlWhereSelect = new StringBuffer(" WHERE g.state=1 AND g.storeid=?");
		List<Object> params = new ArrayList<Object>();
		params.add(storeid);
		//判断优惠类型不为空且不为1：普通类型时
		if(type!=0&&type!=1){
			sqlWhereSelect.append(" AND g.type=?");
			params.add(type);
		}
		//判断如果分类不为空，则添加条件
		if(!StrKit.isBlank(categoryonCode)){
			sqlFromSelect.append(" INNER JOIN t_dict_category c1 ON g.categoryonid=c1.categoryId");
			sqlWhereSelect.append(" AND c1.categoryNum=?");
			params.add(categoryonCode);
		}
		if(!StrKit.isBlank(categorytwCode)){
			sqlFromSelect.append(" INNER JOIN t_dict_category c2 ON g.categorytwid=c2.categoryId");
			sqlWhereSelect.append(" AND c2.categoryNum=?");
			params.add(categorytwCode);
		}
		//根据排序要求，作不同处理
		if(orderBy==1){
		}else if(orderBy==2){
			sqlSelect = sqlSelect + ",(SELECT COUNT(*) FROM t_bus_order bo INNER JOIN t_bus_order_list bol ON bo.orderNum=bol.orderNum WHERE bol.goodsid=g.id AND bo.orderState="+OrderCode.OrderState.FINISH+") saleCount ";
			sqlWhereSelect.append(" ORDER BY saleCount DESC");
		}else if(orderBy==3){
			sqlWhereSelect.append(" ORDER BY g.sellPrice ASC");
		}else if(orderBy==4){
			sqlWhereSelect.append(" ORDER BY g.sellPrice DESC");
		}
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlFromSelect.append(sqlWhereSelect).toString(), params.toArray());
	}
	public Page<Record> findByPage_v133(int storeid,String categoryonCode,String categorytwCode,int type,int pageNum,int pageSize,int orderBy,int isSelf){
		String sqlSelect = "SELECT g.id,g.name,g.thumb,g.spec,g.marketPrice,g.sellPrice,g.urv,g.isSelf,g.isRebate ";
		StringBuffer sqlFromSelect = new StringBuffer("FROM t_bus_goods g");
		StringBuffer sqlWhereSelect = new StringBuffer(" WHERE g.state=1 ");
		List<Object> params = new ArrayList<Object>();
		if(isSelf == 1){
			sqlWhereSelect.append(" AND g.isSelf=1 ");
		} else {
			sqlWhereSelect.append(" AND (g.storeid=? OR g.isSelf=1)");
			params.add(storeid);
		}
		//判断优惠类型不为空且不为1：普通类型时
		if(type!=0&&type!=1){
			sqlWhereSelect.append(" AND g.type=?");
			params.add(type);
		}
		//判断如果分类不为空，则添加条件
		if(!StrKit.isBlank(categoryonCode)){
			sqlFromSelect.append(" INNER JOIN t_dict_category c1 ON g.categoryonid=c1.categoryId");
			sqlWhereSelect.append(" AND c1.categoryNum=?");
			params.add(categoryonCode);
		}
		if(!StrKit.isBlank(categorytwCode)){
			sqlFromSelect.append(" INNER JOIN t_dict_category c2 ON g.categorytwid=c2.categoryId");
			sqlWhereSelect.append(" AND c2.categoryNum=?");
			params.add(categorytwCode);
		}
		//根据排序要求，作不同处理
		if(orderBy==1){
		}else if(orderBy==2){
			sqlSelect = sqlSelect + ",(SELECT COUNT(*) FROM t_bus_order bo INNER JOIN t_bus_order_list bol ON bo.orderNum=bol.orderNum WHERE bol.goodsid=g.id AND bo.orderState="+OrderCode.OrderState.FINISH+") saleCount ";
			sqlWhereSelect.append(" ORDER BY saleCount DESC");
		}else if(orderBy==3){
			sqlWhereSelect.append(" ORDER BY g.sellPrice ASC");
		}else if(orderBy==4){
			sqlWhereSelect.append(" ORDER BY g.sellPrice DESC");
		}
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlFromSelect.append(sqlWhereSelect).toString(), params.toArray());
	}
	
	/**
	 * 查询指定店铺的所有可用且可显示的商品标签，以及各标签下N条商品数据
	 * @param storeid
	 * @param max 要随标签显示的商品最大数量
	 */
	public List<Record> getAllTag(int storeid,int max){
		List<Record> list = Db.find("SELECT gt.tagName,gt.code tagCode,gt.orderNo,g.id,g.name,g.thumb,g.marketPrice,g.sellPrice"
				+ " FROM t_bus_goods g"
				+ " INNER JOIN t_bus_goods_tag_rel gtr ON g.id=gtr.goodsid"
				+ " INNER JOIN t_bus_goods_tag gt ON gtr.tagid=gt.id AND gt.storeid=?"
				+ " WHERE g.state=1 AND g.storeid=? AND gt.enabled=1 AND gt.isShow=1"
				+ " ORDER BY gt.orderNo",storeid,storeid);
		if(list!=null){
			Map<String,Record> tags = new LinkedHashMap<String,Record>();
			for(Record row : list){
				String tagCode = row.get("TAGCODE");
				Record tag = tags.get(tagCode);
				List<Record> goodsList;
				if(tag==null){
					goodsList = new ArrayList<Record>();
					tags.put(tagCode, new Record()
							.set("TAGNAME", row.get("TAGNAME"))
							.set("TAGCODE", row.get("TAGCODE"))
							.set("LIST", goodsList));
				}else{
					goodsList = tag.get("LIST");
					//查询的商品数量如果大于最大值，则跳过
					if(goodsList.size()>=max){
						continue;
					}
				}
				goodsList.add(row);
			}
			return Lists.newArrayList(tags.values().iterator());
		}
		return null;
	}
	
	/**
	 * 查询指定标签的商品列表
	 * @param storeid 店铺ID
	 * @param tagCode 标签代码
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public Page<Record> findPageByTag(int storeid,String tagCode,int pageNum,int pageSize){
		String sqlSelect = "SELECT g.id,g.name,g.thumb,g.marketPrice,g.sellPrice,g.isSelf ";
		String sqlWhere = " FROM t_bus_goods g"
				+ " INNER JOIN t_bus_goods_tag_rel gtr ON g.id=gtr.goodsid"
				+ " INNER JOIN t_bus_goods_tag gt ON gtr.tagid=gt.id AND gt.storeid=?"
				+ " WHERE g.state=1 AND g.storeid=? AND gt.code=? AND gt.enabled=1 AND gt.isShow=1";
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlWhere, storeid, storeid, tagCode);
	}
	
	/**
	 * 查询精品推荐的商品列表
	 * @param storeid 店铺ID
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public Page<Record> findPageByRecom(int storeid,int pageNum,int pageSize){
		String sqlSelect = "SELECT g.id,g.name,g.thumb,g.marketPrice,g.sellPrice,g.isSelf ";
		String sqlWhere = " FROM t_bus_goods g"
				+ " WHERE g.state=1 AND (g.storeid=? OR g.isSelf=1) AND g.type=2";
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlWhere, storeid);
	}
	
	/**
	 * 查询商品信息
	 * @param storeid 店铺ID
	 * @param goodsid 商品ID
	 */
	public Record findDetail(int storeid,int goodsid){
		String sql = "SELECT vg.id,vg.storeid,vg.name,vg.spec,vg.shortInfo,vg.marketPrice,vg.sellPrice,vg.thumb,vg.state,vg.type,vg.urv,vg.createDate,"
				+ "vg.categoryonid,vg.categoryonName,vg.categoryonCode,vg.categorytwid,vg.categorytwName,vg.categorytwCode,"
				+ "vg.categorythid,vg.categorythName,vg.categorythCode,vg.categoryfoid,vg.categoryfoName,vg.categoryfoCode,"
				+ "vg.brandid,vg.brandName,vg.isSelf,vg.isRebate,vg.percent1,vg.percent2,vg.percent3,vg.percent4,vg.percent5,"
				+ "(CASE WHEN vg.describe IS NULL OR length(vg.describe)<10 THEN 0 ELSE 1 END) hasDescribe,"
				+ "0 isCollected,"
				+ "(SELECT COUNT(*) FROM t_bus_order bo INNER JOIN t_bus_order_list bol ON bo.orderNum=bol.orderNum WHERE bol.goodsid=? AND bo.orderState=?) saleCount,"
				+ "(SELECT COUNT(*) FROM t_bus_user_collection buc WHERE buc.goodsid=? AND buc.isCancel=0) collectedCount "
				+ " FROM v_goods vg WHERE id=? AND (storeid=? OR isSelf=1)";
		return Db.findFirst(sql,goodsid,OrderCode.OrderState.FINISH,goodsid,goodsid,storeid);
	}
	
	/**
	 * 查询商品信息，并且查询该商品是否被当前登录用户收藏
	 * @param userid 当前登录用户ID
	 * @param storeid 店铺ID
	 * @param goodsid 商品ID
	 */
	public Record findDetail(int userid,int storeid,int goodsid){
		String sql = "SELECT vg.id,vg.storeid,vg.name,vg.spec,vg.shortInfo,vg.marketPrice,vg.sellPrice,vg.thumb,vg.state,vg.type,vg.urv,vg.createDate,"
				+ "vg.categoryonid,vg.categoryonName,vg.categoryonCode,vg.categorytwid,vg.categorytwName,vg.categorytwCode,"
				+ "vg.categorythid,vg.categorythName,vg.categorythCode,vg.categoryfoid,vg.categoryfoName,vg.categoryfoCode,"
				+ "vg.brandid,vg.brandName,vg.isSelf,vg.isRebate,vg.percent1,vg.percent2,vg.percent3,vg.percent4,vg.percent5,"
				+ "(CASE WHEN vg.describe IS NULL OR length(vg.describe)<10 THEN 0 ELSE 1 END) hasDescribe,"
				+ "(SELECT COUNT(*) FROM t_bus_user_collection buc WHERE buc.userid=? AND buc.goodsid=? AND buc.isCancel=0)!=0 isCollected,"
				+ "(SELECT COUNT(*) FROM t_bus_order bo INNER JOIN t_bus_order_list bol ON bo.orderNum=bol.orderNum WHERE bol.goodsid=? AND bo.orderState=?) saleCount,"
				+ "(SELECT COUNT(*) FROM t_bus_user_collection buc WHERE buc.goodsid=? AND buc.isCancel=0) collectedCount "
				+ " FROM v_goods vg WHERE id=? AND (storeid=? OR isSelf=1)";
		return Db.findFirst(sql,userid,goodsid,goodsid,OrderCode.OrderState.FINISH,goodsid,goodsid,storeid);
	}
	
	/**
	 * 查询商品图片集
	 * @param goodsid 商品ID
	 */
	public List<Record> findImages(int goodsid){
		return Db.find("SELECT thumb FROM t_bus_goods_images sgi WHERE goodsid=? ORDER BY orderNo",goodsid);
	}
	
	/**
	 * 查询商品信息描述
	 * @param storeid 店铺ID
	 * @param goodsid 商品ID
	 */
	public Record findDetailDescribe(int storeid,int goodsid){
		return Db.findFirst("SELECT `describe` FROM t_bus_goods WHERE id=? AND storeid=?",goodsid,storeid);
	}
}
