package cn.com.dashihui.web.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.kit.ListKit;
import cn.com.dashihui.web.dao.Brand;
import cn.com.dashihui.web.dao.Category;
import cn.com.dashihui.web.dao.City;
import cn.com.dashihui.web.dao.CommunityDetail;
import cn.com.dashihui.web.dao.GoodsTag;
import cn.com.dashihui.web.dao.Store;

public class APIService {
	
	public List<Brand> getAllBrand(){
		return Brand.me().find("SELECT id,name FROM t_dict_brand");
	}
	
	public List<City> getAllProvince(){
		return City.me().find("SELECT id,name FROM t_dict_city WHERE type=1");
	}
	
	public List<City> getAllCityByProvince(int provinceid){
		return City.me().find("SELECT id,name FROM t_dict_city WHERE type=2 AND parentid=?",provinceid);
	}
	
	public List<City> getAllAreaByCity(int cityid){
		return City.me().find("SELECT id,name FROM t_dict_city WHERE type=3 AND parentid=?",cityid);
	}
	
	public List<Category> getCategoryByParent(int parentid){
		return Category.me().find("SELECT categoryId id,categoryName name FROM t_dict_category WHERE categoryFatherId=? ORDER BY categoryNo",parentid);
	}
	
	public Page<Store> getStorePage(int p,int c,int a,String keyword,int pageNum,int pageSize){
		StringBuffer sqlExcept = new StringBuffer(" FROM t_dict_store A WHERE 1=1");
		ListKit<Object> params = new ListKit<Object>();
		if(a!=0){
			sqlExcept.append(" AND province=? AND city=? AND area=?");
			params.add(p).add(c).add(a);
		}else if(c!=0){
			sqlExcept.append(" AND province=? AND city=?");
			params.add(p).add(c);
		}else if(p!=0){
			sqlExcept.append(" AND province=?");
			params.add(p);
		}
		if(!StrKit.isBlank(keyword)){
			sqlExcept.append(" AND (title LIKE ? OR address LIKE ?)");
			params.add("%"+keyword+"%").add("%"+keyword+"%");
		}
		sqlExcept.append(" ORDER BY A.orderNo DESC");
		return Store.me().paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(), params.toArray());
	}
	
	public Page<Record> getGoodsPage(int storeid,int c1,int c2,int c3,int c4,String keyword,int pageNum,int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM (SELECT A.* FROM v_goods A WHERE A.storeid=?");
		ListKit<Object> params = new ListKit<Object>();
		params.add(storeid);
		if(c4!=0){
			sqlExcept.append(" AND A.categoryonid=? AND A.categorytwid=? AND A.categorythid=? AND A.categoryfoid=?");
			params.add(c1).add(c2).add(c3).add(c4);
		}else if(c3!=0){
			sqlExcept.append(" AND A.categoryonid=? AND A.categorytwid=? AND A.categorythid=?");
			params.add(c1).add(c2).add(c3);
		}else if(c2!=0){
			sqlExcept.append(" AND A.categoryonid=? AND A.categorytwid=?");
			params.add(c1).add(c2);
		}else if(c1!=0){
			sqlExcept.append(" AND A.categoryonid=?");
			params.add(c1);
		}
		if(!StrKit.isBlank(keyword)){
			sqlExcept.append(" AND (A.name LIKE ? OR A.shortInfo LIKE ?)");
			params.add("%"+keyword+"%").add("%"+keyword+"%");
		}
		sqlExcept.append(") A");
		sqlExcept.append(" ORDER BY A.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(), params.toArray());
	}
	
	public Page<Record> getSellerPage(int storeid,String phone,String keyword,int pageNum,int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_dict_store_seller A WHERE A.storeid=?");
		ListKit<Object> params = new ListKit<Object>();
		params.add(storeid);
		if(!StrKit.isBlank(phone)){
			sqlExcept.append(" AND A.tel=?");
			params.add(phone);
		}
		if(!StrKit.isBlank(keyword)){
			sqlExcept.append(" AND A.name LIKE ?");
			params.add("%"+keyword+"%");
		}
		sqlExcept.append(" ORDER BY A.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(), params.toArray());
	}
	
	public List<CommunityDetail> getAllBuildByStore(int storeid){
		return CommunityDetail.me().find("SELECT cd.id,cd.name FROM t_dict_community_detail cd INNER JOIN t_dict_community c ON cd.communityid=c.id AND c.storeid=? WHERE type=1",storeid);
	}
	
	public List<CommunityDetail> getAllUnitByParent(int buildid){
		return CommunityDetail.me().find("SELECT id,name FROM t_dict_community_detail WHERE parentid=? AND type=2",buildid);
	}

	public List<CommunityDetail> getAllRoomByParent(int unitid){
		return CommunityDetail.me().find("SELECT id,name FROM t_dict_community_detail WHERE parentid=? AND type=3",unitid);
	}
	
	public List<GoodsTag> getAllGoodsTag(int storeid){
		return GoodsTag.me().find("SELECT A.id,A.tagName name FROM t_bus_goods_tag A WHERE A.enabled=1 AND A.storeid=? ORDER BY A.orderNo",storeid);
	}
}
