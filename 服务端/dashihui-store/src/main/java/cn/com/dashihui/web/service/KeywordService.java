package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Keyword;

public class KeywordService {
	
	public boolean sortKeyword(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_dict_keyword SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public Page<Record> findByPage(int storied, int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_dict_keyword A WHERE A.storeid=?");
		sqlExcept.append(" ORDER BY A.orderNo");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(), storied);
	}
	
	public boolean addKeyword(Keyword newObject){
		return newObject.save();
	}
	
	public boolean editKeyword(Keyword newObject){
		return newObject.update();
	}
	
	public boolean delKeyword(int id){
		return Keyword.me().deleteById(id);
	}
	
	public Keyword findById(int id){
		return Keyword.me().findFirst("SELECT * FROM t_dict_keyword WHERE id=?",id);
	}
}
