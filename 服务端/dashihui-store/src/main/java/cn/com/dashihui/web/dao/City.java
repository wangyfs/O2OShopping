package cn.com.dashihui.web.dao;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class City extends Model<City>{
	private static final long serialVersionUID = 1L;
	private static City me = new City();
	public static City me(){
		return me;
	}
	
	//子级地市
	private boolean hasChildren = false;
	public List<City> getChildren() {
		return get("children");
	}
	public void setChildren(List<City> children) {
		put("children", children);
		hasChildren = true;
	}
	public boolean hasChildren(){
		return hasChildren;
	}
	public void addChild(City child){
		List<City> children = getChildren();
		if(children==null){
			children = new ArrayList<City>();
		}
		children.add(child);
		setChildren(children);
		hasChildren = true;
	}
	
	//子级便利店，只有县/区才有子级便利店数据
	private boolean hasStore = false;
	public List<Store> getStoreList() {
		return get("storeList");
	}
	public void setStoreList(List<Store> storeList) {
		put("storeList", storeList);
		hasStore = true;
	}
	public boolean hasStoreList(){
		return hasStore;
	}
	public void addStore(Store store){
		List<Store> storeList = getStoreList();
		if(storeList==null){
			storeList = new ArrayList<Store>();
		}
		storeList.add(store);
		setStoreList(storeList);
		hasStore = true;
	}
}
