package cn.com.dashihui.web.controller;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Keyword;
import cn.com.dashihui.web.service.KeywordService;

public class KeywordController extends BaseController{
	private static final Logger logger = Logger.getLogger(KeywordController.class);
	private KeywordService service = new KeywordService();
    
    public void index(){
		render("index.jsp");
    }
	
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(getStoreid(), pageNum, pageSize));
	}
    
    public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		service.sortKeyword(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }
    
	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：关键字为空
	 */
	public void doAdd(){
		//关键字
		String keyword = getPara("keyword");
		if(StrKit.isBlank(keyword)){
			renderResult(1);
			return;
		}
		//保存
		Keyword obj = new Keyword().set("storeid", getCurrentUser().get("id")).set("keyword", keyword);
		if(service.addKeyword(obj)){
			renderSuccess(obj);
			return;
		}
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：关键字为空
	 */
	public void doEdit(){
		//ID
		String keywordid = getPara("keywordid");
		//关键字
		String keyword = getPara("keyword");
		if(StrKit.isBlank(keywordid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(keyword)){
			renderResult(1);
			return;
		}else{
			//更新
			if(service.editKeyword(new Keyword()
				.set("id", keywordid)
				.set("keyword", keyword))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delKeyword(id)){
			logger.info("删除关键字【"+id+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
