package cn.com.dashihui.web.controller;

import com.jfinal.kit.PropKit;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.LogKeywordService;

public class LogKeywordController extends BaseController{
	private LogKeywordService service = new LogKeywordService();
    
    public void index(){
		render("index.jsp");
    }
	
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(getStoreid(), pageNum, pageSize));
	}
}
