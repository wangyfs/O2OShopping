package cn.com.dashihui.web.interceptor;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.web.base.BaseController;

/**
 * 验证当前是否有用户登录，如果有，则放过，如果没有，则判断当前是否是Ajax请求来作出响应
 */
public class AuthLoginInterceptor implements Interceptor{
	@Override
	public void intercept(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		if(c.getCurrentUser()!=null){
			inv.invoke();
		}else if(CommonKit.isAjax((HttpServletRequest)c.getRequest())){
			c.renderResult(-2);
		}else{
			c.redirect("/login");;
		}
	}
}
