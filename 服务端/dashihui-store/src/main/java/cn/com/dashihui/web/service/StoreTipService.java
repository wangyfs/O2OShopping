package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.StoreTip;

public class StoreTipService {
	
	public boolean sortAd(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_dict_store_tip SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public boolean addTip(StoreTip newObject){
		return newObject.save();
	}
	
	public StoreTip findByCode(String code){
		return StoreTip.me().findFirst("SELECT * FROM t_dict_store_tip WHERE code=?",code);
	}
	
	public boolean delTip(int id){
		Db.update("DELETE FROM t_dict_store_tip WHERE id=?",id);
		return true;
	}
	
	public boolean editTip(StoreTip object){
		return object.update();
	}
	
	public StoreTip findById(int id){
		return StoreTip.me().findFirst("SELECT a.* FROM t_dict_store_tip a WHERE a.id=?",id);
	}
	
	public Page<Record> findByPage(int pageNum, int pageSize, int storeid){
		StringBuffer sqlExcept = new StringBuffer("FROM t_dict_store_tip A WHERE A.storeid=? ORDER BY A.orderNo");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(),storeid);
	}
}
