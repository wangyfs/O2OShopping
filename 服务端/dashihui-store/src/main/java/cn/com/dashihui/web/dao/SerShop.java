package cn.com.dashihui.web.dao;

import com.jfinal.plugin.activerecord.Model;

public class SerShop extends Model<SerShop>{
	private static final long serialVersionUID = 1L;
	private static SerShop me = new SerShop();
	public static SerShop me(){
		return me;
	}
	
	public String getTrueName(){
		return getStr("trueName");
	}
}
