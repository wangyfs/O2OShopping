package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.web.dao.GoodsTag;

public class GoodsTagService {
	
	public boolean sortAd(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_bus_goods_tag SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public boolean addTag(GoodsTag newObject){
		return newObject.save();
	}
	
	public GoodsTag findByCode(int storeid,String code){
		return GoodsTag.me().findFirst("SELECT * FROM t_bus_goods_tag WHERE storeid=? AND code=?",storeid,code);
	}
	
	@Before(Tx.class)
	public boolean delTag(int id){
		GoodsTag.me().deleteById(id);
		Db.update("DELETE FROM t_bus_goods_tag_rel WHERE tagid=?",id);
		return true;
	}
	
	public boolean editTag(GoodsTag object){
		return object.update();
	}
	
	public GoodsTag findById(int id){
		return GoodsTag.me().findFirst("SELECT a.* FROM t_bus_goods_tag a WHERE a.id=?",id);
	}
	
	public Page<Record> findByPage(int pageNum, int pageSize, int storeid){
		StringBuffer sqlExcept = new StringBuffer("FROM t_bus_goods_tag A WHERE A.storeid=? ORDER BY A.orderNo");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(),storeid);
	}
}
