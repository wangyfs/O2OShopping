package cn.com.dashihui.web.controller;

import com.jfinal.kit.PropKit;

import cn.com.dashihui.kit.LuceneKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.GoodsBaseService;

/**
 * 平台基础商品管理，基础商品可由各店铺进行复制
 */
public class GoodsBaseController extends BaseController{
	GoodsBaseService service = new GoodsBaseService();
	/**
	 * 转到基础商品列表页面
	 */
	public void index(){
		render("index.jsp");
	}
	/**
	 * 列表页面异步请求数据
	 */
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		int c1 = getParaToInt("c1",0);
		int c2 = getParaToInt("c2",0);
		int c3 = getParaToInt("c3",0);
		int c4 = getParaToInt("c4",0);
		String keyword = getPara("k");
		renderResult(0,service.findByPage(pageNum,pageSize,getStoreid(),c1,c2,c3,c4,keyword));
	}
	
	/**
	 * 商品详情
	 */
	public void toDetail(){
		int goodsid = getParaToInt(0,0);
		setAttr("object", service.findById(goodsid).put("imageList",service.findImages(goodsid)));
		render("detail.jsp");
	}
	
	/**
	 * 商品复制
	 */
	public void doCopy(){
		int goodsid = getParaToInt(0,0);
		if(service.copyTo(goodsid,getStoreid())){
			//生成索引
			LuceneKit.createIndex();
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
