<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-offset-6 col-lg-6">
				<div class="pull-right">
					<button type="button" class="btn btn-link" onclick="toAddItem()"><span class="fa fa-plus"></span> 添加</button>
				</div>
			</div>
		</div>
		<div class="table-responsive" style="min-height:300px;">
			<table id="itemDataTable" class="table table-striped table-hover">
	            <thead>
	                <tr>
						<th>标题</th>
						<th width="16%">市场价(元)</th>
						<th width="16%">销售价(元)</th>
						<th width="15%">操作</th>
	                </tr>
	            </thead>
	            <tbody id="itemDataList"></tbody>
	        </table>
		</div>
	</div>
</div>
<input type="hidden" name="hidshopid" value="${shopid}">
<script type="text/html" id="itemListDataTpl">
	{{each object as item}}
	<tr id="{{item.id}}" data-id="{{item.id}}">
		<td>{{item.title}}</td>
		<td>{{item.marketPrice}}</td>
		<td>{{item.sellPrice}}</td>
		<td>
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEditItem('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDeleteItem('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button> 
		</td>
	</tr>
	{{/each}}	
</script>
<script type="text/javascript">
var shopid = $("input[name='hidshopid']").val();
$(loadItem);
function loadItem(){
	$.post("${BASE_PATH}/service/shop/itemList/"+shopid,function(result){
		if(result.flag==0){
			$("#itemDataList").empty().append(template("itemListDataTpl",result));
		}
	},'json');
}
var addItemDialog;
function toAddItem(){
	addItemDialog = Kit.dialog("添加服务项","${BASE_PATH}/service/shop/toItemAdd/"+shopid).open();
}
var editItemDialog;
function toEditItem(id){
	editItemDialog = Kit.dialog("修改服务项","${BASE_PATH}/service/shop/toItemEdit/"+id).open();
}
function toDeleteItem(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/service/shop/doItemDelete/"+id,function(result){
			if(result.flag==0){
				loadItem();
			}else{
				kit.alert("失败了");
			}
			
		});
	});
}
</script>