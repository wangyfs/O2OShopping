<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- 自定义工具包，需要第一时间引入，因为其他文件中会有引用 -->
<script src='${BASE_PATH}/static/js/custom-kit.js' type='text/javascript'></script>
<!-- jQuery -->
<script src='${BASE_PATH}/static/plugins/jquery/jquery.min.js' type='text/javascript'></script>
<!-- bootstrap -->
<script src='${BASE_PATH}/static/plugins/bootstrap/js/bootstrap.min.js' type='text/javascript'></script>
<!-- bootstrap dialog -->
<script src='${BASE_PATH}/static/plugins/bootstrap-dialog/js/bootstrap-dialog.min.js' type='text/javascript'></script>
<!-- bootstrap pagination -->
<script src='${BASE_PATH}/static/plugins/bootstrap-pagination/bootstrap-pagination.min.js' type='text/javascript'></script>
<script src='${BASE_PATH}/static/plugins/bootstrap-pagination/bootstrap-pagination-ext.js' type='text/javascript'></script>
<!-- icheck -->
<script src='${BASE_PATH}/static/plugins/bootstrap-icheck/js/icheck.min.js' type='text/javascript'></script>
<!-- 折叠菜单 -->
<script src='${BASE_PATH}/static/plugins/metisMenu/js/metisMenu.min.js' type='text/javascript'></script>
<!-- jQuery验证 -->
<script src='${BASE_PATH}/static/plugins/jquery-validate/jquery.validate.js' type='text/javascript'></script>
<script src='${BASE_PATH}/static/plugins/jquery-validate/messages_zh.min.js' type='text/javascript'></script>
<script src='${BASE_PATH}/static/plugins/jquery-validate/jquery.validate.ext.js' type='text/javascript'></script>
<!-- jQuery form -->
<script src='${BASE_PATH}/static/plugins/jquery-form/jquery.form.js' type='text/javascript'></script>
<!-- layer弹出工具 -->
<script src='${BASE_PATH}/static/plugins/layer/layer.js' type='text/javascript'></script>
<script src='${BASE_PATH}/static/plugins/layer/extend/layer.ext.js' type='text/javascript'></script>
<!-- bootstrap datetimepicker -->
<script type="text/javascript" src='${BASE_PATH}/static/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'></script>
<script type="text/javascript" src='${BASE_PATH}/static/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.zh-CN.js'></script>
<!-- bootstrap select -->
<script type="text/javascript" src='${BASE_PATH}/static/plugins/bootstrap-select/js/bootstrap-select.min.js'></script>
<script type="text/javascript" src='${BASE_PATH}/static/plugins/bootstrap-select/js/bootstrap-select.zh-CN.js'></script>
<!-- bootstrap fileinput -->
<script type="text/javascript" src="${BASE_PATH}/static/plugins/bootstrap-fileinput/js/fileinput.js"></script>
<script type="text/javascript" src="${BASE_PATH}/static/plugins/bootstrap-fileinput/js/fileinput_locale_zh.js"></script>
<!-- template 模板工具，以及一些常用的自定义函数 -->
<script src='${BASE_PATH}/static/plugins/template/template.js' type='text/javascript'></script>
<script src='${BASE_PATH}/static/plugins/template/template-ext.js' type='text/javascript'></script>
<!-- jquery slimscroll -->
<script src='${BASE_PATH}/static/plugins/jquery-slimscroll/js/jquery.slimscroll.min.js' type='text/javascript'></script>
<!-- 自定义一些初始化代码，需要最后引入，因为要依赖其他插件 -->
<script src='${BASE_PATH}/static/js/custom.js' type='text/javascript'></script>