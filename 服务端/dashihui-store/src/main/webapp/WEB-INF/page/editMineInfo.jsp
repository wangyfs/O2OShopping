<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#editMineInfoForm").validate({
		rules:{
			tel: {isTelOrMobile: true},
			mail: {email: true}
		},
		messages:{
			title: {required: "请输入名称"},
			tel: {isTelOrMobile: "请输入座机或手机号码"},
			mail: {email: "请输入正确的邮箱地址"},
			address:{required: "请输入店铺具体地址"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
						case 5:
							Kit.alert("请输入座机或手机号码");return;
						case 6:
							Kit.alert("请输入正确的邮箱地址");return;
						case -1:
							Kit.alert("系统异常，请重试");return;
						case 0:
							onMineInfoEdited(data.object);
							editMineInfoDialog.close();
					}
				}
			});
		}
	});
});
</script>
<form id="editMineInfoForm" action="${BASE_PATH}/doEditMineInfo" method="post" class="form-horizontal" enctype="multipart/form-data">
		<div class="form-group">
	    <label class="col-lg-2 control-label">店铺名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="title" value="${object.title}" class="form-control" placeholder="请输入店铺名称" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
		<label class="col-lg-2 control-label">门头照片</label>
		<div class="col-lg-9">
			<input type="file" id="thumb" name="thumb" accept="image/jpg" title="选择文件" <c:if test="${object.thumb!=null}">data-val="${object.thumb}"</c:if>>
		</div>
		<div class="col-lg-offset-2 p-t-5 col-lg-9"><span class="text-success">建议640px*300px,大小60K以内，支持JPEG、PNG格式</span></div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">地址</label>
	     <div class="col-lg-9">
    		<select id="province" name="province" class="selectpicker" data-width="33%" data-url="${BASE_PATH}/api/province" data-val="${object.province}" data-isfirst="true" data-next="#city" data-key="id:name" required></select>
			<select id="city" name="city" class="selectpicker" data-width="33%" data-url="${BASE_PATH}/api/city/{value}" data-val="${object.city}" data-next="#area" data-key="id:name" required></select>
			<select id="area" name="area" class="selectpicker" data-width="32%" data-url="${BASE_PATH}/api/area/{value}" data-val="${object.area}" data-key="id:name" required></select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label"></label>
	    <div class="col-lg-9">
       	    <input type="text" name="address" value="${object.address}" class="form-control" placeholder="请输入详细地址" required maxlength="100">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">店长</label>
	    <div class="col-lg-9">
        	<input type="text" name="manager" value="${object.manager}" class="form-control" placeholder="请输入店长名称"  maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">电话</label>
	    <div class="col-lg-9">
        	<input type="text" name="tel" value="${object.tel}" class="form-control" placeholder="请输入电话" maxlength="13">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">营业从</label>
       	<div class="col-lg-9">
        	<input class="form-control datetimepicker" name="beginTime" size="16" type="text" data-format="hh:ii" readonly="readonly" value="${object.beginTime}" placeholder="请点击选择营业开始时间">
        </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">营业至</label>
       	<div class="col-lg-9">
	        <input class="form-control datetimepicker" name="endTime" size="16" type="text" data-format="hh:ii" readonly="readonly" value="${object.endTime}" placeholder="请点击选择营业结束时间">
    	</div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">邮箱</label>
	    <div class="col-lg-9">
        	<input type="text" name="mail" value="${object.mail}" class="form-control" placeholder="请输入邮箱" maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">配送说明</label>
	    <div class="col-lg-9">
	    	<textarea name="deliverydes" class="form-control" placeholder="请输入配送说明" maxlength="200">${object.deliverydes}</textarea>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editMineInfoDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="thumbOld" value="${object.thumb}"/>
</form>