<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		rules:{
			passwordConfirm: {
				equalTo: "#passwordOld"
			},
			tel: {isTelOrMobile: true}
		},
		messages:{
			name: {required: "请输入商家名称"},
			address:{required: "请输入商家具体地址"},
			tel: {isTelOrMobile: "请输入座机或手机号码"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
						case 4:
							Kit.alert("请输入座机或手机号码");return;
						case -1:
							Kit.alert("系统异常，请重试");return;
						case 0:
							dataPaginator.loadPage(1);
							editDialog.close();
					}
				}
			});
		}
	});
});
</script>
<form id="editForm" action="${BASE_PATH}/ser/shop/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">商家名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" value="${object.name}" class="form-control" placeholder="请输入商家名称" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">地址</label>
	    <div class="col-lg-9">
    		<input type="text" name="address" value="${object.address}" class="form-control" placeholder="请输入商家地址" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">电话</label>
	    <div class="col-lg-9">
        	<input type="text" name="tel" value="${object.tel}" class="form-control" placeholder="请输入电话（座机或手机号）" maxlength="13">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">营业中</label>
	    <div class="col-lg-9">
	        <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="isWork" value="1" <c:if test="${object.isWork==1}">checked</c:if>> 是</li>
	    		<li><input type="radio" class="iCheck" name="isWork" value="0" <c:if test="${object.isWork==0}">checked</c:if>> 否</li>
	    	</ul>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">状态</label>
	    <div class="col-lg-9">
	    	<select name="enabled" class="selectpicker form-control">
				<option value="1" <c:if test="${object.enabled==1}">checked</c:if>>可用</option>
				<option value="0" <c:if test="${object.enabled==0}">checked</c:if>>禁用</option>
	        </select>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="shopid" value="${object.id}"/>
</form>