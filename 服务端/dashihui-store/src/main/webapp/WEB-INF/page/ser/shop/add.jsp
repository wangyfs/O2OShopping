<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		rules:{
			username: {isMobile: true},
			passwordConfirm: {
				equalTo: "#passwordOld"
			},
			tel: {isTelOrMobile: true}
		},
		messages:{
			username: {required: "请输入用户名"},
			username: {isTelOrMobile: "请输入正确的手机号码作为用户名"},
			password: {required: "请输入密码"},
			passwordConfirm: {required: "请再次输入密码",equalTo: "两次密码输入不相同"},
			name: {required: "请输入商家名称"},
			address:{required: "请输入商家具体地址"},
			tel: {isTelOrMobile: "请输入座机或手机号码"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
						case 3:
							Kit.alert("用户名已经存在");return;
						case 9:
							Kit.alert("请输入座机或手机号码");return;
						case -1:
							Kit.alert("系统异常，请重试");return;
						case 0:
							dataPaginator.loadPage(1);
							addDialog.close();
					}
				}
			});
		}
	});
});
</script>
<form id="addForm" action="${BASE_PATH}/ser/shop/doAdd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">用户名</label>
	    <div class="col-lg-9">
        	<input type="text" name="username" value="" class="form-control" placeholder="请输入用户名（手机号）" required maxlength="11">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordOld" name="password" value="" class="form-control" placeholder="请输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">确认密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordConfirm" name="passwordConfirm" value="" class="form-control" placeholder="请再次输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">商家名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" value="" class="form-control" placeholder="请输入商家名称" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">地址</label>
	    <div class="col-lg-9">
    		<input type="text" name="address" value="" class="form-control" placeholder="请输入商家地址" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">电话</label>
	    <div class="col-lg-9">
        	<input type="text" name="tel" value="" class="form-control" placeholder="请输入电话（座机或手机号）" maxlength="13">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">营业中</label>
	    <div class="col-lg-9">
	        <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="isWork" value="1" checked> 是</li>
	    		<li><input type="radio" class="iCheck" name="isWork" value="0"> 否</li>
	    	</ul>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">状态</label>
	    <div class="col-lg-9">
	    	<select name="enabled" class="selectpicker form-control">
				<option value="1">可用</option>
				<option value="0">禁用</option>
	        </select>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>