//
//  AppDelegate.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RDVTabBarController;
#import "WXApi.h"
//升级提示框tag
#define COMPEL_UPDATA_TAG 55
#define UPDATA_TAG 56
@interface AppDelegate : UIResponder <UIApplicationDelegate,WXApiDelegate>
{
    enum WXScene _scene;
}

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) RDVTabBarController *tabBarController;
@property(nonatomic, assign) NSInteger currentSelectedTabBarIndex;
@property(nonatomic, assign) NSInteger oldSelectedTabBarIndex;
- (void)selectedControllerAtTabBarIndex:(NSInteger)index;
@end

