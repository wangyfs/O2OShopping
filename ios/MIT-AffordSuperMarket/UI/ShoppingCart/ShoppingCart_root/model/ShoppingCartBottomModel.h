//
//  ShoppingCartBottomModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/20.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ShoppingCartBottomModel
 Created_Date： 20160420
 Created_People： GT
 Function_description： 购物车底部视图模型
 ***************************************/

#import <Foundation/Foundation.h>

@interface ShoppingCartBottomModel : NSObject
@property(nonatomic)BOOL isSelected;//是否全选
@property(nonatomic,strong)NSString *totalPrice;//总价格
@property(nonatomic,strong)NSString *goodsCount;//去结算的商品个数
@end
