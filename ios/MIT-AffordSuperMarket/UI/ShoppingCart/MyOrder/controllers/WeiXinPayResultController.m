//
//  WeiXinPayResultController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/8.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "WeiXinPayResultController.h"
//untils
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
#import "AppDelegate.h"
//vendor
#import "RDVTabBarController.h"
//controller
#import "MyOrderDetail_rootController.h"
#import "MyGoodsOrderDetailController.h"
#import "MyGoodsOrderController.h"
#import "MTA.h"
@interface WeiXinPayResultController ()
@property(nonatomic, strong)NSString *imageName;
@property(nonatomic, strong)NSString *stateText;
@end

@implementation WeiXinPayResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [self addBackNavItem];
    [self setupSubView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:NO];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"支付结果页面"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"支付结果页面"];
}
#pragma mark -- butotn Action
- (void)beatEvent {
    
//    myOrderVC.homeMakeType =self.
    //requestDataType  商品订单 是1 服务订单是 0
    if ([self.requestDataType isEqualToString:@"1"]) {//商品订单
        MyGoodsOrderController *myGoodsOrderVC = [[MyGoodsOrderController alloc]init];
        myGoodsOrderVC.myGoodOrderType = kGoodWholeType;
        myGoodsOrderVC.selectedIndex =0;
        [self.navigationController pushViewController:myGoodsOrderVC animated:YES];
    }else  if([self.requestDataType isEqualToString:@"0"]){//服务订单是
        MyOrderDetail_rootController *myOrderVC = [[MyOrderDetail_rootController alloc]init];
        myOrderVC.orderNum = self.orderNum;
        NSInteger currentIndex = 0;
        NSInteger oldIndex = 0;
        NSInteger selectedItemIndex = 0;
        if ([self.state isEqualToString:@"0"]) {//成功
            currentIndex = 1;
            oldIndex = 0;
            selectedItemIndex = 1;
        } else if ([self.state isEqualToString:@"-1"]) {//失败
            currentIndex = 0;
            oldIndex = 1;
            selectedItemIndex = 0;
        }
        myOrderVC.currentIndexOfController = currentIndex;
        myOrderVC.oldIndexOfController = oldIndex;
        myOrderVC.selectedItemIndex = selectedItemIndex;
        myOrderVC.homeMakeType =self.homeMakeType;
        myOrderVC.requestDataType =self.requestDataType;
        [self.navigationController pushViewController:myOrderVC animated:YES];
    }
}
#pragma mark -- setter method
- (void)setState:(NSString *)state {
   
    if ([state isEqualToString:@"0"]) {//成功
        self.title = @"支付成功";
        _imageName = @"pay_failed";
        self.stateText = @"恭喜您，订单支付成功";
    } else if ([state isEqualToString:@"-1"]) {//失败
        self.title = @"支付失败";
        _imageName = @"pay_success";
        self.stateText = @"订单支付失败";
    } else if([state isEqualToString:@"-2"]){
        self.title = @"支付失败";
        _imageName = @"pay_success";
        self.stateText = @"用户已取消该次支付";
    }
     _state = state;
}
- (void)backBtnAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    UIImage *image = [UIImage imageNamed:self.imageName];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame) - image.size.width/2
                                                                          , 102
                                                                          , image.size.width
                                                                          , image.size.height)];
    imageView.image = image;
    [self.view addSubview:imageView];
    CGSize stateTextSize = [self.stateText sizeWithFont:[UIFont systemFontOfSize:16] maxSize:CGSizeMake(self.view.frame.size.width, 24)];
    UILabel *stateTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame) - stateTextSize.width/2
                                                                       , CGRectGetMaxY(imageView.frame) + 30
                                                                       , stateTextSize.width
                                                                       , 24)];
    stateTextLabel.backgroundColor = [UIColor clearColor];
    stateTextLabel.font = [UIFont systemFontOfSize:16];
    stateTextLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    stateTextLabel.text = self.stateText;
    [self.view addSubview:stateTextLabel];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(CGRectGetMidX(self.view.frame) - 65
                              , CGRectGetMaxY(stateTextLabel.frame) + 38
                              , 130
                              , 44);
    //requestDataType  商品订单 是1 服务订单是 0
    NSString *buttonTitle = @"查看详情";
    if ([self.requestDataType isEqualToString:@"1"]) {// 商品订单
        buttonTitle = @"查看订单";
    }
    [button setTitle:buttonTitle forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:16];
    [button.layer setMasksToBounds:YES];
    [button.layer setBorderWidth:1.5];
    [button.layer setCornerRadius:4];
    button.backgroundColor = [UIColor clearColor];
    [button.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    [button addTarget:self action:@selector(beatEvent) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
