//
//  MyOrderDetail_rootController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyOrderDetail_rootController.h"
//vendor
#import "RDVTabBarController.h"
//untils
#import "UIColor+Hex.h"
#import "AppDelegate.h"
#import "Global.h"
//views
#import "MenuScrollView.h"
//controllers
#import "MyOrderDetailController.h"
#import "MyOrderStateController.h"
@interface MyOrderDetail_rootController ()<UIScrollViewDelegate>
@property (nonatomic,strong)NSArray  *controllerItemsArray;//所有的controller

@end

@implementation MyOrderDetail_rootController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"大实惠云超市";
    [self addBackNavItem];
    self.view.backgroundColor = [UIColor colorWithHexString:@"efefef"];
    _controllerItemsArray = [[NSMutableArray alloc]init];
    [self setupSubView];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    [self chengeSubControl];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}

#pragma mark -- 初始化视图
- (void)setupSubView {
     NSArray *titles = @[@"订单状态",@"订单详情"];
    //把视图添加到容器里面
    //订单详情
    MyOrderDetailController *myOrderDetailVC = [[MyOrderDetailController alloc]init];
//    myOrderDetailVC.orderNum=self.orderNum;
    //订单状态
    MyOrderStateController *myOrderStateVC = [[MyOrderStateController alloc]init];
//    myOrderStateVC.orderNum=self.orderNum;
    [self addChildViewController:myOrderStateVC];
    [self addChildViewController:myOrderDetailVC];
    self.controllerItemsArray = @[myOrderStateVC,myOrderDetailVC];
    
    
    //[self chengeSubControl];
    
    //初始化menuScrollView
    CGRect menuScrollViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, HightScalar(50));
    MenuScrollView *menu = [[MenuScrollView alloc]initWithFrame:menuScrollViewFrame titles:titles dataScrollView:nil];
    menu.backgroundColor = [UIColor grayColor];
    menu.selectIndex = self.selectedItemIndex;
    menu.showBottomLine = YES;
    menu.menuScrollDelegate = (id<MenuScrollViewDelegate>)self;
    menu.showVerticalLine = NO;
   // menu.verticalLineImage = [UIImage imageNamed:@"vertical_sepLine_bg"];
    menu.bottomLineImage = [UIImage imageNamed:@"bottomLine_bg"];
    menu.tabBackgroundColor = [UIColor colorWithHexString:@"#dddddd"];
    menu.tabSelectedBackgroundColor = [UIColor colorWithHexString:@"#fffff"];
    menu.tabSelectedTitleColor  = [UIColor colorWithHexString:@"#c52720"];
    menu.tabTitleColor = [UIColor colorWithHexString:@"#555555"];
    
    [self.view addSubview:menu];
}

- (void)chengeSubControl
{
    //移除一个child view controller
    [(UIViewController*)[_controllerItemsArray objectAtIndex:_oldIndexOfController] willMoveToParentViewController:nil];
    [((UIViewController*)[_controllerItemsArray objectAtIndex:_oldIndexOfController]).view removeFromSuperview];
    [(UIViewController*)[_controllerItemsArray objectAtIndex:_oldIndexOfController] removeFromParentViewController];
    //添加一个child view controller
    UIViewController *currentVC = [_controllerItemsArray objectAtIndex:_currentIndexOfController];
    if ([currentVC isKindOfClass:[MyOrderStateController class]]) {
        ((MyOrderStateController*)currentVC).orderNum = self.orderNum;
        ((MyOrderStateController*)currentVC).requestDataType = self.requestDataType;
        ((MyOrderStateController*)currentVC).homeMakeType = self.homeMakeType;
    } else if([currentVC isKindOfClass:[MyOrderDetailController class]]) {
        
        ((MyOrderDetailController*)currentVC).orderNum = self.orderNum;
        ((MyOrderDetailController*)currentVC).requestDataType = self.requestDataType;
        ((MyOrderStateController*)currentVC).homeMakeType = self.homeMakeType;
    }
    
    [self addChildViewController:currentVC];
    CGRect contentFrame = CGRectMake(CGRectGetMinX(self.view.frame)
                                     ,HightScalar(50)
                                     , CGRectGetWidth(self.view.frame)
                                     , CGRectGetHeight(self.view.bounds) - HightScalar(50));
    currentVC.view.frame = contentFrame;
    
    [self.view addSubview:currentVC.view];
    
    [currentVC didMoveToParentViewController:self];
}
#pragma mark -- MenuScrollViewDelegate
- (void)selectedMenuItemAtIndex:(NSInteger)index {
    
    if (index == _currentIndexOfController)
    {
        return;
    }
    self.oldIndexOfController = self.currentIndexOfController;
    self.currentIndexOfController = index;
    [self chengeSubControl];
}

#pragma mark buttonAction
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
