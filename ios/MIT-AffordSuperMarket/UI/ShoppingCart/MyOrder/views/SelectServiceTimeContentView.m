//
//  SelectServiceTimeContentView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SelectServiceTimeContentView.h"
#define ksepLineHight 1
#define kitemHight 54
#define kleftSpace 15
//model
#import "SelectServiceTimeContentViewModel.h"
#import "SelectServiceTimeContentItemModel.h"
//untils
#import "Global.h"
#import "UIImage+ColorToImage.h"
#import "UIColor+Hex.h"
@interface SelectServiceTimeContentView ()
{
    NSInteger _columnNum;//列数
    CGFloat _itemWidth;//item宽度
    CGFloat _itemHeight;//item高度
    NSInteger _currentSelectedIndex;
    NSInteger _oldSelectedIndex;
}
@property(nonatomic, strong)NSMutableArray *buttons;
@property(nonatomic, strong)NSMutableArray *dataSource;
@property(nonatomic, strong)SelectServiceTimeContentViewModel *viewModel;
@end

@implementation SelectServiceTimeContentView

- (instancetype)initWithFrame:(CGRect)frame withDataArr:(NSArray*)dataArr columnNums:(NSInteger)columns{
    self = [super initWithFrame:frame];
    if (self) {
        _dataSource = [[NSMutableArray alloc]init];
        _currentSelectedIndex = 0;
        _oldSelectedIndex = 0;
        _columnNum = columns;
        _buttons = [[NSMutableArray alloc]init];
        _itemWidth = (VIEW_WIDTH - (_columnNum + 1)*ksepLineHight - 2*kleftSpace)/_columnNum;//计算九宫格item的宽度
        _itemHeight = kitemHight;
        _viewModel = [[SelectServiceTimeContentViewModel alloc]init];
        [_viewModel initDataWithArray:dataArr];
        [_dataSource addObjectsFromArray:[_viewModel getServiceTimeContentWithSelectedDateIndex:0]];
        [self setupSubViewWithDataArr:[_dataSource copy]];
    }
    return self;
}


- (void)setupSubViewWithDataArr:(NSArray*)dataArr {
    if (dataArr.count != 0) {
        UIImage *h_sepLineImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#d7d7d7"]
                                                          frame:CGRectMake(0, 0, _itemHeight, ksepLineHight)];
        UIImage *v_sepLineImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#d7d7d7"]
                                                          frame:CGRectMake(0, 0, ksepLineHight, _itemHeight)];
        UIImage *disableImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#f2f2f2"]
                                                        frame:CGRectMake(0, 0, _itemWidth, _itemHeight)];
        UIImage *selectedImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#d84f4f"] frame:CGRectMake(0, 0, _itemWidth, _itemHeight)];

        NSInteger lineNum = [self calculationLineNumWithDataArr:dataArr];//行数
        NSInteger index = 0;
        //行
        for (NSInteger i = 0; i < lineNum; i++) {
            //列
            for (NSInteger j = 0; j < _columnNum; j++) {
                //左分割线
                UIImageView *leftImageView = [[UIImageView alloc]initWithImage:v_sepLineImage];
                leftImageView.frame = CGRectMake(kleftSpace +(_itemWidth + ksepLineHight)*j
                                                 , ksepLineHight +(_itemHeight + ksepLineHight)*i
                                                 , ksepLineHight
                                                 , _itemHeight);
                [self addSubview:leftImageView];
                //上分割线
                UIImageView *topImageView = [[UIImageView alloc]initWithImage:h_sepLineImage];
                topImageView.frame = CGRectMake( kleftSpace + (_itemWidth + ksepLineHight)*j
                                                ,(_itemHeight + ksepLineHight)*i
                                                , _itemWidth + ksepLineHight
                                                , ksepLineHight);
                [self addSubview:topImageView];
                
                SelectServiceTimeContentItemModel *model = nil;
                //防止数组越界
                if (dataArr.count > index) {
                    model = (SelectServiceTimeContentItemModel*)[dataArr objectAtIndex:index];
                } else {
                    model = [[SelectServiceTimeContentItemModel alloc]init];
                }
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                NSString *title = model.time;
                [button setTitle:title forState:UIControlStateNormal];
                if (!model.enable) {
                    button.enabled = model.enable;
                    [button setBackgroundImage:disableImage forState:UIControlStateDisabled];
                }
                [button setBackgroundImage:selectedImage forState:UIControlStateSelected];
                button.titleLabel.font = [UIFont systemFontOfSize:15];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
                [button addTarget:self action:@selector(selectedItemEvent:) forControlEvents:UIControlEventTouchUpInside];
                button.tag = index;
                CGRect buttonFrame = CGRectMake(CGRectGetMaxX(leftImageView.frame)
                                                , CGRectGetMaxY(topImageView.frame)
                                                ,_itemWidth
                                                ,_itemHeight);
                button.frame = buttonFrame;
                index ++;
                
                if (j == _columnNum -1 ) {//每行的最后一个item添加竖直分割线
                    UIImageView *rightSepLineView = [[UIImageView alloc]initWithImage:v_sepLineImage];
                    rightSepLineView.frame = CGRectMake(CGRectGetMaxX(button.frame)
                                                                  , CGRectGetMinY(button.frame)
                                                                  , ksepLineHight
                                                                  , CGRectGetHeight(button.bounds));
                    [self addSubview:rightSepLineView];

                }
                [_buttons addObject:button];
                [self addSubview:button];
            }
            
        }
        
        //对最后一行进行处理
        UIImageView *h_latest_sepLine_imageView = [[UIImageView alloc]initWithImage:h_sepLineImage];
        h_latest_sepLine_imageView.frame = CGRectMake(kleftSpace
                                                      , (_itemHeight + ksepLineHight)*lineNum
                                                      , VIEW_WIDTH - 2*kleftSpace
                                                      , ksepLineHight);
        [self addSubview:h_latest_sepLine_imageView];
        
        
    }
}

- (NSInteger)calculationLineNumWithDataArr:(NSArray*)dataArr {
    NSInteger lineNum = dataArr.count/_columnNum;
    if (dataArr.count%_columnNum != 0) {
        lineNum = dataArr.count/_columnNum + 1;
    }
    return lineNum;
}
#pragma mark -- button Action
- (void)selectedItemEvent:(id)sender {
    UIButton *button = (UIButton*)sender;
    if (button.tag == _currentSelectedIndex) {
        return;
    }
    _oldSelectedIndex = _currentSelectedIndex;
    _currentSelectedIndex = button.tag;
    [self changeSelectedButtonStyle];
    SelectServiceTimeContentItemModel *model = [self.dataSource objectAtIndex:_currentSelectedIndex];
    if (_delegate && [_delegate respondsToSelector:@selector(selectedListTtemAtIndex: title:)]) {
        [_delegate selectedListTtemAtIndex:button.tag title:model.date];
    }
}
#pragma mark -- 改变button样式
- (void)changeSelectedButtonStyle {
    UIButton *currentBtn = [self.buttons objectAtIndex:_currentSelectedIndex];
    UIButton *oldBtn = [self.buttons objectAtIndex:_oldSelectedIndex];
    currentBtn.selected = YES;
    oldBtn.selected = NO;
    
}
#pragma mark -- 刷新数据
- (void)reloadViewWithDateSelectedIndex:(NSInteger)selectedIndex {
    UIImage *disableImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#f2f2f2"]
                                                    frame:CGRectMake(0, 0, _itemWidth, _itemHeight)];
    NSArray *dataArr = [_viewModel getServiceTimeContentWithSelectedDateIndex:selectedIndex];
    [_dataSource removeAllObjects];
    [_dataSource addObjectsFromArray:dataArr];
    for (NSInteger i = 0; i < _buttons.count; i++) {
        UIButton *button = (UIButton*)[_buttons objectAtIndex:i];
        SelectServiceTimeContentItemModel *model = nil;
        //防止数组越界
        if (dataArr.count > i) {
            model = (SelectServiceTimeContentItemModel*)[dataArr objectAtIndex:i];
        } else {
            model = [[SelectServiceTimeContentItemModel alloc]init];
        }
        if (!model.enable) {
            button.enabled = model.enable;
            [button setBackgroundImage:disableImage forState:UIControlStateDisabled];
        } else {
            button.enabled = YES;
        }
        button.selected = NO;
        [button setTitle:model.time forState:UIControlStateNormal];
        
    }
}

@end
