//
//  MyOrderCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
#define KLeftSpacing 15
#define KRightSpacing 15
#define KCellHigtle HightScalar(200)
#define KTopSpacing 10

#import "Global.h"
#import "UIImage+ColorToImage.h"
#import "UIColor+Hex.h"
#import "MyOrderCell.h"
#import "NSString+Conversion.h"
//model
#import "MyOrderModel.h"
//vendor
#import "UIImageView+AFNetworking.h"
#import "ManagerGlobeUntil.h"
#import "XiaoQuAndStoreInfoModel.h"

@interface MyOrderCell()
//订单状态
@property (nonatomic,strong)UILabel *orderState;
//商品logo
@property (nonatomic,strong)UIImageView *goodsImage;
//多个商品时
@property (nonatomic,strong)UIImageView *goodsImageone;
@property (nonatomic,strong)UIImageView *goodsImagetwo;
@property (nonatomic,strong)UIImageView *goodsImagethree;
//商品名称
@property (nonatomic,strong)UILabel *goodsName;
//实际付款价格
@property (nonatomic,strong)UILabel *goodsMoney;

//一件商品的订单
@property (nonatomic,strong)UIView *oneView;

//更多商品的订单
@property (nonatomic,strong)UIView *moreView;

//服务订单 参数
@property(nonatomic,strong)UILabel *shopName;
@property(nonatomic,strong)UIView *serviceView;
@property (nonatomic,strong)UIImageView *serviceShopNameImage;
@property(nonatomic,strong)UILabel *serviceShopName;
@property(nonatomic,strong)UILabel *serviceTitle;

@property(nonatomic,strong)UILabel *serviceTime;

@property(nonatomic,strong)UILabel *serciceAddress;
@property(nonatomic,strong)UIImageView *logoImageView;
@property (nonatomic,strong)NSString *type; //如果是 家政订单 type才有值
@end

@implementation MyOrderCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier OrderType:(OrderType)OrderType
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberViewWithOrderType:OrderType];
    }
    return self;
}

#pragma mark --ButtonClick
-(void)goPayClick:(UIButton *)sender
{
    UIButton *btn =(UIButton*)sender;
    if (_delegate && [_delegate respondsToSelector:@selector(selectedFirstItemWithTag:title:homeMakeType:)]) {
        [_delegate selectedFirstItemWithTag:btn.tag  title:btn.titleLabel.text homeMakeType:_type];
    }
}
-(void)deleteOrderClick:(UIButton *)sender
{
    UIButton *btn =(UIButton*)sender;
    if (_delegate && [_delegate respondsToSelector:@selector(selectedSecondItemWithTag:title:homeMakeType:)]) {
        [_delegate selectedSecondItemWithTag:btn.tag title:btn.titleLabel.text  homeMakeType:_type];
    }
}

#pragma mark -- 更新数据源
- (void)updateViewwithData:(NSDictionary*)dataDic OrderType:(OrderType)OrderType{
    if (OrderType==kserviceOrderType) {
//        _shopName.hidden=NO;
        _serviceView.hidden=NO;
        _oneView.hidden=YES;
        _moreView.hidden=YES;
//        _logoImageView.hidden=YES;
        
    }else if(OrderType ==kgoodOrderType)
    {
//        _shopName.hidden=YES;
        _serviceView.hidden=YES;
        _oneView.hidden=NO;
        _moreView.hidden=NO;
//        _logoImageView.hidden=NO;
    }
    //付款价格
    NSString *totalPrice = [NSString stringWithFormat:@"实付款：￥%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"AMOUNT"]] floatValue]];
    NSMutableAttributedString *totalPriceAttributedStr = [[NSMutableAttributedString alloc]initWithString:totalPrice];
    //设置：在0-3个单位长度内的内容显示成红色
    [totalPriceAttributedStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4,totalPriceAttributedStr.length-4)];
    self.goodsMoney.attributedText = totalPriceAttributedStr;
    //订单状态
    
       self.shopName.text=[NSString stringTransformObject:[dataDic objectForKey:@"STORENAME"]];
    if (OrderType==kgoodOrderType) {
        if ([[dataDic objectForKey:@"GOODSLIST"] count] > 1) {//一个表单包含两个商品
            [self.contentView insertSubview:self.moreView aboveSubview:self.oneView];
            NSMutableArray *imageUrs = [[NSMutableArray alloc]init];
            for (NSDictionary *dic in [dataDic objectForKey:@"GOODSLIST"]) {//把图片保存起来
                NSString *imageurl = [NSString stringTransformObject:[dic objectForKey:@"THUMB"]];
                [imageUrs addObject:[NSString appendImageUrlWithServerUrl:imageurl]];
            }
            for (NSInteger i = 0; i < [imageUrs count]; i++) {
                if (i == 0) {
                    [self.goodsImageone setImageWithURL:[NSURL URLWithString:[imageUrs objectAtIndex:i]]
                                       placeholderImage:[UIImage imageNamed:@"place_image"]];
                } else if (i == 1) {
                    [self.goodsImagetwo setImageWithURL:[NSURL URLWithString:[imageUrs objectAtIndex:i]]
                                       placeholderImage:[UIImage imageNamed:@"place_image"]];
                } else if (i == 2) {
                    [self.goodsImagethree setImageWithURL:[NSURL URLWithString:[imageUrs objectAtIndex:i]]
                                         placeholderImage:[UIImage imageNamed:@"place_image"]];
                }
            }
            
        } else {//一个表单只包含1个商品
            [self.contentView insertSubview:self.oneView aboveSubview:self.moreView];
            NSDictionary *dic = [[dataDic objectForKey:@"GOODSLIST"] firstObject];
            NSString *imageurl =  [NSString stringTransformObject:[dic objectForKey:@"THUMB"]];
            //        [self.goodsImage setImageWithURL:[NSURL URLWithString:imageurl]
            //                       placeholderImage:[UIImage imageNamed:@"place_image"]];
            self.goodsImage.image=[UIImage createImageWithImageUrlString:imageurl];
            self.goodsName.text = [NSString stringISNull:[dic objectForKey:@"NAME"]];
        }
        
       
        self.orderState.text = [self judgeOrderStatusWithOrderState:[NSString stringTransformObject:[dataDic objectForKey:@"ORDERSTATE"] ] PayState:[NSString stringTransformObject:[dataDic objectForKey:@"PAYSTATE"] ] DeliveState:[NSString stringTransformObject:[dataDic objectForKey:@"DELIVERSTATE"]] PayType:[NSString stringTransformObject:[dataDic objectForKey:@"PAYTYPE"]] DeliveType:[NSString stringTransformObject:[dataDic objectForKey:@"TAKETYPE"]] OrderType:OrderType servicetype:nil];
    }else if(OrderType==kserviceOrderType){
        NSMutableArray *array=[[NSMutableArray alloc]init];
        [array addObjectsFromArray:[dataDic objectForKey:@"SERVICELIST"]];
        NSDictionary *dic=[array firstObject];

        if ([[NSString stringTransformObject:[dataDic objectForKey:@"TYPE"]] isEqualToString:@"1"]) {
            _type =@"1";
        }else{
            _type =@"";
        }
        
        self.serviceTitle.text=[NSString stringTransformObject:[dataDic objectForKey:@"SERVICETITLE"]];
        if (dic.count!=0) {
                self.shopName.text=[NSString stringTransformObject:[dic objectForKey:@"SHOPNAME"]];
                self.serviceTitle.text=[NSString stringTransformObject:[dic objectForKey:@"SERVICETITLE"]];
        }
        
        self.serviceTime.text=[NSString stringTransformObject:[dataDic objectForKey:@"SERTIME"]];
        self.serciceAddress.text=[NSString stringTransformObject:[dataDic objectForKey:@"ADDRESS"]];
        self.orderState.text = [self judgeOrderStatusWithOrderState:[NSString stringTransformObject:[dataDic objectForKey:@"ORDERSTATE"] ] PayState:[NSString stringTransformObject:[dataDic objectForKey:@"PAYSTATE"] ] DeliveState:[NSString stringTransformObject:[dataDic objectForKey:@"DELIVERSTATE"]] PayType:[NSString stringTransformObject:[dataDic objectForKey:@"PAYTYPE"]] DeliveType:@"1" OrderType:OrderType servicetype:[NSString stringTransformObject:[dataDic objectForKey:@"TYPE"]]];
        
        if ([[ NSString stringTransformObject:[dataDic objectForKey:@"PROSERNAME"]] length] ==0) {
            self.serviceShopNameImage.hidden =YES;
            self.serviceShopName.hidden =YES;
            self.serviceView.frame =CGRectMake(0, CGRectGetMaxY(_shopName.frame)-HightScalar(15), VIEW_WIDTH,  HightScalar(120)-1);
        }else{
            self.serviceShopName.text =[ NSString stringTransformObject:[dataDic objectForKey:@"PROSERNAME"]];
        }
     
    }
    
}
#pragma mark --根据状态设置订单页面按钮 和右上角 订单装态
- (NSString*)judgeOrderStatusWithOrderState:(NSString*)OrderState PayState:(NSString *)PayState DeliveState:(NSString *)DeliveState PayType:(NSString *)payType DeliveType:(NSString *)DeliveType OrderType:(OrderType)OrderType servicetype:(NSString *)serviceType
{
         CGRect firstfram=self.goPay.frame;//第一个按钮位置
         CGRect twofram=CGRectMake(CGRectGetMinX(self.goPay.frame)-85, CGRectGetMinY(self.goPay.frame), CGRectGetWidth(self.goPay.bounds), CGRectGetHeight(self.goPay.bounds));//第二个按钮位置
    
    if ([OrderState isEqualToString:KOrderStateNormal] )
    {//订单正常  支付状态为 未支付
        if ([payType isEqualToString:KOrderPayTypeIsPayPal] && [DeliveType isEqualToString:KOrderDeliverTypeIsDelivery])//在线付款+送货上门
        {
            if ([PayState isEqualToString:KOrderPayStateIsNo]) {
                [self.goPay setTitle:@"去支付" forState:UIControlStateNormal];
                self.deleteOrder.hidden=NO;
                [self.deleteOrder setTitle:@"取消订单" forState:UIControlStateNormal];
                self.deleteOrder.frame= twofram;
                 return @"待付款";
            }else if([PayState isEqualToString:KOrderPayStateIsYes])//订单正常  支付状态为 已支付
            {
                if ([DeliveState isEqualToString:KOrderDeliverStateIsNo]) {
                    self.deleteOrder.hidden=YES;
                    [self.goPay setTitle:@"催单" forState:UIControlStateNormal];
                    if (OrderType==kgoodOrderType) {
                        return @"待发货";
                    }else if(OrderType==kserviceOrderType){
                        return  @"待接单";
                    }
                }else if([DeliveState isEqualToString:KOrderDeliverStateIsYes]){//订单正常  收货状态为 已发货
                    if ([serviceType isEqualToString:@"1"]) {
                         self.deleteOrder.hidden=YES;
                        [self.goPay setTitle:@"催单" forState:UIControlStateNormal];
                        return @"配单中";
                    }else{
                    self.deleteOrder.hidden=YES;
                    if (OrderType==kgoodOrderType) {
                        [self.goPay setTitle:@"确认收货" forState:UIControlStateNormal];
                        return @"待收货";
                    }else if(OrderType==kserviceOrderType){
                        [self.goPay setTitle:@"确认服务" forState:UIControlStateNormal];
                         return @"已接单";
                    }
                  }
                }else if([DeliveState isEqualToString:@"3"]){//家政店铺已派单
                     self.deleteOrder.hidden=YES;
                    [self.goPay setTitle:@"催单" forState:UIControlStateNormal];
                    return @"配单中";
                }else if([DeliveState isEqualToString:@"4"])//家政店铺确认订单
                     self.deleteOrder.hidden=YES;
                    [self.goPay setTitle:@"确认服务" forState:UIControlStateNormal];
                    return @"已接单";
               }
            }else if([payType isEqualToString:KOrderPayTypeIsPayPal]&& [DeliveType isEqualToString:KOrderDeliverTypeIsSelfUp]){//自取 +在线付款
            
            if([PayState isEqualToString:KOrderPayStateIsNo]){
                 self.deleteOrder.hidden = NO;
                 self.deleteOrder.frame= twofram;
                [self.deleteOrder setTitle:@"取消订单" forState:UIControlStateNormal];
                [self.goPay setTitle:@"去支付" forState:UIControlStateNormal];
                return @"待付款";
            
            }else if([PayState isEqualToString:KOrderPayStateIsYes]) //如果是 家政订单 type才有值
            {
                self.deleteOrder.hidden = YES;
                [self.goPay setTitle:@"确认取货" forState:UIControlStateNormal];
                return @"待取货";
                    
            }
        }else if([payType isEqualToString:KOrderPayTypeIsCOD]&&[DeliveType isEqualToString:KOrderDeliverTypeIsDelivery]) //货到付款 + 送货上门
        {
            if([DeliveState isEqualToString:KOrderDeliverStateIsNo]){
                self.deleteOrder.hidden = NO;
                self.deleteOrder.frame= twofram;
                [self.deleteOrder setTitle:@"取消订单" forState:UIControlStateNormal];
                [self.goPay setTitle:@"催单" forState:UIControlStateNormal];
                if (OrderType==kgoodOrderType) {
                    return @"待发货";
                }else if(OrderType==kserviceOrderType){
                    return @"待接单";
                }
            }else if ([DeliveState isEqualToString:KOrderDeliverStateIsYes])//2
            {
                if ([serviceType isEqualToString:@"1"]) {
                    self.deleteOrder.hidden = NO;
                    self.deleteOrder.frame= twofram;
                    [self.deleteOrder setTitle:@"取消订单" forState:UIControlStateNormal];
                    [self.goPay setTitle:@"催单" forState:UIControlStateNormal];
                    return @"配单中";
                }else{
                    self.deleteOrder.hidden =YES;
                    if (OrderType==kgoodOrderType) {
                        [self.goPay setTitle:@"确认收货" forState:UIControlStateNormal];
                        return @"待收货";
                    }else if(OrderType==kserviceOrderType){
                        [self.goPay setTitle:@"确认服务" forState:UIControlStateNormal];
                        return @"已接单";
                    }
                }
                
            }else if ([DeliveState isEqualToString:@"3"])//家政店铺已派单
            {
                self.deleteOrder.hidden = NO;
                self.deleteOrder.frame= twofram;
                [self.deleteOrder setTitle:@"取消订单" forState:UIControlStateNormal];
                [self.goPay setTitle:@"催单" forState:UIControlStateNormal];
                return @"配单中";

            }else if([DeliveState isEqualToString:@"4"])//家政店铺已派单
            {
                 self.deleteOrder.hidden=YES;
                [self.goPay setTitle:@"确认服务" forState:UIControlStateNormal];
                return @"已接单";
            }

        }else if([payType isEqualToString:KOrderPayTypeIsCOD]&&[DeliveType isEqualToString:KOrderDeliverTypeIsSelfUp]) //货到付款 + 自取
        {
            
            [self.goPay setTitle:@"确认取货" forState:UIControlStateNormal];
            self.deleteOrder.hidden=NO;
            [self.deleteOrder setTitle:@"取消订单" forState:UIControlStateNormal];
            return @"待取货";
        }
    }  else if([OrderState isEqualToString:KOrderStateFinish])//订单状态为已完成
    {
        self.deleteOrder.hidden=NO;
        [self.deleteOrder setTitle:@"删除订单" forState:UIControlStateNormal];
         self.deleteOrder.frame=firstfram;
        return @"交易完成";
    }else if ([OrderState isEqualToString:KOrderStateCancel])//订单状态为已取消
    {
        self.deleteOrder.hidden=NO;
        [self.deleteOrder setTitle:@"删除订单" forState:UIControlStateNormal];
        self.deleteOrder.frame=firstfram;
        return @"已取消";
    } else if ([OrderState isEqualToString:KOrderStateBeOverdue])//订单状态为已过期
    {
        self.deleteOrder.hidden=NO;
        [self.deleteOrder setTitle:@"删除订单" forState:UIControlStateNormal];
        self.deleteOrder.frame=firstfram;
        return @"已过期";
    }else if ([serviceType isEqualToString:@"1"]&&[OrderState isEqualToString:@"4"]) //家政拒单
    {
        self.deleteOrder.hidden = YES;
        self.deleteOrder.frame= twofram;
        [self.goPay setTitle:@"催单" forState:UIControlStateNormal];
        return @"配单中";
        
    }else if ([serviceType isEqualToString:@"1"]&&[OrderState isEqualToString:@"6"]) //家政已过期
    {
        self.deleteOrder.hidden=NO;
        [self.deleteOrder setTitle:@"删除订单" forState:UIControlStateNormal];
        self.deleteOrder.frame=firstfram;
        return @"已过期";
    }
    return @"";
}
#pragma mark -初始化控件
-(void)setUpSuberViewWithOrderType:(OrderType)OrderType
{
    //cell 边框
        [self.layer setBorderWidth:0.5];
        [self.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
//    //标签图片
//    _logoImageView=[[UIImageView alloc]initWithFrame:CGRectMake(KLeftSpacing,KTopSpacing,65, 20)];
//    _logoImageView.image=[UIImage imageNamed:@"img_dashihui_logo"];
//    [self addSubview:_logoImageView];
//    _logoImageView.hidden=YES;
    _shopName =[[UILabel alloc]initWithFrame:CGRectMake(KLeftSpacing,0,VIEW_WIDTH-60-30, HightScalar(50))];
    _shopName.font=[UIFont systemFontOfSize:FontSize(16)];
    _shopName.textColor=[UIColor colorWithHexString:@"#555555"];
    [self.contentView addSubview:_shopName];
    //订单状态
    _orderState =[[UILabel alloc]initWithFrame:CGRectMake(VIEW_WIDTH-100-KRightSpacing
                                                          , CGRectGetMinY(_shopName.frame)
                                                          , 100
                                                          , CGRectGetHeight(_shopName.bounds))];
    _orderState.font=[UIFont systemFontOfSize:FontSize(16)];
    _orderState.textAlignment =NSTextAlignmentRight;
    _orderState.textColor=[UIColor colorWithHexString:@"#5a5a5a"];
    [self.contentView addSubview:_orderState];
    //一个物品时的view
    _oneView =[[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_shopName.frame)+1
                                                      , VIEW_WIDTH,HightScalar(120)-2)];
    _oneView.backgroundColor=[UIColor whiteColor];
    
    //商品图片
    _goodsImage=[[UIImageView alloc]initWithFrame:CGRectMake(KLeftSpacing, (CGRectGetHeight(self.oneView.bounds)-HightScalar(80))/2, HightScalar(80), HightScalar(80))];
    [_goodsImage.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    [_goodsImage.layer setBorderWidth:0.7];
    _goodsImage.contentMode= UIViewContentModeScaleAspectFit;
    [self.oneView addSubview:_goodsImage];
    
    //商品名称
    _goodsName=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImage.frame)+10
                                                        , (CGRectGetHeight(self.oneView.bounds)-HightScalar(50))/2
                                                        , VIEW_WIDTH-CGRectGetWidth(self.goodsImage.bounds)-KLeftSpacing-KRightSpacing-10
                                                        , HightScalar(50))];
    _goodsName.font =[UIFont systemFontOfSize:FontSize(17)];
    _goodsName.numberOfLines=0;
    _goodsName.textColor=[UIColor colorWithHexString:@"#5a5a5a"];
    [self.oneView addSubview:_goodsName];
    
    
    //多件商品
    _moreView =[[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_shopName.frame)+1, VIEW_WIDTH, CGRectGetHeight(self.oneView.bounds))];
    _moreView.backgroundColor=[UIColor whiteColor];
    
    //商品图片
    _goodsImageone=[[UIImageView alloc]initWithFrame:CGRectMake(KLeftSpacing, (CGRectGetHeight(self.moreView.bounds)-HightScalar(80))/2, CGRectGetWidth(self.goodsImage.bounds), CGRectGetHeight(self.goodsImage.bounds))];
    [self.moreView addSubview:_goodsImageone];
    _goodsImageone.contentMode= UIViewContentModeScaleAspectFit;
    
    _goodsImagetwo=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImageone.frame)+10, CGRectGetMinY(self.goodsImageone.frame),CGRectGetWidth(self.goodsImage.bounds), CGRectGetHeight(self.goodsImage.bounds))];
    [self.moreView addSubview:_goodsImagetwo];
    _goodsImagetwo.contentMode= UIViewContentModeScaleAspectFit;
    
    _goodsImagethree=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImagetwo.frame)+10, CGRectGetMinY(self.goodsImagetwo.frame), CGRectGetWidth(self.goodsImage.bounds), CGRectGetHeight(self.goodsImage.bounds))];
    [self.moreView addSubview:_goodsImagethree];
    _goodsImagethree.contentMode= UIViewContentModeScaleAspectFit;
    
    
    
    //服务订单中间部分cell
    

    _serviceView =[[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_shopName.frame)+1, VIEW_WIDTH, HightScalar(120)-1)];
    [self.contentView addSubview:_serviceView];
    _serviceView.hidden=YES;
    //灰色分割线
    UILabel *labelone=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMinY(self.oneView.frame)-1, VIEW_WIDTH, 1)];
    labelone.backgroundColor=[UIColor colorWithHexString:@"#e3e3e3"];
    [self.contentView addSubview:labelone];
       //灰色分割线
    UILabel *labeltwo=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(self.oneView.frame)+1, VIEW_WIDTH, 1)];
    labeltwo.backgroundColor=[UIColor colorWithHexString:@"#e3e3e3"];
    [self.contentView addSubview:labeltwo];
    
    //实际付款：
    _goodsMoney=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_shopName.frame), CGRectGetMaxY(self.serviceView.frame), VIEW_WIDTH/2, HightScalar(50))];
    _goodsMoney.textColor=[UIColor colorWithHexString:@"#5a5a5a"];
    _goodsMoney.font=[UIFont systemFontOfSize:FontSize(16)];
    [self.contentView addSubview:_goodsMoney];
    
    
    //去付款
    _goPay=[UIButton buttonWithType:UIButtonTypeCustom];
    _goPay.frame=CGRectMake(VIEW_WIDTH-85, CGRectGetMinY(self.goodsMoney.frame)+HightScalar(8), 70, HightScalar(34));
    UIImage *goPayBackgroundImage = [UIImage createImageWithColor:[UIColor whiteColor]
                                                            frame:CGRectMake(0, 0, CGRectGetWidth(_goPay.bounds), CGRectGetHeight(_goPay.bounds))];
    [_goPay setBackgroundImage:goPayBackgroundImage forState:UIControlStateNormal];
    [_goPay setTitle:@"去支付" forState:UIControlStateNormal];
    [_goPay.layer setBorderWidth:0.5];
    [_goPay.layer setBorderColor:[UIColor colorWithHexString:@"#c52720"].CGColor];
    [_goPay setTitleColor:[UIColor colorWithHexString:@"#c52720"] forState:UIControlStateNormal];
    _goPay.titleLabel.textColor=[UIColor whiteColor];
    _goPay.titleLabel.font=[UIFont systemFontOfSize:FontSize(14)];
    [_goPay addTarget:self action:@selector(goPayClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [_goPay.layer setMasksToBounds:YES];
    [_goPay.layer setCornerRadius:4.0];
    [self.contentView addSubview:_goPay];
    
    //删除订单
    _deleteOrder=[UIButton buttonWithType:UIButtonTypeCustom];
    _deleteOrder.frame=CGRectMake(CGRectGetMinX(self.goPay.frame)-85, CGRectGetMinY(self.goPay.frame), CGRectGetWidth(self.goPay.bounds), CGRectGetHeight(self.goPay.bounds));
    [_deleteOrder.layer setBorderWidth:0.5];
    [_deleteOrder.layer setBorderColor:[UIColor colorWithHexString:@"#555555"].CGColor];
    [_deleteOrder setTitle:@"删除订单" forState:UIControlStateNormal];
    [_deleteOrder setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
    UIImage *deleteOrderBackgroundImage = [UIImage createImageWithColor:[UIColor whiteColor]
                                                                  frame:CGRectMake(0, 0, CGRectGetWidth(_deleteOrder.bounds), CGRectGetHeight(_deleteOrder.bounds))];
    [_deleteOrder setBackgroundImage:deleteOrderBackgroundImage forState:UIControlStateNormal];
    
    [_deleteOrder addTarget:self action:@selector(deleteOrderClick:) forControlEvents:UIControlEventTouchUpInside];
    _deleteOrder.titleLabel.font=[UIFont systemFontOfSize:FontSize(14)];
    [_deleteOrder.layer setMasksToBounds:YES];
    [_deleteOrder.layer setCornerRadius:4.0];
    [self.contentView addSubview:_deleteOrder];


    //服务商家
    _serviceShopNameImage=[[UIImageView alloc]initWithFrame:CGRectMake(15, 3,HightScalar(20),HightScalar(20))];
    _serviceShopNameImage.image=[UIImage imageNamed:@"Nav_Person_BG"];
    [_serviceView addSubview:_serviceShopNameImage];
    _serviceShopName=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_serviceShopNameImage.frame)+10, CGRectGetMinY(_serviceShopNameImage.frame), VIEW_WIDTH-30-CGRectGetWidth(_serviceShopNameImage.bounds)-10, CGRectGetHeight(_serviceShopNameImage.bounds))];
    _serviceShopName.font=[UIFont systemFontOfSize:FontSize(16)];
    _serviceShopName.textColor=[UIColor colorWithHexString:@"#555555"];
    [_serviceView addSubview:_serviceShopName];
    //服务项目
    UIImageView *imageViewOne=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_serviceShopNameImage.frame), CGRectGetMaxY(_serviceShopNameImage.frame)+10,CGRectGetWidth(_serviceShopNameImage.bounds), CGRectGetHeight(_serviceShopNameImage.bounds))];
    imageViewOne.image=[UIImage imageNamed:@"serviceorde_icon_goodsname"];
    [_serviceView addSubview:imageViewOne];
    _serviceTitle=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageViewOne.frame)+10, CGRectGetMinY(imageViewOne.frame), VIEW_WIDTH-30-CGRectGetWidth(imageViewOne.bounds)-10, CGRectGetHeight(imageViewOne.bounds))];
    _serviceTitle.font=[UIFont systemFontOfSize:FontSize(16)];
    _serviceTitle.textColor=[UIColor colorWithHexString:@"#555555"];
    [_serviceView addSubview:_serviceTitle];
    
    //服务预定时间
    UIImageView *imageVieTwo=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(imageViewOne.frame), CGRectGetMaxY(imageViewOne.frame)+10, CGRectGetWidth(imageViewOne.bounds), CGRectGetHeight(imageViewOne.bounds))];
    imageVieTwo.image=[UIImage imageNamed:@"serviceorde_icon_timebuying"];
    [_serviceView addSubview:imageVieTwo];
    _serviceTime=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageVieTwo.frame)+10, CGRectGetMinY(imageVieTwo.frame), VIEW_WIDTH-30-CGRectGetWidth(imageVieTwo.bounds)-10, CGRectGetHeight(imageVieTwo.bounds))];
    _serviceTime.font=[UIFont systemFontOfSize:FontSize(16)];
    _serviceTime.textColor=[UIColor colorWithHexString:@"#555555"];
    [_serviceView addSubview:_serviceTime];
    
    //服务地址
    UIImageView *imageVieThree=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(imageVieTwo.frame), CGRectGetMaxY(imageVieTwo.frame)+10, CGRectGetWidth(imageVieTwo.bounds), CGRectGetHeight(imageVieTwo.bounds))];
    imageVieThree.image=[UIImage imageNamed:@"serviceorde_icon_adress"];
    [_serviceView addSubview:imageVieThree];
    _serciceAddress=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageVieThree.frame)+10, CGRectGetMinY(imageVieThree.frame), VIEW_WIDTH-30-CGRectGetWidth(imageVieThree.bounds)-10, CGRectGetHeight(imageVieThree.bounds))];
    _serciceAddress.font=[UIFont systemFontOfSize:FontSize(16)];
    _serciceAddress.textColor=[UIColor colorWithHexString:@"#555555"];
    [_serviceView addSubview:_serciceAddress];
    
   
    
    
    
}


@end
