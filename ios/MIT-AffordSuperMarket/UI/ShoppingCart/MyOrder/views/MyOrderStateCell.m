//
//  MyOrderStateCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
#define kleftSpace 15

#import "Global.h"
#import "MyOrderStateCell.h"
#import "UIColor+Hex.h"
#import "MyOrderStateModel.h"
#import "NSString+TextSize.h"
@interface MyOrderStateCell()

//时间
@property (nonatomic,strong)UILabel *timeLabel;
//主标题
@property (nonatomic,strong)UILabel *titleLabel;
//描述标题
@property (nonatomic,strong)UILabel *detailLabel;

@end
@implementation MyOrderStateCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberView];
    }
    return  self;
    
}
#pragma mark --初始化控件
-(void)setUpSuberView
{
    //主标题
    _titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(kleftSpace,0,VIEW_WIDTH/2,HightScalar(50))];
    [self.contentView addSubview:_titleLabel];
     _titleLabel.font=[UIFont systemFontOfSize:FontSize(17)];
    _titleLabel.textColor=[UIColor colorWithHexString:@"#555555"];
    
//    //描述标题
//    _detailLabel =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame),CGRectGetMaxY(self.titleLabel.frame)+7,self.bounds.size.width-kleftSpace*2,20)];
//    [self.contentView addSubview:_detailLabel];
//    _detailLabel.font =[UIFont systemFontOfSize:14.0f];
//    
//    _detailLabel.textColor=[UIColor colorWithHexString:@"#999999"];
    //时间
    _timeLabel =[[UILabel alloc]initWithFrame:CGRectZero];
    [self.contentView addSubview:_timeLabel];
    
    _timeLabel.font=[UIFont systemFontOfSize:FontSize(15)];
    _timeLabel.textColor=[UIColor colorWithHexString:@"#999999"];
    
}
- (void)updateData:(NSDictionary *)dataDic {
    self.titleLabel.text = [dataDic objectForKey:@"title"];
   // self.detailLabel.text = [dataDic objectForKey:@"subTitle"];
    self.timeLabel.text = [dataDic objectForKey:@"time"];
    CGRect timeFrame = self.timeLabel.frame;
    CGSize timeSize = [self.timeLabel.text sizeWithFont:[UIFont systemFontOfSize:HightScalar(15)] maxSize:CGSizeMake(180, 44)];
    timeFrame.origin.x = VIEW_WIDTH - kleftSpace - timeSize.width;
    timeFrame.origin.y = CGRectGetMinY(_titleLabel.frame);
    timeFrame.size.width = timeSize.width;
    timeFrame.size.height = CGRectGetHeight(_titleLabel.bounds);
    self.timeLabel.frame = timeFrame;
}

@end
