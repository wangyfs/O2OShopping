//
//  MyGoodsTradeFinishedCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsTradeFinishedCell.h"
//untils
#import "Global.h"
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "NSString+Conversion.h"
//vendor
#import "UIImageView+AFNetworking.h"
#define korderLabelHight HightScalar(44)
#define kleftSpace 15
#define ksepLineHight 0.7
#define kimageHight HightScalar(77)
#define kvSpace HightScalar(8)
#define khSpace 12
@interface MyGoodsTradeFinishedCell()
{
    CGFloat cellHight;
}
@property(nonatomic, strong)UILabel *orderNumLabel;
@property(nonatomic, strong)UILabel *priceLabel;
@end
@implementation MyGoodsTradeFinishedCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        cellHight = HightScalar(180) + 2*ksepLineHight;
        [self setUpSuberView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setUpSuberView {
    //订单号
    NSString *orderTitle = @"订单号：";
    CGSize orderTitleSize = [orderTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(16)]
                                             maxSize:CGSizeMake(120, korderLabelHight)];
    UILabel *orderTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                       , 2
                                                                       , orderTitleSize.width
                                                                        , korderLabelHight)];
    orderTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    orderTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    orderTitleLabel.text = orderTitle;
    [self.contentView addSubview:orderTitleLabel];
    
    _orderNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(orderTitleLabel.frame)
                                                             , CGRectGetMinY(orderTitleLabel.frame)
                                                             , VIEW_WIDTH - CGRectGetMaxX(orderTitleLabel.frame) - kleftSpace
                                                              , CGRectGetHeight(orderTitleLabel.frame))];
    _orderNumLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    _orderNumLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    [self.contentView addSubview:_orderNumLabel];
    
//    //分割线
    UIImage *image = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"]
                                             frame:CGRectMake(0, 0, 1, 1)];
//    UIImageView *sepLineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
//                                                                                , CGRectGetMaxY(self.orderNumLabel.frame)
//                                                                                , VIEW_WIDTH - kleftSpace
//                                                                                 , ksepLineHight)];
//    sepLineImageView.image = image;
//    [self.contentView addSubview:sepLineImageView];
    
    //价格
    NSString *priceTitle = @"实付款：";
    CGSize priceTitleSize = [orderTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(16)]
                                             maxSize:CGSizeMake(120, korderLabelHight)];
    UILabel *priceTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                        ,cellHight - korderLabelHight - ksepLineHight
                                                                        , priceTitleSize.width
                                                                        , korderLabelHight)];
    priceTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    priceTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    priceTitleLabel.text = priceTitle;
    [self.contentView addSubview:priceTitleLabel];
    
    _priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(priceTitleLabel.frame)
                                                              , CGRectGetMinY(priceTitleLabel.frame)
                                                              , VIEW_WIDTH - CGRectGetMaxX(priceTitleLabel.frame) - kleftSpace
                                                              , CGRectGetHeight(priceTitleLabel.frame))];
    _priceLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    _priceLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    [self.contentView addSubview:_priceLabel];
    
    //底部分割线
    UIImageView *bottomSepLineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0
                                                                                 , cellHight - ksepLineHight
                                                                                 , VIEW_WIDTH
                                                                                 , ksepLineHight)];
    bottomSepLineImageView.image = image;
    [self.contentView addSubview:bottomSepLineImageView];
    
}
//只包含一个商品
- (void)setupSubViewWithImage:(UIImage*)image content:(NSString*)content {
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                         , CGRectGetMaxY(self.orderNumLabel.frame) + ksepLineHight + kvSpace
                                                                         , kimageHight
                                                                          , kimageHight)];
    imageView.image = image;
    [self.contentView addSubview:imageView];
    
    UILabel *contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + khSpace
                                                                    , CGRectGetMinY(imageView.frame) + kvSpace
                                                                    , VIEW_WIDTH - CGRectGetMaxX(imageView.frame) - kleftSpace - khSpace
                                                                     , CGRectGetHeight(imageView.bounds) - 2*kvSpace)];
    contentLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    contentLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    contentLabel.numberOfLines = 0;
    contentLabel.text = content;
    contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.contentView addSubview:contentLabel];
}
//包含多个商品
- (void)setupSubViewWithImages:(NSArray*)images {
    NSInteger count = [images count];
    if (images.count > 3) {
        count = 3;
    }
    
    for (NSInteger i = 0; i < count; i++) {
        CGRect frame = CGRectMake(kleftSpace + (kimageHight + kvSpace)*i
                                   , CGRectGetMaxY(self.orderNumLabel.frame) + ksepLineHight + kvSpace
                                   , kimageHight
                                   , kimageHight);
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
        [imageView setImageWithURL:[NSURL URLWithString:[images objectAtIndex:i]]
                           placeholderImage:[UIImage imageNamed:@"place_image"]];

        [self.contentView addSubview:imageView];
    }
}
#pragma mark --给视图赋值
- (void)updateViewWithData:(NSDictionary*)dataDic {
    
    if ([[dataDic objectForKey:@"GOODSLIST"] count] == 0) {
        return ;
    }
    self.orderNumLabel.text = [NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]];
    self.priceLabel.text =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"AMOUNT"]] floatValue]];
    if ([[dataDic objectForKey:@"GOODSLIST"] count] == 1) {//只包含一个商品
        NSDictionary *goodsInfo = [[dataDic objectForKey:@"GOODSLIST"] firstObject];
        UIImage *goodsImage = nil;
         NSString *imageurl =  [NSString stringTransformObject:[goodsInfo objectForKey:@"THUMB"]];
        if (imageurl.length == 0) {
            goodsImage = [UIImage imageNamed:@"place_image"];
        }
        goodsImage = [UIImage createImageWithImageUrlString:imageurl];
        [self setupSubViewWithImage:goodsImage content:[NSString stringTransformObject:[goodsInfo objectForKey:@"NAME"]]];
        
    } else if ([[dataDic objectForKey:@"GOODSLIST"] count] > 1) {//包含多个商品
        NSMutableArray *images = [[NSMutableArray alloc]init];
        for (NSInteger i=0; i<[[dataDic objectForKey:@"GOODSLIST"] count]; i++) {
            NSDictionary *dic =[[dataDic objectForKey:@"GOODSLIST"] objectAtIndex:i];
            NSString *imageurl = [NSString stringTransformObject:[dic objectForKey:@"THUMB"]];
            [images addObject:[NSString appendImageUrlWithServerUrl:imageurl]];
        }
        [self setupSubViewWithImages:images];
    }
    
}

- (CGFloat)cellFactHight {
    return HightScalar(180) + 2*ksepLineHight;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
