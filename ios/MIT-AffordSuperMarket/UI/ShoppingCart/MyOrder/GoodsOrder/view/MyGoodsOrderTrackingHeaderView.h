//
//  MyGoodsOrderTrackingHeaderView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsOrderTrackingHeaderView
 Created_Date： 20160315
 Created_People：GT
 Function_description： 商品订单追踪table头部视图
 ***************************************/

#import <UIKit/UIKit.h>

@interface MyGoodsOrderTrackingHeaderView : UIView
//初始化视图根据标题个数
- (id)initWithFrame:(CGRect)frame titles:(NSArray*)titles;
//给视图赋值
- (void)updateViewWithDatas:(NSArray*)datas;
@end
