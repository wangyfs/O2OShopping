//
//  MyGoodsSureOrderBottomView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/8.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsSureOrderBottomView
 Created_Date： 20160408
 Created_People：GT
 Function_description： 商品提交订单底部视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol MyGoodsSureOrderBottomViewDelegate<NSObject>
//提交订单
- (void)submitOrder;

@end
@interface MyGoodsSureOrderBottomView : UIView
@property(nonatomic, unsafe_unretained)id<MyGoodsSureOrderBottomViewDelegate>delegate;
- (id)initWithFrame:(CGRect)frame goodsType:(NSInteger)goodsType storeGoodsPrice:(NSString*)storeGoodsPrice selfGoodsPrice:(NSString*)selfGoodsPrice goodsTotalPrice:(NSString*)totalPrice;
- (void)updateViewWithGoodsType:(NSInteger)goodsType storeGoodsPayType:(NSString*)storeGoodsPayType selfGoodsPayType:(NSString*)selfGoodsPayType storeGoodsPrice:(NSString*)storeGoodsPrice selfGoodsPrice:(NSString*)selfGoodsPrice goodsTotalPrice:(NSString*)totalPrice offSetstoreGoodsPrice:(NSString*)offSetstoreGoodsPrice offSetselfGoodsPrice:(NSString*)offSetselfGoodsPrice offSetgoodsTotalPrice:(NSString*)offSettotalPrice switchState:(BOOL)switchState;
@end
