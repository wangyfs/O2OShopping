//
//  MyGoodsOrderTrackingCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsOrderTrackingCell
 Created_Date： 20160315
 Created_People：GT
 Function_description： 商品订单追踪cell视图
 ***************************************/

#import <UIKit/UIKit.h>
#import "MyGoodsOrderTrackingModel.h"
@interface MyGoodsOrderTrackingCell : UITableViewCell
- (void)updateViewWithData:(MyGoodsOrderTrackingModel*)model;
- (CGFloat)cellFactHight;
@end
