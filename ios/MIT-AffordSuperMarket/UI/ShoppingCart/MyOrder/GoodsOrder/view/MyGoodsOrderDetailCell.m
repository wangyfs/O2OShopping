//
//  MyGoodsOrderDetailCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
#define KleftSpace 15
#define KcellHight  HightScalar(110)
#import "MyGoodsOrderDetailCell.h"
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+TextSize.h"
#import "NSString+Conversion.h"
#import "UIImageView+AFNetworking.h"
@interface MyGoodsOrderDetailCell()
//商品图片
@property (nonatomic,strong)UIImageView *goodsLogo;
//商品名称
@property (nonatomic,strong)UILabel *goodName;
//商品数量
@property (nonatomic,strong)UILabel *goodNumber;
//商品价格
@property (nonatomic,strong)UILabel *goodMoney;
@end
@implementation MyGoodsOrderDetailCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberView];
    }
    return self;
}
#pragma mark---初始化视图
-(void)setUpSuberView
{
    //商品LOGO
    _goodsLogo =[[UIImageView alloc]initWithFrame:CGRectMake(KleftSpace, (KcellHight - HightScalar(75))/2, HightScalar(75), HightScalar(75))];
    [self.contentView addSubview:_goodsLogo];
    //商品名字
    _goodName =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsLogo.frame)+10, CGRectGetMinY(self.goodsLogo.frame), HightScalar(165), CGRectGetHeight(self.goodsLogo.bounds)/2)];
    _goodName.textColor=[UIColor colorWithHexString:@"#555555"];
    _goodName.font=[UIFont systemFontOfSize:FontSize(15)];
    _goodName.numberOfLines =2;
    //    _goodName.adjustsFontSizeToFitWidth =YES;
    //    _goodMoney.minimumScaleFactor = 0.8;
    [self.contentView addSubview:self.goodName];
    
    
    
    //商品价格
    _goodMoney =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.goodName.frame), 0, VIEW_WIDTH -CGRectGetWidth(self.goodsLogo.bounds)-CGRectGetWidth(self.goodName.bounds)-KleftSpace*2-10, KcellHight)];
    _goodMoney.font =[UIFont systemFontOfSize:FontSize(16)];
    
    _goodMoney.textColor=[UIColor colorWithHexString:@"#555555"];
    _goodMoney.textAlignment=NSTextAlignmentRight;
    [self.contentView addSubview:self.goodMoney];
    //商品数量
    _goodNumber =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.goodName.frame), CGRectGetMaxY(self.goodName.frame), CGRectGetWidth(self.goodName.bounds),CGRectGetHeight(self.goodName.bounds))];
    _goodNumber.font =[UIFont systemFontOfSize:FontSize(16)];
    _goodNumber.textColor=[UIColor colorWithHexString:@"#555555"];
    _goodNumber.textAlignment=NSTextAlignmentLeft;
    [self.contentView addSubview:self.goodNumber];
}
//requestDataType  商品订单 是1 服务订单是 0
-(void)retSubViewWithData:(NSMutableDictionary *)dictionary
{

    [self.goodsLogo setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[NSString stringTransformObject:[dictionary objectForKey:@"THUMB"]]]]placeholderImage:[UIImage imageNamed:@"place_image"]];
    self.goodName.text=[NSString stringTransformObject:[dictionary objectForKey:@"NAME"]];
    
    self.goodMoney.text=[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dictionary objectForKey:@"PRICE"]] floatValue]];
    NSString *num =[NSString stringWithFormat:@"X%@",[NSString stringTransformObject:[dictionary objectForKey:@"COUNT"]]];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:num];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize(10)] range:NSMakeRange(0, 1)];
    self.goodNumber.attributedText =str;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
