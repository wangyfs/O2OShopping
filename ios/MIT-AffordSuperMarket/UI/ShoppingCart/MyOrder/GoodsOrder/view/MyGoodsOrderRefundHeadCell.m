//
//  MyGoodsOrderRefundHeadCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOrderRefundHeadCell.h"
//untils
#import "Global.h"
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
#define kleftSpace 15
#define ksepLineHight 0.7
@interface MyGoodsOrderRefundHeadCell()
@property(nonatomic, strong)UILabel *orderNumLabel;
@property(nonatomic, strong)UILabel *applyTime;//申请时间
@property(nonatomic, strong)UILabel *refundState;//退款状态
@end
@implementation MyGoodsOrderRefundHeadCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setUpSuberView {
    //订单号
    _orderNumLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    _orderNumLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    _orderNumLabel.font = [UIFont systemFontOfSize:FontSize(13)];
    [self.contentView addSubview:_orderNumLabel];
    
    _applyTime = [[UILabel alloc]initWithFrame:CGRectZero];
    _applyTime.textColor = [UIColor colorWithHexString:@"#999999"];
    _applyTime.font = [UIFont systemFontOfSize:FontSize(13)];
    [self.contentView addSubview:_applyTime];
    
    
    _refundState = [[UILabel alloc]initWithFrame:CGRectZero];
    _refundState.textColor = [UIColor colorWithHexString:@"#db3b34"];
    _refundState.font = [UIFont systemFontOfSize:FontSize(16)];
    [self.contentView addSubview:_refundState];
}
- (void)updateViewWithData:(MyGoodSOrderRefundModel *)refundModel
{
    NSString *orderNum=[NSString stringWithFormat:@"退款单号：%@",[NSString stringTransformObject:refundModel.refundNum]];
    _orderNumLabel.text =orderNum;
    CGSize orderNumSize = [orderNum sizeWithFont:[UIFont systemFontOfSize:FontSize(13)]
                                             maxSize:CGSizeMake(VIEW_WIDTH/3*2, HightScalar(64)/2)];
    _orderNumLabel.frame =CGRectMake(kleftSpace, 10, orderNumSize.width, HightScalar(64)/2-10);
   
    NSString *applyTime=[NSString stringWithFormat:@"申请时间：%@",[NSString stringTransformObject:refundModel.createDate]];
    _applyTime.text =applyTime;
    CGSize applyTimeSize = [applyTime sizeWithFont:[UIFont systemFontOfSize:FontSize(13)]
                                           maxSize:CGSizeMake(VIEW_WIDTH/3*2, HightScalar(64)/2)];
    _applyTime.frame =CGRectMake(kleftSpace, CGRectGetMaxY(self.orderNumLabel.frame), applyTimeSize.width,CGRectGetHeight(self.orderNumLabel.bounds));
    
    
    NSString *refundState=[NSString stringTransformObject:[self setStateTextWithState:refundModel.state]];
    _refundState.text =refundState;
    CGSize refundStateSize = [refundState sizeWithFont:[UIFont systemFontOfSize:FontSize(16)]
                                           maxSize:CGSizeMake(VIEW_WIDTH/3*2, HightScalar(64)/2)];
    _refundState.frame =CGRectMake(VIEW_WIDTH-15 - refundStateSize.width, 0, refundStateSize.width, HightScalar(64));
    
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(NSString*)setStateTextWithState:(NSString *)state
{
    if ([state isEqualToString:@"1"]) {
        return @"审核中";
    }else if([state isEqualToString:@"2"]){
        return @"审核通过";
    }else if([state isEqualToString:@"3"]){
        return @"审核不通过";
    }else if([state isEqualToString:@"4"]){
        return @"已退款";
    }else{
        return @"";
    }
}
@end
