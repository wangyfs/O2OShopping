//
//  MyGoodsDetailBottomView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsDetailBottomView.h"
#import "BottomCountdownView.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
#import "UIImage+ColorToImage.h"
#import "ManagerGlobeUntil.h"
@interface MyGoodsDetailBottomView()
@property(nonatomic,strong)BottomCountdownView *countDownView;
@property(nonatomic, strong)NSMutableArray *buttonArr;//存放buton数组
@property(nonatomic,strong)UIView *noPayView;
@property(nonatomic,strong)UIView *otherStateView;
@end
@implementation MyGoodsDetailBottomView

-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if (self) {
        [self setSubView];
        _buttonArr =[[NSMutableArray alloc]init];
    }
    return self;
}

-(void)operationMyMarkclick:(UIButton *)sender
{
    UIButton *button =(UIButton *)sender;
    if (self.delegate && [self.delegate respondsToSelector:@selector(bottomButtonSelectedWithButtonTitle:)]) {
        [self.delegate bottomButtonSelectedWithButtonTitle:button.titleLabel.text];
    }
}

-(void)setButtonWithDictionary:(NSMutableDictionary *)dataDic
{
    
    NSString *PayType=[NSString stringTransformObject:[dataDic objectForKey:@"PAYTYPE"]];
    NSString *DeliveType=[NSString stringTransformObject:[dataDic objectForKey:@"TAKETYPE"]];
    NSString *PayState=[NSString stringTransformObject:[dataDic objectForKey:@"PAYSTATE"]];
    NSString *DeliveState=[NSString stringTransformObject:[dataDic objectForKey:@"DELIVERSTATE"]];
    NSString *OrderState=[NSString stringTransformObject:[dataDic objectForKey:@"ORDERSTATE"]];
    NSString *packState=[NSString stringTransformObject:[dataDic objectForKey:@"PACKSTATE"]];
    NSString *eavlState=[NSString stringTransformObject:[dataDic objectForKey:@"EVALSTATE"]];
    self.countDownView.timestamp =[NSString stringWithFormat:@"%.0f",[[NSString stringTransformObject:[dataDic objectForKey:@"REMAIN"]]floatValue]/1000].integerValue;
    [self.countDownView timesBeginning];
//    NSLog(@"%ld",(long)self.countDownView.timestamp);
    [self setupBottomViewWithPayType:PayType DeliveType:DeliveType PayState:PayState DeliveState:DeliveState OrderState:OrderState packState:packState EvalState:eavlState];
    
}
- (void)setupBottomViewWithPayType:(NSString*)payType DeliveType:(NSString*)DeliveType PayState:(NSString*)PayState DeliveState:(NSString*)DeliveState OrderState:(NSString*)OrderState packState:(NSString *)packState EvalState:(NSString *)evalState
{
    for (UIButton *btn in self.buttonArr) {
        [btn removeFromSuperview];
    }
    NSString *firstButtonTitle = @"";
    NSString *secondButtonTitle = @"";
    NSString *thirdButtonTitle =@"";
    if ([OrderState isEqualToString:KOrderStateNormal] ){//订单正常  支付状态为 未支付
        if ([payType isEqualToString:KOrderPayTypeIsPayPal] && [DeliveType isEqualToString:KOrderDeliverTypeIsDelivery])//在线付款+送货上门
        {
            if ([PayState isEqualToString:KOrderPayStateIsNo]) {
                firstButtonTitle = @"取消订单";
                secondButtonTitle = @"去支付";
            }else if([PayState isEqualToString:KOrderPayStateIsYes] &&[DeliveState isEqualToString:KOrderDeliverStateIsNo])//订单正常  支付状态为 已支付
            {
                if ([packState isEqualToString:KOrderPackState1]) {
                    firstButtonTitle = @"取消订单";
                    secondButtonTitle = @"催单";
                    
                }else if([packState isEqualToString:KOrderPackState2]){
                    firstButtonTitle = @"催单";
                }
                else if([packState isEqualToString:KOrderPackState3]){
                    firstButtonTitle = @"催单";
                }
                else if([packState isEqualToString:KOrderPackState4]){
                    firstButtonTitle = @"催单";
                }
            }else if([DeliveState isEqualToString:KOrderDeliverStateIsYes]){//订单正常  收货状态为 已发货
                firstButtonTitle = @"确认收货";
            }
        }else if([payType isEqualToString:KOrderPayTypeIsPayPal]&& [DeliveType isEqualToString:KOrderDeliverTypeIsSelfUp]){//自取 +在线付款
            
            if([PayState isEqualToString:KOrderPayStateIsNo]){
                firstButtonTitle = @"取消订单";
                secondButtonTitle = @"去支付";
                
            }else if([PayState isEqualToString:KOrderPayStateIsYes])
            {
                if ([packState isEqualToString:KOrderPackState1]) {
                    firstButtonTitle = @"取消订单";
                    
                }else{
                     firstButtonTitle = @"确认取货";
                }
               
                
            }
        }else if([payType isEqualToString:KOrderPayTypeIsCOD]&&[DeliveType isEqualToString:KOrderDeliverTypeIsDelivery]) //货到付款 + 送货上门
        {
            
            if([DeliveState isEqualToString:KOrderDeliverStateIsNo]){
                if ([packState isEqualToString:KOrderPackState1]) {
                    firstButtonTitle = @"取消订单";
                    secondButtonTitle = @"催单";
                }else if([packState isEqualToString:KOrderPackState2]){
                    firstButtonTitle = @"取消订单";
                    secondButtonTitle = @"催单";                }
                else if([packState isEqualToString:KOrderPackState3]){
                    firstButtonTitle = @"取消订单";
                    secondButtonTitle = @"催单";
                }
                else if([packState isEqualToString:KOrderPackState4]){
                    firstButtonTitle = @"取消订单";
                    secondButtonTitle = @"催单";
                }
            }else if ([DeliveState isEqualToString:KOrderDeliverStateIsYes])//2
            {
                    firstButtonTitle = @"确认收货";
        }
            
        }else if([payType isEqualToString:KOrderPayTypeIsCOD]&&[DeliveType isEqualToString:KOrderDeliverTypeIsSelfUp]) //货到付款 + 自取
        {
            if ([packState isEqualToString:KOrderPackState1]) {
                firstButtonTitle = @"取消订单";
                
            }else if([packState isEqualToString:KOrderPackState2]){
                firstButtonTitle = @"取消订单";
                               }
            else if([packState isEqualToString:KOrderPackState3]){
                firstButtonTitle = @"取消订单";
            }
            else if([packState isEqualToString:KOrderPackState4]){
               firstButtonTitle = @"取消订单";
                secondButtonTitle = @"确认取货";

            }
        }
    }else if([OrderState isEqualToString:KOrderStateFinish])//订单状态为已完成
    {
        if([evalState isEqualToString:KOrderEvalStateIsNo]){
            firstButtonTitle = @"去评价";
        }else if ([evalState isEqualToString:KOrderEvalStateIsYse]){
            firstButtonTitle = @"删除订单";
        }
    }else if ([OrderState isEqualToString:KOrderStateCancel])//订单状态为已取消
    {
        firstButtonTitle = @"删除订单";
    } else if ([OrderState isEqualToString:KOrderStateBeOverdue])//订单状态为已过期
    {
        firstButtonTitle = @"删除订单";
    }
    [self buttonWithButtonTitleFirstButtonTitle:firstButtonTitle SecondButtonTitle:secondButtonTitle ThirdButtonTitle:thirdButtonTitle];
}
-(void)buttonWithButtonTitleFirstButtonTitle:(NSString *)firstButtonTitle SecondButtonTitle:(NSString *)secondButtonTitle ThirdButtonTitle:(NSString *)thirdButtonTitle
{
    for (UIButton *btn in self.buttonArr) {
        [btn removeFromSuperview];
    }
    CGFloat leftSpace = 0;
    CGFloat hSpace  = 0;
    NSArray *titles = nil;
    CGFloat itemWidth =0;
    if (firstButtonTitle.length != 0) {
        if (firstButtonTitle.length != 0 && secondButtonTitle.length != 0 &&thirdButtonTitle.length ==0) {//两个button
           
            titles = @[firstButtonTitle,secondButtonTitle];
            if ([firstButtonTitle isEqualToString:@"取消订单"] && [secondButtonTitle isEqualToString:@"去支付"] ) {
                leftSpace = 15;
                hSpace  = 15;
                itemWidth= (VIEW_WIDTH/2+30- hSpace - leftSpace)/titles.count;
            }else{
                leftSpace = 15;
                hSpace  = 30;
                itemWidth= (VIEW_WIDTH - hSpace - 2*leftSpace)/titles.count;
            }
            
        } else if(firstButtonTitle.length != 0 && secondButtonTitle.length == 0){//一个button
            leftSpace = HightScalar(100);
            titles = @[firstButtonTitle];
            itemWidth = (VIEW_WIDTH - hSpace - 2*leftSpace)/titles.count;
        }else{
            leftSpace = 10;
            hSpace  = 10;
            titles = @[firstButtonTitle,secondButtonTitle,thirdButtonTitle];
            itemWidth = (VIEW_WIDTH - hSpace*2 - 2*leftSpace)/titles.count;
        }
        
        UIImage *deleteOrderBackgroundImage = [UIImage createImageWithColor:[UIColor whiteColor]
                                                                      frame:CGRectMake(0, 0, itemWidth,HightScalar(45))];
        for (NSInteger i = 0; i < titles.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
             if ([firstButtonTitle isEqualToString:@"取消订单"] && [secondButtonTitle isEqualToString:@"去支付"] )
             {
                  button.frame = CGRectMake(VIEW_WIDTH/2-30 +(itemWidth + hSpace)*i,(self.bounds.size.height- HightScalar(45))/2, itemWidth, HightScalar(45));
                
             }else{
                  button.frame = CGRectMake(leftSpace +(itemWidth + hSpace)*i,(self.bounds.size.height- HightScalar(45))/2, itemWidth, HightScalar(45));
            }
           
            [button setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
            [button.layer setMasksToBounds:YES];
            [button.layer setCornerRadius:2];
            if([[titles objectAtIndex:i] isEqualToString:@"去支付"] || [[titles objectAtIndex:i] isEqualToString:@"去评价"] ||[[titles objectAtIndex:i] isEqualToString:@"确认收货"])
            {
                [button.layer setBorderColor:[UIColor colorWithHexString:@"#c52720"].CGColor];
                [button setTitleColor:[UIColor colorWithHexString:@"#c52720"] forState:UIControlStateNormal];
            }else{
                [button.layer setBorderColor:[UIColor colorWithHexString:@"#555555"].CGColor];
                [button setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
            }
            [button setBackgroundImage:deleteOrderBackgroundImage forState:UIControlStateNormal];
            [button.layer setBorderWidth:0.5];
                        button.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
            [button addTarget:self action:@selector(operationMyMarkclick:) forControlEvents:UIControlEventTouchUpInside];
            if ([firstButtonTitle isEqualToString:@"取消订单"] && [secondButtonTitle isEqualToString:@"去支付"] )
            {
                 [self.noPayView addSubview:button];
            }else{
                [self.otherStateView addSubview:button];
            }
            

            [_buttonArr addObject:button];
        }
    }
    if([firstButtonTitle isEqualToString:@"取消订单"] && [secondButtonTitle isEqualToString:@"去支付"])
    {
        [self insertSubview:self.noPayView aboveSubview:self.otherStateView];
        
    }else{
        [self insertSubview:self.otherStateView aboveSubview:self.noPayView];
    }
}
#pragma mark -- 初始化控件
-(void)setSubView
{
    self.noPayView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, self.frame.size.height)];
    self.noPayView.backgroundColor =[UIColor colorWithHexString:@"#efefef"];
    [self addSubview:self.noPayView];
    self.otherStateView =[[UIView alloc]initWithFrame:self.noPayView.frame];
    self.otherStateView.backgroundColor =[UIColor colorWithHexString:@"#efefef"];
    [self addSubview:self.otherStateView];
    
    [self.noPayView addSubview:self.countDownView];
}
-(void)timesStop
{
    [self.countDownView timesStop];
}
-(BottomCountdownView *)countDownView
{
    if (_countDownView == nil) {
        UILabel *lable =[[UILabel alloc]init];
        lable.text =@"付款剩余时间:   ";
        lable.frame =CGRectMake(15, 5, [[ManagerGlobeUntil sharedManager]setLabelWidthWithNSString:lable.text Font:FontSize(14)].width, HightScalar(30));
        lable.textColor =[UIColor colorWithHexString:@"#555555"];
        lable.font =[UIFont systemFontOfSize:FontSize(14)];
        [self.noPayView addSubview:lable];
       BottomCountdownView *countDownView = [BottomCountdownView shareCountDown];
       countDownView.frame = CGRectMake(CGRectGetMinX(lable.frame),CGRectGetMaxY(lable.frame)-10, CGRectGetWidth(lable.bounds), HightScalar(35));
        
        //    countDown.backgroundImageName = @"search_k";
        //    self.countDownView.timerStopBlock = ^{
        //        NSLog(@"时间停止");
        //    };
        _countDownView =countDownView;

    }
    return _countDownView;
}
@end
