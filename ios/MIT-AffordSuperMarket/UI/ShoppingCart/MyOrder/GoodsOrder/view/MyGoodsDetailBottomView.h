//
//  MyGoodsDetailBottomView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyGoodsDetailBottomViewDelegate <NSObject>

-(void)bottomButtonSelectedWithButtonTitle:(NSString *)title;

@end

@interface MyGoodsDetailBottomView : UIView
-(void)setButtonWithDictionary:(NSMutableDictionary *)dataDic;
@property(nonatomic,assign)id<MyGoodsDetailBottomViewDelegate> delegate;
-(void)timesStop;
@end
