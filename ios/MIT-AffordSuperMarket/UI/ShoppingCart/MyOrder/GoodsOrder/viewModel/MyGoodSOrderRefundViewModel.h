//
//  MyGoodSOrderRefundViewModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： MyGoodsOrderTrackingViewModel
 Created_Date： 20160315
 Created_People：jsq
 Function_description： 商品退款数据处理
 ***************************************/
#import <Foundation/Foundation.h>

@interface MyGoodSOrderRefundViewModel : NSObject
@property(nonatomic, strong)NSMutableArray *dataSource;
- (void)parameters:(NSMutableDictionary *)parameters requestSuccess:(void (^)(BOOL isSuccess))success failure:(void(^)(NSString *failureInfo))failureInfo;
@property(nonatomic,strong)NSString *stateMent;
@property(nonatomic,strong)NSString *totalCount;
@end