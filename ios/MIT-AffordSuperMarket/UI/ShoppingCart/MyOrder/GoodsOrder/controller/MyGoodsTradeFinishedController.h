//
//  MyGoodsTradeFinishedController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsTradeFinishedController
 Created_Date： 20160314
 Created_People：GT
 Function_description： 商品交易完成视图
 ***************************************/

#import "BaseViewController.h"

@interface MyGoodsTradeFinishedController : BaseViewController
@property(nonatomic, strong)NSDictionary *goodsOrderInfoDic;//商品订单信息
@end
