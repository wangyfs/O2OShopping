//
//  MyGoodsOrderController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/3.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： MyGoodsOrderController
 Created_Date： 20160314
 Created_People：JSQ
 Function_description： 商品订单列表
 ***************************************/

#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, MyGoodOrderType) {
    kGoodWholeType = 100,//全部订单
    kGoodNoPayMoneyType = 101,//待付款
    kGoodNoReceivingType = 102,//待收货
    kGoodNoEvaluateType , //待评价
};

@interface MyGoodsOrderController : BaseViewController
@property(nonatomic)MyGoodOrderType myGoodOrderType;
@property(nonatomic)NSInteger selectedIndex;
@property(nonatomic,strong)NSString *PopType;
@end
