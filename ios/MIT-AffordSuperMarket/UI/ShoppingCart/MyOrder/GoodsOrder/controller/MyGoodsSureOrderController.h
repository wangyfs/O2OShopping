//
//  MyGoodsSureOrderController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/8.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsSureOrderController
 Created_Date： 20160408
 Created_People：GT
 Function_description： 商品提交订单页面
 ***************************************/

#import "BaseViewController.h"
#import "ReceivedAddressOBJECT.h"
@interface MyGoodsSureOrderController : BaseViewController
@property(nonatomic, strong)ReceivedAddressOBJECT *addressData;
@property(nonatomic, strong)NSString *markContent;
@property(nonatomic, strong)NSMutableArray *storeGoodsArr;
@property(nonatomic, strong)NSMutableArray *selfGoodsArr;
@end
