//
//  MyGoodsOrderRefundController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOrderRefundController.h"
#import "EmptyPageView.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "AppDelegate.h"
#import "NSString+Conversion.h"
//vendor
#import "MTA.h"
#import "RDVTabBarController.h"
#import "MJRefresh.h"
//view
#import "MyGoodsOrderRefundHeadCell.h"
#import "MyGoodsOrderDetailCell.h"
#import "MyGoodsOrderRefundFootCell.h"
//viewModel
#import "MyGoodSOrderRefundViewModel.h"
//model
#import "MyGoodSOrderRefundModel.h"

#define ksectionHeaderHihgt HightScalar(12)

@interface MyGoodsOrderRefundController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSInteger _requestPage;//当前请求页
    NSInteger _totalCount;//数据总条数
}
@property(nonatomic, strong)MyGoodSOrderRefundViewModel *viewModel;
@property(nonatomic, strong)UITableView *tableView;
//空视图
@property (nonatomic,strong)EmptyPageView *emptyView;
@end

@implementation MyGoodsOrderRefundController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    self.title = @"退款";
    [self addBackNavItem];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.emptyView];
    _requestPage = 1;
    [self requestDataWithrequestPage:_requestPage requestCount:kRequestLimit];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"商品退款"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"商品退款"];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}

#pragma mark -- requestData
- (void)requestDataWithrequestPage:(NSInteger)requestPage requestCount:(NSInteger)requestCount{
    __weak typeof(self) weakSelf = self;
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    UserInfoModel *userInfoMode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    XiaoQuAndStoreInfoModel *xiaoquModel =[[ManagerGlobeUntil sharedManager]getxiaoQuAndStoreInfo];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringTransformObject:userInfoMode.token] forKey:@"TOKEN"];
     [parameter setObject:[NSString stringTransformObject:xiaoquModel.storeID] forKey:@"STOREID"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)requestPage] forKey:@"PAGENUM"];
    [parameter setObject:[NSNumber numberWithInteger:requestCount] forKey:@"PAGESIZE"];
//    [parameter setObject:self.orderNumber.length == 0 ? @"" : self.orderNumber forKey:@"ORDERNUM"];
    [self.viewModel parameters:parameter requestSuccess:^(BOOL isSuccess) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if (weakSelf.viewModel.dataSource.count == 0) {
            _tableView.hidden=YES;
            _emptyView.hidden=NO;
        }else{
            _tableView.hidden=NO;
            _emptyView.hidden=YES;}
        
        [weakSelf.tableView reloadData];
        [weakSelf endRefresh];
    } failure:^(NSString *failureInfo) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:failureInfo inView:weakSelf.view];
        [weakSelf.tableView reloadData];
        [weakSelf endRefresh];
    }];
}
#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.viewModel.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.viewModel.dataSource.count !=0) {

    MyGoodSOrderRefundModel *model =(MyGoodSOrderRefundModel *)[self.viewModel.dataSource objectAtIndex:section] ;
    return  model.goodsList.count+2;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (self.viewModel.dataSource.count !=0) {
        MyGoodSOrderRefundModel *model =(MyGoodSOrderRefundModel *)[self.viewModel.dataSource objectAtIndex:indexPath.section];
        if (indexPath.row ==0) {//订单号和时间和退款状态
            static NSString *cellIdentifier = @"cellHeaddentifier";
            MyGoodsOrderRefundHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (!cell) {
                cell = [[MyGoodsOrderRefundHeadCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                cell.backgroundColor =[UIColor whiteColor];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.backgroundColor =[UIColor whiteColor];
            [cell updateViewWithData:model];
            return cell;
        }else if(indexPath.row ==model.goodsList.count+1){//退款金额，拒绝退款理由
            static NSString *cellIdentifier = @"celldentifier";
            MyGoodsOrderRefundFootCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (!cell) {
                cell = [[MyGoodsOrderRefundFootCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                 cell.backgroundColor =[UIColor whiteColor];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            [cell updateViewWithData:model stateMent:self.viewModel.stateMent];
           
            return cell;
            
        }else{//
            NSString *cellIdentifier = @"cellContentIdentifier";
            MyGoodsOrderDetailCell*cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(!cell) {
                cell=[[MyGoodsOrderDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor =[UIColor whiteColor];
            }
            [cell retSubViewWithData:[model.goodsList objectAtIndex:indexPath.row-1]];
            
            return cell;
            
        }

    }else{
          NSString *cell1Identifier = @"cellNumber";
        UITableViewCell*cell=[tableView dequeueReusableCellWithIdentifier:cell1Identifier];
            if(!cell) {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell1Identifier];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            return cell;
    }
    
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
//    return [(MyGoodsOrderTrackingCell*)cell cellFactHight];
    if (self.viewModel.dataSource.count !=0) {
        MyGoodSOrderRefundModel *model =(MyGoodSOrderRefundModel *)[self.viewModel.dataSource objectAtIndex:indexPath.section];
        if (indexPath.row ==0) {
            return HightScalar(64);
        }else if (indexPath.row ==model.goodsList.count+1){
            UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
            return [(MyGoodsOrderRefundFootCell*)cell cellFactHight];
        }else{
            return HightScalar(110);
        }
    }else{
        return 1;
    }
    
    
}
#pragma mark - 上拉/下拉  更新数据
-(void)headerRereshing {
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        _requestPage = 1;
         [self requestDataWithrequestPage:_requestPage requestCount:kRequestLimit];
    }else{
        [self endRefresh];
    }
    
}
-(void)footerRereshing
{
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        if (_requestPage*kRequestLimit < _totalCount) {
            
            _requestPage++;
             [self requestDataWithrequestPage:_requestPage requestCount:kRequestLimit];
        } else {
            
            [_tableView setFooterRefreshingText:@"已加载完全部订单~"];
            
            
            [self endRefresh];
        }
        
    } else {
        [self endRefresh];
        
    }
    
}
- (void)endRefresh
{
    [self.tableView headerEndRefreshing];
    [self.tableView footerEndRefreshing];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (self.viewModel.dataSource.count!=0) {
        if (section ==self.viewModel.dataSource.count-1) {
            return 0;
        }
    }
    return ksectionHeaderHihgt;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, ksectionHeaderHihgt)];
    view.backgroundColor = [UIColor clearColor];
    [view.layer setBorderWidth:0.7];
    [view.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    return view;
}
#pragma mark -- button Action
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -- 初始化视图
- (UITableView*)tableView {
    if (!_tableView) {
        CGRect tableFrame = CGRectMake(0, 0, VIEW_WIDTH, CGRectGetHeight(self.view.bounds) - 64);
        UITableView *tableview = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
        tableview.delegate = self;
        tableview.dataSource = self;
        tableview.backgroundColor =[UIColor colorWithHexString:@"#efefef"];
        tableview.allowsSelection = NO;
        tableview.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        UIView *footView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 0.1)];
        tableview.tableFooterView =footView;
        [tableview addFooterWithTarget:self action:@selector(footerRereshing)];
        [tableview addHeaderWithTarget:self action:@selector(headerRereshing)];
        _tableView = tableview;
    }
    return _tableView;
}

- (MyGoodSOrderRefundViewModel*)viewModel {
    if (!_viewModel) {
        MyGoodSOrderRefundViewModel *viewmodel = [[MyGoodSOrderRefundViewModel alloc]init];
        _viewModel = viewmodel;
    }
    return _viewModel;
}
-(EmptyPageView *)emptyView
{
    if (!_emptyView) {
       EmptyPageView *emptyView =[[EmptyPageView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH,_tableView.bounds.size.height) EmptyPageViewType:kGoodsOrderRefundView];
        emptyView.backgroundColor=[UIColor whiteColor];
        _emptyView = emptyView;
    }
    return _emptyView;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
