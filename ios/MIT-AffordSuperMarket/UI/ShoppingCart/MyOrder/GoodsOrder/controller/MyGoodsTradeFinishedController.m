//
//  MyGoodsTradeFinishedController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsTradeFinishedController.h"
#import "Evaluate_Store_Controller.h"
#import "Home_Root_Controller.h"
//unitls
#import "UIColor+Hex.h"
#import "Global.h"
#import "AppDelegate.h"
#import "NSString+TextSize.h"
#import "NSString+Conversion.h"
#import "UIImage+ColorToImage.h"
#import "MyGoodsOrderDetailController.h"
//vendor
#import "MTA.h"
#import "RDVTabBarController.h"
//view
#import "MyGoodsTradeFinishedHeaderView.h"
#import "MyGoodsTradeFinishedCell.h"
#define ktableHeaderHight  HightScalar(208)
#define ksectionHeaderHihgt 12
#define kbottomViewHight 72
#define kleftSpace 16
#define khSpace 40
#define ktopSpace 13
@interface MyGoodsTradeFinishedController ()
@property(nonatomic, strong)UIView *bottomView;
@property(nonatomic, strong)MyGoodsTradeFinishedHeaderView *tableHeaderView;
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)NSArray *titles;
@end

@implementation MyGoodsTradeFinishedController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    self.title = @"交易完成";
    self.titles = @[@"查看订单",@"返回首页"];
    [self addBackNavItem];
    [self setupSubView];
   
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"交易完成页面"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"交易完成页面"];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.goodsOrderInfoDic count] != 0 ? 1 : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    MyGoodsTradeFinishedCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[MyGoodsTradeFinishedCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell updateViewWithData:self.goodsOrderInfoDic];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return [(MyGoodsTradeFinishedCell*)cell cellFactHight];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return ksectionHeaderHihgt;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, ksectionHeaderHihgt)];
    view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [view.layer setBorderWidth:0.7];
    [view.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    return view;
}
#pragma mark -- MyGoodsTradeFinishedHeaderViewDelegate 
//去评价
- (void)goEvaluate {
    Evaluate_Store_Controller *evalVc =[[Evaluate_Store_Controller alloc]init];
    evalVc.orderNum =[NSString stringTransformObject:[self.goodsOrderInfoDic objectForKey:@"ORDERNUM"]];
    [self.navigationController pushViewController:evalVc animated:YES];
}
#pragma mark -- button Action
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
//查看订单详情 或 返回首页
- (void)beatButtonEvent:(id)sender {
    UIButton *button = (UIButton*)sender;
    if (button.tag == 100) {//查看订单详情
        BOOL isContainOrderDetailVC = NO;
        for (UIViewController*VC in self.navigationController.childViewControllers) {
            if ([VC isKindOfClass:[MyGoodsOrderDetailController class]]) {//从订单详情页面跳转到订单完成，直接返回
                isContainOrderDetailVC = YES;
                [self.navigationController popToViewController:VC animated:YES];
            }
        }
        
        if (!isContainOrderDetailVC) {//从订单列表页面跳转到订单完成，需要push到订单详情
            MyGoodsOrderDetailController *orderDetailVC = [[MyGoodsOrderDetailController alloc]init];
            orderDetailVC.orderNum = [NSString stringTransformObject:[self.goodsOrderInfoDic objectForKey:@"ORDERNUM"]];
            [self.navigationController pushViewController:orderDetailVC animated:YES];
        }
    }else if (button.tag == 101) {//返回首页
       
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [self.navigationController setViewControllers:@[[self.navigationController.childViewControllers firstObject]]];
        [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
        Home_Root_Controller *homeVC = [[[appDelegate.tabBarController.viewControllers firstObject] childViewControllers] firstObject];
        [homeVC.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];
        [appDelegate selectedControllerAtTabBarIndex:0];
    }
}

#pragma mark -- 初始化视图
- (void)setupSubView {
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
}
#pragma mark -- getter method
- (MyGoodsTradeFinishedHeaderView*)tableHeaderView {
    if (!_tableHeaderView) {
        CGRect headerFrame = CGRectMake(0, 0, VIEW_WIDTH, ktableHeaderHight);
        MyGoodsTradeFinishedHeaderView *headerView = [[MyGoodsTradeFinishedHeaderView alloc]initWithFrame:headerFrame];
        headerView.delegate = (id<MyGoodsTradeFinishedHeaderViewDelegate>)self;
        _tableHeaderView = headerView;
    }
    return _tableHeaderView;
}
- (UITableView*)tableView {
    if (!_tableView) {
        CGRect tableFrame = CGRectMake(0, 0, VIEW_WIDTH, CGRectGetHeight(self.view.bounds) - 64 - kbottomViewHight);
        UITableView *tableview = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
        tableview.delegate = (id<UITableViewDelegate>)self;
        tableview.dataSource = (id<UITableViewDataSource>)self;
        tableview.allowsSelection = NO;
        tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableview.backgroundColor = [UIColor clearColor];
        tableview.tableHeaderView = self.tableHeaderView;
        _tableView = tableview;
    }
    return _tableView;
}

- (UIView*)bottomView {
    if (!_bottomView) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0
                                                              , CGRectGetMaxY(self.tableView.frame)
                                                              , VIEW_WIDTH
                                                               , kbottomViewHight)];
        view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
        CGFloat buttonWidth = (VIEW_WIDTH - 2*kleftSpace - khSpace)/2;
        CGFloat buttonHight = kbottomViewHight - 2*ktopSpace;
        for (NSInteger i = 0; i < self.titles.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(kleftSpace + (buttonWidth + khSpace)*i , ktopSpace, buttonWidth, buttonHight);
            [button setTitle:[self.titles objectAtIndex:i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
            [button setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#ffffff"] frame:CGRectMake(0, 0, 1, 1)]
                              forState:UIControlStateNormal];
//            [button setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#efefef"] frame:CGRectMake(0, 0, 1, 1)]
//                              forState:UIControlStateHighlighted];
            [button.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
            [button.layer setBorderWidth:1];
            [button.layer setMasksToBounds:YES];
            [button.layer setCornerRadius:3];
            button.tag = 100 +i;
            [button addTarget:self action:@selector(beatButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:button];
            
        }
        _bottomView = view;
    }
    return _bottomView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
