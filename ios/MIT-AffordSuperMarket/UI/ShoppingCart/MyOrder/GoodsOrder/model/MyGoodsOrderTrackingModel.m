//
//  MyGoodsOrderTrackingModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOrderTrackingModel.h"

@implementation MyGoodsOrderTrackingModel


+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{
             @"orderType" : @"TYPE",
             @"handlePeople" : @"USER",
             @"handleTitle" : @"ACTION",
             @"handleContent" : @"CONTENT",
             @"handleDate" : @"CREATEDATE",
             };
}
@end
