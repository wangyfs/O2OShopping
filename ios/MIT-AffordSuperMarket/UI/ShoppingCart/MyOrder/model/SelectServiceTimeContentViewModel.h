//
//  SelectServiceTimeContentViewModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SelectServiceTimeContentViewModel
 Created_Date： 20151224
 Created_People：gt
 Function_description： 选择服务时间内容view Model
 ***************************************/

#import <Foundation/Foundation.h>

@interface SelectServiceTimeContentViewModel : NSObject
- (void)initDataWithArray:(NSArray*)dataArr;
- (NSArray*)getServiceTimeContentWithSelectedDateIndex:(NSInteger)index;
@end
