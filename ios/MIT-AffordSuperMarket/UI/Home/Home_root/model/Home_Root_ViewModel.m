//
//  Home_Root_ViewModel.m
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Home_Root_ViewModel.h"
#import "ManagerHttpBase.h"
#import "XiaoQuAndStoreInfoModel.h"
#import "NSString+Conversion.h"
#import "ManagerGlobeUntil.h"
#import "Global.h"
typedef void(^RequestSuccess)(NSDictionary* responseData);
@interface Home_Root_ViewModel()
@property(nonatomic,copy)RequestSuccess requestSuccess;
@property(nonatomic, strong)NSMutableArray * dataArray;//数据源
@property(nonatomic, strong)NSMutableArray * loopImagesArr;//数据源
@end
@implementation Home_Root_ViewModel
-(id)init
{
    self = [super init];
    if (self) {
        _dataArray =[[NSMutableArray alloc]init];
        _loopImagesArr =[[NSMutableArray alloc]init];
    }
    return self;
}
- (void)loadHomeHeaderDataSuccess:(void(^)(NSDictionary* responseData))success failue:(void(^)(bool isFailure))failure {
    
    self.requestSuccess = success;
    [self loadHomeLoopImageData];
    
}
//请求头部数据
-(void)loadHomeHeaderTagData
{
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    [parameter setObject:mode.storeID forKey:@"STOREID"];
     __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"goods/tags" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            // NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [weakSelf handleHomeHeaderTagData:dataDic];
            }else {//请求失败
                
            }

        }
        
    } failure:^(bool isFailure) {
        
        
    }];

}
//请求小惠通知数据
- (void)loadNotificationInof {
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    [parameter setObject:mode.storeID forKey:@"STOREID"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"store/tip" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            // NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                NSString *title = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"CONTENT"]];//状态码
                [weakSelf handleBigKindData:title];
            }else {//请求失败
                [weakSelf handleBigKindData:@""];
            }

        }
        
    } failure:^(bool isFailure) {
         [weakSelf handleBigKindData:@""];
        
    }];

}
//请求头部轮播图
-(void)loadHomeLoopImageData
{
     //请求参数说明： SIGNATURE  设备识别码
        ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
        NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
     __weak typeof(self)weakSelf = self;
        [manager parameters:parameter customPOST:@"ad/publicAdList" success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = (NSDictionary*)responseObject;
                // NSString *msg = [dataDic objectForKey:@"MSG"];
                NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
                if ([state isEqualToString:@"0"]) {//请求成功
                    [weakSelf.loopImagesArr removeAllObjects];
                    [weakSelf.dataArray removeAllObjects];
                    [weakSelf handleLoopImagesData:dataDic];
                    
                }else {//请求失败
                    
                }
            }
            
        } failure:^(bool isFailure) {
            
            
        }];
}
//处理轮播图数据
- (void)handleLoopImagesData:(NSDictionary*)dataDic {
    NSMutableArray *imagesDataArr = [[NSMutableArray alloc]initWithArray:[dataDic objectForKey:@"OBJECT"]];
    NSMutableArray *imagesUrlsArr = [[NSMutableArray alloc]init];
    for (NSDictionary *dic in imagesDataArr) {
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",baseImageUrl,[dic objectForKey:@"THUMB"]];
        [imagesUrlsArr addObject:imageUrl];
    }
    
    [self.loopImagesArr addObjectsFromArray:imagesUrlsArr];
    [self loadNotificationInof];
}
//处理大类数据
-(void)handleBigKindData:(NSString*)sectionTitle
{
    NSArray * bigKindData = @[@{@"title":@"生鲜蔬果",@"imageName":@"home_btn_vegetables"},@{@"title":@"生活百货",@"imageName":@"home_btn_departmentoflife"}
                              ,@{@"title":@"粮油调料",@"imageName":@"home_btn_seasoning"},@{@"title":@"酒水饮料",@"imageName":@"home_btn_drinks"}];
    NSDictionary *dic = @{@"viewType":@"bigKind",@"Data":bigKindData,@"sectionTitle":sectionTitle};
    [self.dataArray addObject:dic];
     [self loadHomeHeaderTagData];
}
//处理活动数据
-(void)handleHomeHeaderTagData:(NSDictionary *)dataDic
{
    NSMutableArray *headerTagData = [[NSMutableArray alloc]initWithArray:[dataDic objectForKey:@"OBJECT"]];
    
    for (NSDictionary *dic in headerTagData) {
        NSDictionary *dicdata = @{@"viewType":@"headerTag",@"Data":dic};
        [self.dataArray addObject:dicdata];
    }
    [self handleServiceData];

}
//处理 生活服务数据
-(void)handleServiceData
{
    NSArray * serviceData = @[@"home_liveservice",@"home_homeWork",@"home_flower"];
    NSDictionary *dic = @{@"viewType":@"service",@"Data":serviceData};
    [self.dataArray addObject:dic];
    [self recommendTitle];
    
}
-(void)recommendTitle
{
    NSArray * recommendTitle = @[@"精品推荐"];
    NSDictionary *dic = @{@"viewType":@"recommend",@"Data":recommendTitle};
    [self.dataArray addObject:dic];
    [self handleData];

}
-(void)handleData
{
    NSDictionary *dic =@{@"loopData":self.loopImagesArr,@"otherData":self.dataArray};
    self.requestSuccess(dic);
}

@end
