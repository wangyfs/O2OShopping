//
//  Service_RootCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Service_RootCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "XiaoQuAndStoreInfoModel.h"
#import "ManagerGlobeUntil.h"
#import "UIImage+ColorToImage.h"
#import "Home_TableView_BigKindCollectionCell.h"
#define kcollectionItemHight HightScalar(100) 
#define khSepLineHight 0.7
@interface Service_RootCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic, strong)NSMutableArray *dataSource;
@property(nonatomic, strong)UICollectionView *collectionView;
@property(nonatomic, strong)UIImageView *sepLineView;
@end
@implementation Service_RootCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _dataSource = [[NSMutableArray alloc]init];
        [self setUpSubview];
    }
    return self;
}

#pragma mark -- 初始化视图
- (void)setUpSubview {
    [self.contentView addSubview:self.collectionView];
    [self.contentView addSubview:self.sepLineView];
}

- (UICollectionView*)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(VIEW_WIDTH/4, kcollectionItemHight - khSepLineHight);
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing =0;
        UICollectionView *collectionview = [[UICollectionView alloc]initWithFrame:CGRectMake(0
                                                                                             , 0
                                                                                             , VIEW_WIDTH
                                                                                             , kcollectionItemHight - khSepLineHight) collectionViewLayout:layout];
        collectionview.backgroundColor = [UIColor whiteColor];
        [collectionview registerClass:[Home_TableView_BigKindCollectionCell class] forCellWithReuseIdentifier:@"Home_TableView_BigKindCollectionCell"];
        [collectionview registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Home_TableView_BigKindCollectionCell"];
        collectionview.delegate = self;
        collectionview.dataSource = self;
        collectionview.userInteractionEnabled = YES;
        _collectionView = collectionview;
    }
    return _collectionView;
}
- (UIImageView*)sepLineView {
    if (!_sepLineView) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.collectionView.frame), CGRectGetWidth(self.collectionView.frame), khSepLineHight)];
        imageView.backgroundColor = [UIColor colorWithHexString:@"#dddddd"];
        _sepLineView = imageView;
    }
    return _sepLineView;
}
-(void)setUpSuberViewWithCategory:(NSMutableArray *)category {
    [self.dataSource removeAllObjects];
    [self.dataSource addObjectsFromArray:[category copy]];
    CGFloat viewFactHight = 0;
    if (category.count > 4 && category.count <= 8) {//显示两行
        viewFactHight = 2*kcollectionItemHight - khSepLineHight;
    } else if (category.count > 0 && category.count <= 4) {//显示一行
        viewFactHight = kcollectionItemHight - khSepLineHight;
    } else if (category.count > 8 && category.count <= 12) {
        viewFactHight = 3*kcollectionItemHight - khSepLineHight;
    }
    self.collectionView.frame = CGRectMake(0, 0, VIEW_WIDTH, viewFactHight);
     self.frame = CGRectMake(0, 0, VIEW_WIDTH, viewFactHight + khSepLineHight);;
    CGRect sepLineFrame = self.sepLineView.frame;
    sepLineFrame.origin.y = kcollectionItemHight;
    self.sepLineView.frame = sepLineFrame;
   
    [self.collectionView reloadData];
}


#pragma mark -- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Home_TableView_BigKindCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Home_TableView_BigKindCollectionCell" forIndexPath:indexPath];
    [cell updateServiceViewWithData:[_dataSource objectAtIndex:indexPath.row]];
    [cell setTitleColor:@"#777777"];
    return cell;
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    Cell.alpha = 0.2;
    
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    Cell.alpha = 1;
}
#pragma mark -- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItemWithItemContent:)]) {
        [_delegate selectedItemWithItemContent:[self.dataSource objectAtIndex:indexPath.row]];
    }
}

- (CGFloat)cellFactHight {
    
    if (self.dataSource.count != 0) {
        if (self.dataSource.count <= 4) {
            return kcollectionItemHight + khSepLineHight;
        } else if (self.dataSource.count <= 8) {
            return 2*kcollectionItemHight + khSepLineHight;
        }else if (self.dataSource.count <= 12) {
            return  3*kcollectionItemHight + khSepLineHight;
        }
        
    }
    return 0;
}
@end
