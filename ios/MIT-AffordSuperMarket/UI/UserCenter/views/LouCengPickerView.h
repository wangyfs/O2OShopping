//
//  LouCengPickerView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
/***************************************
 ClassName： LouCengPickerView
 Created_Date： 20151123
 Created_People： GT
 Function_description： 选择楼层，单元，门牌号  （1.楼存放到楼数组 2.单元存放到单元数组（每个楼的单元）3.创建单元section数组用户存放每栋楼每个单元的房门号，它和单元处以同一等级 4.把每个单元的房间号存放到对应的单元（单元存放到section）
 ***************************************/
@class LouCengInfoModel;
@protocol LouCengPickerViewDelegate <NSObject>
- (void)selectedLouNumberModel:(LouCengInfoModel*)louNumberModel danYuanNumberModel:(LouCengInfoModel*)danyuanNumberModel roomNumberModel:(LouCengInfoModel*)roomNumberModel;
@optional


@end
@interface LouCengPickerView : UIView
@property (nonatomic , unsafe_unretained) id <LouCengPickerViewDelegate> delegate;
- (id)initWithFrame:(CGRect)frame withComponentContents:(NSArray*)conponentContents delegate:(id<LouCengPickerViewDelegate>)delegate;
- (void)showInView:(UIView *)view;
- (void)cancelPicker;
@end
