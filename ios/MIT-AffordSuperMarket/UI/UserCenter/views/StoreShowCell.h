//
//  StoreShowCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
/***************************************
 ClassName： StoreShowCell
 Created_Date： 20151210
 Created_People： GT
 Function_description： 门店展示cell
 ***************************************/
#import "StoreShowModel.h"
@protocol StoreShowCellDelegate <NSObject>
- (void)callTelePhoneWithIndexPath:(NSIndexPath*)indexPath;
@end
@interface StoreShowCell : UITableViewCell
@property (nonatomic, strong)NSIndexPath *indexpath;
@property(nonatomic, unsafe_unretained)id<StoreShowCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property(nonatomic, strong)StoreShowModel *model;

@end
