//
//  ReceivedAddressCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ReceivedAddressCell
 Created_Date： 20151118
 Created_People： GT
 Function_description： 收货地址cell
 ***************************************/

#import <UIKit/UIKit.h>
@protocol ReceivedAddressCellDelegate <NSObject>
- (void)selectedReceivedAddressWithItemIndex:(NSInteger)index;
@end
@interface ReceivedAddressCell : UITableViewCell
@property(nonatomic, unsafe_unretained)id<ReceivedAddressCellDelegate>delegate;
@property(nonatomic, strong)NSIndexPath *itemIndexPath;
@property(nonatomic, strong)UILabel *defautMarkLabel;
- (void)updataWithName:(NSString*)name telNumber:(NSString*)telNumber address:(NSString*)address;
@end
