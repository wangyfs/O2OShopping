//
//  UserInfoShowView.m
//  BG-Clound
//
//  Created by Mega on 14/11/1.
//  Copyright (c) 2014年 zd. All rights reserved.
//

#import "UserInfoShowView.h"
#define kleftSpace 15
#define ktopSpace 20
#define kHSpace 10 //视图水平间隙
#define kVSpace 4 //垂直间隙
#define kuserImageHight 80
#define klabelHeight 18
#define ksepWidth 2
//untils
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
#import "Global.h"
@interface UserInfoShowView ()
@property(nonatomic, strong)UIImageView *backGroundimageView;
//登录视图
@property(nonatomic, strong)UIView *loginView;
@property(nonatomic, strong)UIButton *useNameBtn;//用户名

@property(nonatomic, strong)UIButton *userAddressShowBtn;//显示用户地址
@property(nonatomic, strong)UIButton *addressJumpBtn;//用户地址跳转
@property(nonatomic, strong)UIButton *membersBtn;//会员跳转
@property(nonatomic, strong)UILabel *membersLable;//会员类别
//未登录视图
@property(nonatomic, strong)UIButton *unLoginView;

@property(nonatomic, strong)UIButton *guanZhuButtonView;
@property(nonatomic, strong)UIButton *historyButtonView;
@property(nonatomic, strong)UILabel *guanZhuCountLabel;//关注商品个数
@property(nonatomic, strong)UILabel *historyCountLabel;//浏览记录个数

@end
@implementation UserInfoShowView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        [self setupSubView];
        [self showUserInfoWithLoginStatus:NO];
    }
    return self;
}
#pragma mark -- 初始化子视图
- (void)setupSubView {
    //背景视图
    _backGroundimageView = [[UIImageView alloc]initWithFrame:CGRectMake(0
                                                                        , 0
                                                                        , CGRectGetWidth(self.bounds)
                                                                        , CGRectGetHeight(self.bounds))];
    _backGroundimageView.userInteractionEnabled = YES;
    _backGroundimageView.image=[UIImage imageNamed:@"bg_head_userconter"];
    [self addSubview:_backGroundimageView];
    
    /*        未登录视图        */
    _unLoginView = [UIButton buttonWithType:UIButtonTypeCustom];
    _unLoginView.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), HightScalar(120));
    _unLoginView.backgroundColor = [UIColor clearColor];
    [_unLoginView addTarget:self action:@selector(beatHeaderViewEvent) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_unLoginView];
    //用户头像
    UIImageView *unLoginUserImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                                     , CGRectGetMidY(_unLoginView.frame) - kuserImageHight/2
                                                                                     , kuserImageHight
                                                                                     , kuserImageHight)];
    unLoginUserImageView.image = [UIImage imageNamed:@"icon_headportrait"];
    [_unLoginView addSubview:unLoginUserImageView];
    
    //标题
    NSString *title = @"登录/注册";
    CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:FontSize(18)] maxSize:CGSizeMake(160, klabelHeight)];
    UILabel *titleLabel = [self setLabelWithTitle:@"登录/注册" fontSize:FontSize(18) textColor:@""];
    titleLabel.frame = CGRectMake(CGRectGetMaxX(unLoginUserImageView.frame) + kHSpace
                                  , CGRectGetMidY(_unLoginView.frame) - 5 - klabelHeight
                                  , titleSize.width
                                  , klabelHeight);
    
    [_unLoginView addSubview:titleLabel];
    
    //副标题
    NSString *subTitle = @"登录后可享受更多优惠";
    CGSize subTitleSize = [subTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(170, klabelHeight)];
    UILabel *subTitleLabel = [self setLabelWithTitle:subTitle fontSize:FontSize(16) textColor:@""];
    subTitleLabel.frame = CGRectMake(CGRectGetMinX(titleLabel.frame)
                                     , CGRectGetMaxY(titleLabel.frame) + 10
                                     , subTitleSize.width, klabelHeight);
    [_unLoginView addSubview:subTitleLabel];
    
    //右边指示箭头
    UIImage *arrowImage = [UIImage imageNamed:@"btn_white_jiantou"];
    UIImageView *arrowImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.bounds) - kleftSpace - arrowImage.size.width
                                                                              , CGRectGetMidY(_unLoginView.frame) - arrowImage.size.height/2
                                                                              , arrowImage.size.width
                                                                               , arrowImage.size.height)];
    arrowImageView.image = arrowImage;
    [_unLoginView addSubview:arrowImageView];
    
    
     /*        登录视图        */
    _loginView = [[UIView alloc]initWithFrame:CGRectMake(0
                                                          , 0
                                                          , CGRectGetWidth(self.bounds)
                                                           , HightScalar(120))];
    _loginView.backgroundColor = [UIColor clearColor];
//    _loginView.userInteractionEnabled = YES;
    [self addSubview:_loginView];
    
   
    //用户头像
    
    _userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                 , ktopSpace
                                                                 , HightScalar(kuserImageHight)
                                                                 , HightScalar(kuserImageHight))];
    _userImageView.userInteractionEnabled = YES;
    
    [_loginView addSubview:_userImageView];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(beatUserTouXiang)];
    [_userImageView addGestureRecognizer:tapGesture];
    //皇冠
    _CrownLogo = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace+(CGRectGetWidth(_userImageView.bounds)-30)/2
                                                                        , CGRectGetMinY(_userImageView.frame)-15
                                                                        , 30
                                                              , 20)];
    _CrownLogo.image=[UIImage imageNamed:@"user_btn_logo_crown"];
    _CrownLogo.hidden =YES;
    [_loginView addSubview:_CrownLogo];

    //用户名
   
    _useNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _useNameBtn.backgroundColor = [UIColor clearColor];
    NSString *userNameStr = @"13383848566";
    CGSize userNameSize = [userNameStr sizeWithFont:[UIFont systemFontOfSize:16] maxSize:CGSizeMake(120, 38)];
    _useNameBtn.frame = CGRectMake(CGRectGetMaxX(_userImageView.frame) + kHSpace
                                   , CGRectGetMinY(_userImageView.frame) + 4
                                   , userNameSize.width
                                   , 38);
    [_useNameBtn addTarget:self action:@selector(beatUseNameEvent) forControlEvents:UIControlEventTouchUpInside];
    _useNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _useNameBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [_loginView addSubview:_useNameBtn];
    
   
    _membersBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _membersBtn.backgroundColor = [UIColor blackColor];
    NSString *membersStr = @"普通用户";
    CGSize membersSize = [membersStr sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(120, 38)];
    _membersBtn.frame = CGRectMake(CGRectGetMaxX(_useNameBtn.frame) + kHSpace
                                   , CGRectGetMinY(_useNameBtn.frame)+7
                                   , membersSize.width+20
                                   , CGRectGetHeight(_useNameBtn.bounds)-14);
    [_membersBtn addTarget:self action:@selector(beatUserMembers) forControlEvents:UIControlEventTouchUpInside];
    _membersBtn.alpha=0.3;
    _membersBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _membersBtn.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    [_membersBtn.layer setMasksToBounds:YES];
    [_membersBtn.layer setCornerRadius:4.0];
    //会员
     _membersLable=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_useNameBtn.frame) + kHSpace
                                                                             , CGRectGetMinY(_membersBtn.frame)
                                                                             , CGRectGetWidth(_membersBtn.bounds)
                                                                             , CGRectGetHeight(_membersBtn.bounds))];
    _membersLable.backgroundColor=[UIColor clearColor];
    _membersLable.text = membersStr;
    _membersLable.textColor = [UIColor whiteColor];
    _membersLable.font =[UIFont systemFontOfSize:FontSize(16)];
    _membersLable.textAlignment =NSTextAlignmentCenter;
    [_loginView addSubview:_membersBtn];
    [_loginView addSubview:_membersLable];
    //显示用户地址
    
    //用户地址
    _userAddressShowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _userAddressShowBtn.frame = CGRectMake(CGRectGetMinX(_useNameBtn.frame)
                                       , CGRectGetMaxY(_useNameBtn.frame)
                                       , CGRectGetWidth(_useNameBtn.bounds)+ 40
                                       , HightScalar(30));
    _userAddressShowBtn.backgroundColor = [UIColor clearColor];
    [_userAddressShowBtn setTitle:@"我的收货地址" forState:UIControlStateNormal];
    [_userAddressShowBtn setImage:[UIImage imageNamed:@"Myaccount_btn_adress"] forState:UIControlStateDisabled];
    _userAddressShowBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _userAddressShowBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    _userAddressShowBtn.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    _userAddressShowBtn.enabled = NO;
    [_loginView addSubview:_userAddressShowBtn];
    
    _addressJumpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _addressJumpBtn.frame = CGRectMake(CGRectGetWidth(self.bounds) - kleftSpace - 90, CGRectGetMinY(_userAddressShowBtn.frame) - 5  , 90, 38);
    [_addressJumpBtn setImage:[UIImage imageNamed:@"btn_white_jiantou"] forState:UIControlStateNormal];
    _addressJumpBtn.backgroundColor = [UIColor clearColor];
    _addressJumpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [_addressJumpBtn addTarget:self action:@selector(beatUserAddressEvent) forControlEvents:UIControlEventTouchUpInside];
    [_loginView addSubview:_addressJumpBtn];
    
    //关注商品
    
    CGFloat width = (CGRectGetWidth(self.bounds) - ksepWidth)/2;
    _guanZhuButtonView = [UIButton buttonWithType:UIButtonTypeCustom];
    _guanZhuButtonView.frame = CGRectMake(0
                                         , CGRectGetMaxY(_unLoginView.frame)
                                         , width
                                         , HightScalar(61));
    UIImage *btnimage = [UIImage createImageWithColor:[UIColor clearColor] frame:CGRectMake(0, 0,CGRectGetWidth(_guanZhuButtonView.bounds) , CGRectGetHeight(_guanZhuButtonView.bounds))];
    _guanZhuButtonView.backgroundColor = [UIColor clearColor];
    [_guanZhuButtonView addTarget:self action:@selector(beatGuanZhuProductEvent) forControlEvents:UIControlEventTouchUpInside];
    [_guanZhuButtonView setImage:btnimage forState:UIControlStateHighlighted];
    //透明灰色背景
    UIView *viewone=[[UIView alloc]initWithFrame:CGRectMake(0
                                                            , CGRectGetMaxY(_unLoginView.frame)
                                                            , width
                                                            , HightScalar(61))];
    viewone.backgroundColor=[UIColor blackColor];
    viewone.alpha=0.1;
    [_backGroundimageView addSubview:viewone];
    
    [_backGroundimageView addSubview:_guanZhuButtonView];
    
    //content
    _guanZhuCountLabel = [self setLabelWithTitle:@"0" fontSize:FontSize(14) textColor:@""];
    _guanZhuCountLabel.frame = CGRectMake(0
                                          , 0
                                          , CGRectGetWidth(_guanZhuButtonView.bounds)
                                          , CGRectGetHeight(_guanZhuButtonView.bounds)/2);
    
    _guanZhuCountLabel.textAlignment = NSTextAlignmentCenter;
    [_guanZhuButtonView addSubview:_guanZhuCountLabel];
    //title
    UILabel *guanZhuTitleLabel = [self setLabelWithTitle:@"关注的商品" fontSize:FontSize(14) textColor:@""];
    guanZhuTitleLabel.frame = CGRectMake(CGRectGetMinX(_guanZhuCountLabel.frame)
                                         , CGRectGetMaxY(_guanZhuCountLabel.frame)
                                         , CGRectGetWidth(_guanZhuCountLabel.bounds)
                                         , CGRectGetHeight(_guanZhuCountLabel.bounds));
    guanZhuTitleLabel.textAlignment = NSTextAlignmentCenter;
    [_guanZhuCountLabel addSubview:guanZhuTitleLabel];
    //分割线
    UIImageView *sepLineView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_guanZhuButtonView.frame)
                                                                           , CGRectGetMinY(_guanZhuButtonView.frame)
                                                                           , ksepWidth
                                                                           , CGRectGetHeight(_guanZhuButtonView.bounds))];
    sepLineView.backgroundColor = [UIColor clearColor];
    [_backGroundimageView addSubview:sepLineView];
    
    //浏览历史
    
    _historyButtonView = [UIButton buttonWithType:UIButtonTypeCustom];
    _historyButtonView.frame = CGRectMake(CGRectGetMaxX(sepLineView.frame)
                                          , CGRectGetMinY(_guanZhuButtonView.frame)
                                          , CGRectGetWidth(_guanZhuButtonView.bounds)
                                          , CGRectGetHeight(_guanZhuButtonView.bounds));
    _historyButtonView.backgroundColor = [UIColor clearColor];
    [_historyButtonView addTarget:self action:@selector(lookHistoryEvent) forControlEvents:UIControlEventTouchUpInside];
    //透明灰色背景
    UIView *viewtwo=[[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(sepLineView.frame)
                                                            , CGRectGetMinY(_guanZhuButtonView.frame)
                                                            , CGRectGetWidth(_guanZhuButtonView.bounds)
                                                            , CGRectGetHeight(_guanZhuButtonView.bounds))];
    viewtwo.backgroundColor=[UIColor blackColor];
    viewtwo.alpha=0.1;
    [_backGroundimageView addSubview:viewtwo];
    [_backGroundimageView addSubview:_historyButtonView];
    
    //content
    _historyCountLabel = [self setLabelWithTitle:@"0" fontSize:FontSize(14) textColor:@""];
    _historyCountLabel.frame = CGRectMake(0
                                          , 0
                                          , CGRectGetWidth(_historyButtonView.bounds)
                                          , CGRectGetHeight(_historyButtonView.bounds)/2);
    _historyCountLabel.textAlignment = NSTextAlignmentCenter;
    [_historyButtonView addSubview:_historyCountLabel];
    [_historyButtonView setImage:btnimage forState:UIControlStateHighlighted];
    //title
    UILabel *historyTitleLabel = [self setLabelWithTitle:@"浏览历史" fontSize:FontSize(14) textColor:@""];
    historyTitleLabel.frame = CGRectMake(CGRectGetMinX(_historyCountLabel.frame)
                                         , CGRectGetMaxY(_historyCountLabel.frame)
                                         , CGRectGetWidth(_historyCountLabel.bounds)
                                         , CGRectGetHeight(_historyCountLabel.bounds));
    historyTitleLabel.textAlignment = NSTextAlignmentCenter;
    [_historyButtonView addSubview:historyTitleLabel];
    
}

//生成label对象
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    //label.textColor = [UIColor colorWithHexString:textColor];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
//改变用户头部视图
- (void)showUserInfoWithLoginStatus:(BOOL)islogoin {
    if (islogoin) {//已登录
        self.loginView.hidden =  NO;
        [self bringSubviewToFront:self.loginView];
        self.unLoginView.hidden = YES;
        
    } else {//未登录
        self.unLoginView.hidden = NO;
        [self bringSubviewToFront:self.unLoginView];
        self.loginView.hidden = YES;
    }
}
#pragma mark -- setter method

- (void)setTitle:(NSString *)title {
    [self.useNameBtn setTitle:title forState:UIControlStateNormal];
    _title = title;
}
-(void)setMembersBtnTitle:(NSString *)membersBtnTitle
{
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:membersBtnTitle];
    NSRange contentRange = {0, [content length]};
    [content addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
    _membersLable.attributedText = content;
    _membersBtnTitle = membersBtnTitle;
}
- (void)setGuanZhuCountText:(NSString *)guanZhuCountText {
    if (guanZhuCountText.length == 0) {
        guanZhuCountText = @"0";
    }
    self.guanZhuCountLabel.text = guanZhuCountText;
}
- (void)setHistoryCountText:(NSString *)historyCountText {
    if (historyCountText.length == 0) {
        historyCountText = @"0";
    }
    self.historyCountLabel.text = historyCountText;
}
#pragma mark -- action
//用户未登录
- (void)beatHeaderViewEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(userUnLogin)]) {
        [_delegate userUnLogin];
    }
}

//用户登录
- (void)beatUserTouXiang {
    if (_delegate && [_delegate respondsToSelector:@selector(manageUserInfo)]) {
        [_delegate manageUserInfo];
    }
}
- (void)beatUseNameEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(manageUserInfo)]) {
        [_delegate manageUserInfo];
    }
}
- (void)beatUserMembers{
    if (_delegate && [_delegate respondsToSelector:@selector(membersJump)]) {
        [_delegate membersJump];
    }
}
- (void)beatUserAddressEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(manageUserRecivedAddress)]) {
        [_delegate manageUserRecivedAddress];
    }
}

- (void)beatGuanZhuProductEvent {
    
    if (_delegate && [_delegate respondsToSelector:@selector(lookGuanZhun)]) {
        [_delegate lookGuanZhun];
    }
}

- (void)lookHistoryEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(lookLiuLanHistory)]) {
        [_delegate lookLiuLanHistory];
    }
}
@end
