//
//  RulesOrFriendController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/10.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
#import <JavaScriptCore/JavaScriptCore.h>
#import "RulesOrFriendController.h"
//controller
//vendor
#import "RDVTabBarController.h"
#import "ManagerHttpBase.h"
//untils
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "AppDelegate.h"
#import "NSString+Conversion.h"
#import "RDVTabBarController.h"
#import "MTA.h"
#import "Global.h"
#import "MBProgressHUD.h"
#import "ManagerGlobeUntil.h"
@interface RulesOrFriendController ()<UIWebViewDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UIWebView *webView;
@property (nonatomic, strong)JSContext *jsContext;
@end

@implementation RulesOrFriendController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    if (_rulesOrFriendType == kRulesType) {
         self.title =@"消费细则";
    }else{
         self.title =@"如何推荐好友";
    }
   
    [self addBackNavItem];
    [self setUpSubView];
    self.jsContext = [[JSContext alloc] init];
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."inView:self.view];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setWebViewData];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"规则或如何推荐好友"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"规则或如何推荐好友"];
}
#pragma mark--获取WEB数据
-(void)setWebViewData
{
//    //添加设备识别码
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString  *urlType=@"";
    if (_rulesOrFriendType ==kRulesType) {
        urlType=@"";
    }else{
        urlType=@"";
    }
    NSString *URLStr =[NSString stringWithFormat:@"%@%@",baseServerUrl,urlType];
    
    URLStr = @"http://www.baidu.com";
    NSURL  *url =[[NSURL alloc]initWithString:URLStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
    
}
#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[ManagerGlobeUntil sharedManager] hideHUDFromeView:self.view];
    self.jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    //    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.zoom=0.34"];
    self.jsContext.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        context.exception = exceptionValue;
        NSLog(@"异常信息：%@", exceptionValue);
    };
}
-(void)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark--初始化控件
-(void)setUpSubView
{
    //初始化webView
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,VIEW_WIDTH , VIEW_HEIGHT-64)];
    _webView.scalesPageToFit = YES;
    _webView.delegate = self;
    _webView.backgroundColor=[UIColor whiteColor];
    _webView.scalesPageToFit = NO;
    [self.view addSubview:_webView];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
