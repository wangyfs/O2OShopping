//
//  MyWalletViewModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface MyWalletViewModel : NSObject
@property(nonatomic,strong)NSString *shiHuiBiMoney;//实惠币金额
@property(nonatomic,strong)NSString *recommendFriend;//推荐好友个数
@property(nonatomic,strong)NSString *searchType;//收益类型 1是自己收益 2是好友收益
@property(nonatomic,assign)NSInteger totalCount;
- (void)requestMyWalletDetailDatasuccess:(void (^)(NSString *responseObjects))success failure:(void(^)(NSString *failureMessage))failureMessage;
- (void)requestProfitWithPageNum:(NSInteger)pageNum PageSize:(NSInteger)pageSize SearchType:(NSString *)searchType success:(void (^)(NSArray *successMsg))success failure:(void(^)(NSString *failureMessage))failureMessage;
@end
