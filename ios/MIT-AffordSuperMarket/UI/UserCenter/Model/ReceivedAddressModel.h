//
//  ReceivedAddressModel.h
//
//  Created by apple  on 16/1/25
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ReceivedAddressModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *mSG;
@property (nonatomic, strong) NSString *sTATE;
@property (nonatomic, strong) NSArray *oBJECT;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
