//
//  StoreShowController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "StoreShowController.h"
//vendor
#import "RDVTabBarController.h"
#import "Global.h"
//untils
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "NSString+Conversion.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "AppDelegate.h"
//model
#import "StoreShowModel.h"
//view
#import "StoreShowCell.h"
#import "MTA.h"
#import "ACETelPrompt.h"
#import "StoreShowRatingCell.h"
@interface StoreShowController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSString *_telPhone;
}
@property(nonatomic, strong)NSMutableDictionary *ratingInfoDic;
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)UIImageView *headerView;
@property(nonatomic, strong)NSMutableArray *dataSource;
@end

@implementation StoreShowController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [self addBackNavItem];
    _dataSource = [[NSMutableArray alloc]init];
    _ratingInfoDic = [[NSMutableDictionary alloc]init];
    [self setupSubView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
         [self loadStoreInfoData];
    }
   
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"门店展示"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"门店展示"];
}

#pragma mark -- requestData 
- (void)loadStoreInfoData {
//    SIGNATURE			设备识别码
//    ID			店铺ID
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.storeID] forKey:@"STOREID"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"store/detail" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            
            if ([state isEqualToString:@"0"]) {//请求成功
                NSDictionary *dic=[dataDic objectForKey:@"OBJECT"];
                [weakSelf operationDataDictionary:dic];
                [_tableView reloadData];
            } else {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
    } failure:^(bool isFailure) {
        
        
    }];
}

- (void)operationDataDictionary:(NSDictionary*)dataDic {
    [self.ratingInfoDic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"EVAL1"]] forKey:@"EVAL1"];
    [self.ratingInfoDic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"EVAL2"]] forKey:@"EVAL2"];
    [self.ratingInfoDic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"EVAL3"]] forKey:@"EVAL3"];
    StoreShowModel *model = [[StoreShowModel alloc]init];
    _telPhone = [NSString stringWithFormat:@"电话：%@",[NSString stringTransformObject:[dataDic objectForKey:@"TEL"]]];
    model.buttonImageName = @"";
    model.content = [NSString stringWithFormat:@"营业时间：%@ - %@",[NSString stringTransformObject:[dataDic objectForKey:@"BEGINTIME"]],[NSString stringTransformObject:[dataDic objectForKey:@"ENDTIME"]]];
    [_dataSource addObject:model];
    
    model = [[StoreShowModel alloc]init];
    model.buttonImageName = @"";
    model.content =[NSString stringWithFormat:@"店铺地址：%@",[NSString stringTransformObject:[dataDic objectForKey:@"ADDRESS"]]];
    [_dataSource addObject:model];
    
    model = [[StoreShowModel alloc]init];
    model.buttonImageName = @"";
    model.content = [NSString stringWithFormat:@"联络人：%@",[NSString stringTransformObject:[dataDic objectForKey:@"MANAGER"]]];
    [_dataSource addObject:model];
    
    model = [[StoreShowModel alloc]init];
    model.buttonImageName = @"mystore_btn_telephone";
    model.content = [NSString stringWithFormat:@"电话：%@",[NSString stringTransformObject:[dataDic objectForKey:@"TEL"]]];
    [_dataSource addObject:model];
    
    model = [[StoreShowModel alloc]init];
    model.buttonImageName = @"";
    model.content = [NSString stringWithFormat:@"邮箱：%@",[NSString stringTransformObject:[dataDic objectForKey:@"MAIL"]]];
    [_dataSource addObject:model];
    
//    model = [[StoreShowModel alloc]init];
//    model.buttonImageName = @"";
//    model.content = @"配送时间：统一配送时间早上10:00至12:00，下午17:00至19:00，分店配送有费用限制，恕不远送，可自提";
//    [_dataSource addObject:model];
    
    model = [[StoreShowModel alloc]init];
    model.buttonImageName = @"";
    model.content =[NSString stringWithFormat:@"配送说明：%@",[NSString stringTransformObject:[dataDic objectForKey:@"DELIVERYDES"]]];
    self.title=[NSString stringTransformObject:[dataDic objectForKey:@"TITLE"]];
    [_dataSource addObject:model];
   UIImage *image = [UIImage createImageWithImageUrlString:[NSString stringTransformObject:[dataDic objectForKey:@"THUMB"]]];
    if (image == nil) {
        image =[UIImage imageNamed:@"productDetail_Default_loopImage"];
    }
    _headerView.image =image;
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    _headerView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), HightScalar(200))];
    _headerView.backgroundColor = [UIColor clearColor];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - 64)
                                             style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.showsVerticalScrollIndicator = NO;
    [_tableView registerNib:[UINib nibWithNibName:@"StoreShowCell" bundle:nil] forCellReuseIdentifier:@"cellIdentifier"];
    _tableView.tableHeaderView = _headerView;
    UIView *view=[[UIView alloc]init];
    _tableView.tableFooterView=view;
    [self.view addSubview:_tableView];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dataSource.count != 0) {
        return self.dataSource.count + 1;
    }
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataSource.count) {
        static NSString *cellRatingIdentifier = @"cellRatingIdentifier";
        StoreShowRatingCell *cellRating = [tableView dequeueReusableCellWithIdentifier:cellRatingIdentifier];
        if (!cellRating) {
            cellRating = [[StoreShowRatingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellRatingIdentifier];
        }
        [cellRating updateViewWithData:[self.ratingInfoDic copy]];
        cellRating.selectionStyle =  UITableViewCellSelectionStyleNone;
        return cellRating;
    }
    static NSString *cellIdentifier = @"cellIdentifier";
    StoreShowCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[StoreShowCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.model = [self.dataSource objectAtIndex:indexPath.row];
    cell.selectionStyle =  UITableViewCellSelectionStyleNone;
    cell.delegate = (id<StoreShowCellDelegate>)self;
    return cell;
}

#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataSource.count != 0) {
        if (indexPath.row == self.dataSource.count) {//店家评价等级
            UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
            return [(StoreShowRatingCell*)cell cellFactHight];
        }
        StoreShowModel *message = self.dataSource[indexPath.row];
        return message.cellHeight;
    }
    return 0;
}

#pragma mark -- StoreShowCellDelegate
- (void)callTelePhoneWithIndexPath:(NSIndexPath*)indexPath {
    [ACETelPrompt callPhoneNumber:_telPhone
                             call:^(NSTimeInterval duration) {
                                 
                             } cancel:^{
                                 
                             }];
}
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
