//
//  UserCenter_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/9.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： UserCenter_Controller
 Created_Date： 20151108
 Created_People： GT
 Function_description： 个人中心
 ***************************************/

#import "BaseViewController.h"
@interface UserCenter_Controller : BaseViewController

@end
