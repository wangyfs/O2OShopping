//
//  SettingViewController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SettingViewController
 Created_Date： 20151123
 Created_People： GT
 Function_description： 设置界面
 ***************************************/

#import "BaseViewController.h"

@interface SettingViewController : BaseViewController

@end
