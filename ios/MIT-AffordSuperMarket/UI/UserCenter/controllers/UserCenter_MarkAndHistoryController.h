//
//  UserCenter_MarkAndHistoryController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/17.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： UserCenter_MarkAndHistoryController
 Created_Date： 20151117
 Created_People： GT
 Function_description： 关注商品和浏览历史页面
 ***************************************/

#import "BaseViewController.h"
//判断第一步是注册还是修改密码（页面判断）
typedef NS_ENUM(NSInteger, MarkOrHistoyType) {
    kmarkType = 0,//关注页面
    khistoryType //浏览历史
};
@interface UserCenter_MarkAndHistoryController : BaseViewController
@property(nonatomic, strong)NSString *navItemTitle;
@property(nonatomic)MarkOrHistoyType markOrHistoyType;
@end
