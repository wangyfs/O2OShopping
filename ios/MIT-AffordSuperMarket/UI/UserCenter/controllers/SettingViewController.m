//
//  SettingViewController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SettingViewController.h"
#define ktopSpace 15
//vendor
#import "RDVTabBarController.h"
//untils
#import "UIColor+Hex.h"
#import "ManagerGlobeUntil.h"
#import "AppDelegate.h"
#import "Global.h"
//view
#import "SettingFooterView.h"
//controler
#import "AboutUsController.h"
#import "MTA.h"
@interface SettingViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)NSArray *titles;
@property(nonatomic, strong)NSArray *images;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    [self addBackNavItem];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
//    _titles = @[@"关于大实惠",@"关于升级"];
//    _images = @[[UIImage imageNamed:@"about_bg"],[UIImage imageNamed:@"checkUpdate_bg"]];
    _titles = @[@"关于大实惠"];
    _images = @[[UIImage imageNamed:@"about_bg"]];
    [self setupSubView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"设置"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"设置"];
}


#pragma mark --UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titles.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.imageView.image = [self.images objectAtIndex:indexPath.row];
    cell.textLabel.text = [self.titles objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    return cell;
}
#pragma mark -- UITableViewDelegate 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return HightScalar(50);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        AboutUsController *aboutUsVC = [[AboutUsController alloc]init];
        [self.navigationController pushViewController:aboutUsVC animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
#pragma mark -- SettingFooterViewDelegate
- (void)quitUserLogin {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否退出账户?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag=2;
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1){
        [[ManagerGlobeUntil sharedManager] clearUserInfoData];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    //底部视图
    SettingFooterView *footView = [[SettingFooterView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 216)];
    footView.delegate = (id<SettingFooterViewDelegate>)self;
    [footView changeSuViewShowWithLoginStatus:[ManagerGlobeUntil sharedManager].transactionLogin];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0
                                                              , ktopSpace
                                                              , CGRectGetWidth(self.view.bounds)
                                                              , CGRectGetHeight(self.view.bounds) - 64 - 2*ktopSpace)
                                             style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.tableFooterView = footView;
    [self.view addSubview:_tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
