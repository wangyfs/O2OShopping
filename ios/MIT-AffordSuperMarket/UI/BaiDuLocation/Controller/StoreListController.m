//
//  StoreListController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/24.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "StoreListController.h"
//untils
#import "Global.h"
#import "AppDelegate.h"
#import "UIColor+Hex.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
//vendor
#import "RDVTabBarController.h"
#import "MTA.h"
//view
#import "BaiDuLocationCell.h"
@interface StoreListController ()
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)NSMutableArray *dataSource;
@end

@implementation StoreListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    self.title = @"选择便利店";
    [self addBackNavItem];
    _dataSource = [[NSMutableArray alloc]init];
    [self.view addSubview:self.tableView];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    [self requestStoreList];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"便利店列表"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"便利店列表"];
}
#pragma mark -- back event
- (void)backBtnAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
#pragma mark -- 请求数据
- (void)requestStoreList {
    //查询百度lbs数据
    //请求参数说明：
    //SIGNATURE  设备识别码
    //KEYWORD 关键字 为空返回全部数据
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:@"" forKey:@"KEYWORD"];
    __weak typeof(self)weakSelf = self;
     [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..." inView:self.view];
    [manager parameters:parameter customPOST:@"common/lbs" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [self.dataSource removeAllObjects];
                [self.dataSource addObjectsFromArray:[dataDic objectForKey:@"OBJECT"]];
                
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
            [self.tableView reloadData];
        }
        
    } failure:^(bool isFailure) {
    
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
    }];
}

#pragma mark -- requestData
- (void)LoadXiaoQuInfoDataWithBaiDuUID:(NSString*)uid {
    //保存小区UID
    //请求参数说明： SIGNATURE  设备识别码
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:uid forKey:@"BAIDUKEY"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"common/location" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                if (dataDic.count != 0) {
                    XiaoQuAndStoreInfoModel *model = [[XiaoQuAndStoreInfoModel alloc]init];
                    model.xiaoQuID = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"COMMUNITY"] objectForKey:@"ID"]];
                    model.xiaoQuName = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"COMMUNITY"] objectForKey:@"TITLE"]];
                    model.xiaoQuAddress = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"COMMUNITY"] objectForKey:@"ADDRESS"]];
                    model.storeID = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"ID"]];
                    model.storeName = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"TITLE"]];
                    model.storeAddress = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"ADDRESS"]];
                    model.storePhone = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"TEL"]];
                    NSString *storeStartTime = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"BEGINTIME"]];
                    NSString *storeEndTime = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"ENDTIME"]];
                    model.storeYingYeTime = [NSString stringWithFormat:@"%@-%@",storeStartTime,storeEndTime];
                    model.storePeiSongIntroduce = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"DELIVERYDES"]];
                    [[ManagerGlobeUntil sharedManager] updataXiaoquAndStoreInfoWithModel:model];
                    [weakSelf dingweiSuccess];
                }
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
    } failure:^(bool isFailure) {
       // [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"网络连接终中断,请重试~" inView:weakSelf.view];
        
    }];
}

- (void)dingweiSuccess {
//    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:PRODUCTCOUNTCHANGE object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:DINGWEISUCCESS object:nil];
//    }];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identfy = @"chanpincanshu";
    BaiDuLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
    if (cell == nil) {
        cell = [[BaiDuLocationCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identfy];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    NSDictionary *dataDic = [self.dataSource objectAtIndex:indexPath.row];
    [cell updateStoreListCellWithDataDic:dataDic];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HightScalar(72);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}
#pragma mark -- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dataDic = [self.dataSource objectAtIndex:indexPath.row];
    [self LoadXiaoQuInfoDataWithBaiDuUID:[NSString stringTransformObject:[dataDic objectForKey:@"GEOTABLE_ID"]]];
}
- (UITableView*)tableView {
    if (!_tableView) {
        CGRect frame = CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT - 64);
        UITableView *tableview = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
        tableview.delegate = (id<UITableViewDelegate>)self;
        tableview.dataSource = (id<UITableViewDataSource>)self;
        _tableView = tableview;
    }
    return _tableView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
