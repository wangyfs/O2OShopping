//
//  StoreListController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/24.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： StoreLIstController
 Created_Date： 20160324
 Created_People： GT
 Function_description：便利店信息列表
 ***************************************/

#import "BaseViewController.h"

@interface StoreListController : BaseViewController

@end
