//
//  EmptyPageView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/1.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "EmptyPageView.h"
#import "Global.h"
#import "UIColor+Hex.h"

#import "UIImage+ColorToImage.h"
@interface EmptyPageView()
@property(nonatomic,strong)UIView *emptyView;
@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UILabel *label;
@property(nonatomic,strong)UIButton *button;

@end
@implementation EmptyPageView

-(id)initWithFrame:(CGRect)frame EmptyPageViewType:(EmptyPageViewType)emptyPageViewType
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setSubviewsWithFrame:frame];
        [self setSubViewDataWithEmptyPageViewType:emptyPageViewType];
        self.backgroundColor =[UIColor whiteColor];
    }
    return self;
}
//立即逛逛 跳转
-(void)strollCilck
{
    if (_delegate && [_delegate respondsToSelector:@selector(buttonClick)]) {
        [_delegate buttonClick];
    }
}

-(void)setSubviewsWithFrame:(CGRect)frame
{
//    _emptyView =[[UIView alloc]initWithFrame:frame];
//    [self addSubview:_emptyView];
//    _emptyView.backgroundColor=[UIColor whiteColor];
   
    _imageView=[[UIImageView alloc]initWithFrame:CGRectMake(VIEW_WIDTH/3, VIEW_HEIGHT/3-64-49, VIEW_WIDTH/3, VIEW_WIDTH/3)];
       [self addSubview:_imageView];
    
    _label =[[UILabel alloc]initWithFrame:CGRectMake((VIEW_WIDTH/3)/2, CGRectGetMaxY(_imageView.frame)+25, VIEW_WIDTH/3*2, 20)];
    
    _label.font=[UIFont systemFontOfSize:FontSize(16)];
    _label.textColor=[UIColor colorWithHexString:@"#555555"];
    _label.textAlignment=NSTextAlignmentCenter;
    [self addSubview:_label];
    
    
    

    
    _button=[UIButton buttonWithType:UIButtonTypeCustom];
    _button.frame=CGRectMake(VIEW_WIDTH/3, CGRectGetMaxY(_label.frame)+25, VIEW_WIDTH/3, 40);
    _button.titleLabel.font=[UIFont systemFontOfSize:FontSize(16)];
   
    UIImage *strollBackgroundImage = [UIImage createImageWithColor:[UIColor whiteColor]
                                                             frame:CGRectMake(0, 0, CGRectGetWidth(_button.bounds), CGRectGetHeight(_button.bounds))];
    [_button setBackgroundImage:strollBackgroundImage forState:UIControlStateNormal];
    [_button.layer setBorderWidth:0.5];
    [_button.layer setBorderColor:[UIColor colorWithHexString:@"#c52720"].CGColor];
    [_button setTitleColor:[UIColor colorWithHexString:@"#c52720"] forState:UIControlStateNormal];
    [_button.layer setMasksToBounds:YES];
    [_button.layer setCornerRadius:4.0];
    [_button addTarget:self action:@selector(strollCilck) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_button];
}
-(void)setSubViewDataWithEmptyPageViewType:(EmptyPageViewType)emptyPageViewType
{
    
    NSString *imageName =@"";
    NSString *labelText =@"";
    NSString *buttonTitle=@"";
    if (emptyPageViewType == kServiecListView) {
        imageName =@"s_empty";
        labelText =@"附近暂无该类商家";
        buttonTitle =@"逛逛其他";
    }else if (emptyPageViewType == kHistoyView){
        imageName =@"browsinghistory_icon_empty";
        labelText =@"暂无浏览记录,快去逛逛吧";
        buttonTitle =@"立即逛逛";
    }else if (emptyPageViewType == KMarkView){
        imageName =@"attentiongoods_icon_empty";
        labelText =@"暂无关注商品,快去逛逛吧";
        buttonTitle =@"立即逛逛";
    }else if (emptyPageViewType == kManageReceivedAddress){
        imageName =@"myshippingadress_btn_noaddress";
        labelText =@"暂无收货地址,快去添加吧";
        _button.hidden =YES;
        _imageView.frame = CGRectMake(VIEW_WIDTH/3, VIEW_HEIGHT/3-64-49-30, VIEW_WIDTH/3, VIEW_WIDTH/3+30);
    }else if (emptyPageViewType ==kHomeGoodListView ||emptyPageViewType == kSearchResultView){
        if (emptyPageViewType ==kHomeGoodListView) {
            imageName =@"search_icon_ineffective";
            labelText =@"暂无活动商品，换个活动试试";
            
        }else
        {
            imageName =@"search_icon_ineffective";
            labelText =@"没有搜索结果,换个关键词试试";
        }
        _button.hidden =YES;
    }else if (emptyPageViewType == kServiceRootView || emptyPageViewType ==kStoreRootView){
        imageName =@"icon_nonetwork";
        labelText =@"您的手机网络不太顺畅";
        buttonTitle =@"立即重试";
    }else if (emptyPageViewType == kGoodsOrderRootView){
        imageName =@"myorder_icon_empty";
        labelText =@"您还没有相关订单";
        buttonTitle =@"立即逛逛";

    }else if (emptyPageViewType == kServiceOrderRootView){
        imageName =@"myorder_icon_empty";
        labelText =@"您还没有相关订单";
        buttonTitle=@"立即预约";

    }else if (emptyPageViewType == kShoppingCarView){
        imageName =@"icon_empty";
        labelText =@"购物车空空如也,赶紧装满~";
        buttonTitle=@"立即逛逛";
    }else if (emptyPageViewType == kGoodsOrderRefundView){
        imageName =@"myorder_icon_empty";
        labelText =@"暂无相关订单";
       _button.hidden =YES;
    }else if (emptyPageViewType == kStoreNoGoodsView){
        imageName =@"productDetail_Default";
        labelText =@"很抱歉,商品正在上架中,敬请期待";
        _button.hidden =YES;
        _emptyView.backgroundColor =[UIColor clearColor];
        _imageView.frame = CGRectMake(VIEW_WIDTH/4, VIEW_HEIGHT/3-64-49-30, VIEW_WIDTH/2, VIEW_WIDTH/2);
    }else if (emptyPageViewType == kMyWalletNoDataView){
        imageName =@"img_mywallet_no";
        labelText =@"暂无收益";
        _button.hidden =YES;
    }else if (emptyPageViewType == kMyShiHuiBiView){
        imageName =@"img_mywallet_no";
        labelText =@"暂无消费记录";
        _button.hidden =YES;
    }else if (emptyPageViewType == kRecommendFriendView){
        imageName =@"img_mywallet_no";
        labelText =@"暂无推荐的好友";
        _button.hidden =YES;
    }
    [self setSubviewDataWithLableText:labelText ButtonTitle:buttonTitle ImageName:imageName];
}
-(void)setSubviewDataWithLableText:(NSString*)lableText ButtonTitle:(NSString *)buttonTitle ImageName:(NSString *)imageName
{
     _imageView.image=[UIImage imageNamed:imageName];
      _label.text=lableText;
     [_button setTitle:buttonTitle forState:UIControlStateNormal];
}
@end
