//
//  BaseViewController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "BaseViewController.h"
//utils
#import "Global.h"
#import "FL_Button.h"
#import "UIColor+Hex.h"
#import "AFNetworking.h"
#import "NSString+TextSize.h"
#import "ManagerGlobeUntil.h"
@interface BaseViewController ()
@property(nonatomic, strong)UIButton *titleItemButton;
@property(nonatomic, strong)UIButton *searchButton;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(applicationDidBecomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    if (![ManagerGlobeUntil sharedManager].isNetworkReachability) {
//        [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"似乎已断开与互联网的连接" inView:nil];
//    }
    
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - NavBarItem Method

- (void)addBackNavItem
{
    //添加navBarItem
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage  *btnBg =[UIImage imageNamed:@"Nav_top-back"];
    if (IsIOS7)
    {
        btn.frame = CGRectMake(0, 0, btnBg.size.width, btnBg.size.height);
    }
    else
    {
        btn.frame = CGRectMake(0, 0, 44, 44);
    }
    
    [btn setImage:btnBg forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = button;
}

- (void)addRightNavItemWith:(UIImage*)img
{
    //添加navBarItem
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage  *btnBg =img;
    if (IsIOS7)
    {
        btn.frame = CGRectMake(0, 0, btnBg.size.width, btnBg.size.height);
    }
    else
    {
        btn.frame = CGRectMake(0, 0, 44, 44);
    }
    
    [btn setImage:btnBg forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(rigthBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = button;
}

- (void)addRightNavItemWithTitle:(NSString*)title
{
        //添加navBarItem
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, 44, 44);
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"#aaaaaa"] forState:UIControlStateHighlighted];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [btn addTarget:self action:@selector(rigthBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithCustomView:btn];
        self.navigationItem.rightBarButtonItem = button;
    
}

- (void)addLeftNavItemWith:(UIImage*)img title:(NSString*)title
{
    //添加navBarItem
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (!img) {
        img = [UIImage imageNamed:@"Nav_arrow"];
    }
    UIImage  *btnBg =img;
    
    btn.frame = CGRectMake(0, 0, 80, 44);
    btn.backgroundColor = [UIColor clearColor];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:16] maxSize:CGSizeMake(80, 44)];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, titleSize.width + 4, 0, 0);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, -img.size.width, 0, 0);
    [btn setImage:btnBg forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:title forState:UIControlStateNormal];
    UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = button;
}

- (void)setNavgationTitleViewWithTitlt:(NSString*)title {
    self.title = title;
}
- (void)setNavgationTitleViewWithImageName:(NSString*)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    image = [image stretchableImageWithLeftCapWidth:8 topCapHeight:8];
    UIImageView *imageview = [[UIImageView alloc]initWithImage:image];
    self.navigationItem.titleView = imageview;
}
- (void)setHomeNavgationTitleViewWithButtonTitle:(NSString*)title{
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 44)];
    bottomView.backgroundColor = [UIColor colorWithHexString:@"#c52720"];
    
     CGSize size = [title sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14],NSFontAttributeName, nil]];
    _titleItemButton = [[FL_Button alloc]initWithAlignmentStatus:FLAlignmentStatusLeft];
    UIImage  *img = [UIImage imageNamed:@"Nav_arrow"];
    
    _titleItemButton.frame = CGRectMake(15, CGRectGetMinY(bottomView.frame), size.width+25 , 44);
    _titleItemButton.backgroundColor = [UIColor clearColor];
    _titleItemButton.titleLabel.font = [UIFont systemFontOfSize:14];
//    _titleItemButton.titleLabel.adjustsFontSizeToFitWidth = YES;
//    _titleItemButton.titleLabel.minimumScaleFactor = 0.7;
//    _titleItemButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _titleItemButton.titleLabel.textAlignment= NSTextAlignmentLeft;
    [_titleItemButton setImage:img forState:UIControlStateNormal];
    [_titleItemButton addTarget:self action:@selector(titleItemButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_titleItemButton setTitle:title forState:UIControlStateNormal];
    [bottomView addSubview:_titleItemButton];
    
    _searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _searchButton.frame = CGRectMake(CGRectGetMaxX(_titleItemButton.frame)+10,CGRectGetMinY(bottomView.frame)+8 , CGRectGetWidth(bottomView.bounds)-CGRectGetWidth(_titleItemButton.bounds)-15-15-10 , 28);
    [_searchButton setTitle:@"天天都实惠" forState:UIControlStateNormal];
    _searchButton.backgroundColor = [UIColor whiteColor];
    [_searchButton.layer setMasksToBounds:YES];
    [_searchButton.layer setCornerRadius:4];
    _searchButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_searchButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _searchButton.titleLabel.font = [UIFont systemFontOfSize:14];
    _searchButton.imageEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    _searchButton.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
    
    [_searchButton setImage:[UIImage imageNamed:@"search_logo"] forState:UIControlStateNormal];
    [_searchButton addTarget:self action:@selector(beatSearchEvent:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:_searchButton];

    self.navigationItem.titleView = bottomView;
}
- (void)setNavgationTitleViewWithButtonTitle:(NSString*)title buttonImageName:(NSString*)imageName {
    _titleItemButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (imageName.length == 0) {
        imageName = @"Nav_arrow";
    }
    UIImage  *img = [UIImage imageNamed:imageName];
    _titleItemButton.frame = CGRectMake(0, 0, 200, 44);
    _titleItemButton.backgroundColor = [UIColor clearColor];
    _titleItemButton.titleLabel.font = [UIFont systemFontOfSize:16];
     _titleItemButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:16] maxSize:CGSizeMake(180, 44)];
    CGFloat changeCenterX = _titleItemButton.bounds.size.width/2 - (titleSize.width + img.size.width)/2;
    _titleItemButton.imageEdgeInsets = UIEdgeInsetsMake(0, titleSize.width + changeCenterX + 4 , 0, 0);
    _titleItemButton.titleEdgeInsets = UIEdgeInsetsMake(0, -img.size.width + changeCenterX, 0, 0);
    [_titleItemButton setImage:img forState:UIControlStateNormal];
    [_titleItemButton addTarget:self action:@selector(titleItemButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_titleItemButton setTitle:title forState:UIControlStateNormal];
    self.navigationItem.titleView = _titleItemButton;
}
#pragma mark -- button Action
- (void)rigthBtnAction:(id)sender {
    
}
- (void)leftBtnAction:(id)sender {
    
    
}

- (void)backBtnAction:(id)sender
{
    
}

- (void)titleItemButtonAction:(id)sender {
    
}
- (void)beatSearchEvent:(id)sender {
    
}
- (void)setTitleItemTitle:(NSString *)titleItemTitle {
    self.navigationItem.titleView = nil;
    NSString *imageName = @"";
    if ([titleItemTitle isEqualToString:@"生鲜蔬果"] || [titleItemTitle isEqualToString:@"生活百货"] || [titleItemTitle isEqualToString:@"粮油调料"] || [titleItemTitle isEqualToString:@"酒水饮料"]) {
        imageName = @"Nav_arrow";
    } else {
        imageName = @"Nav_arrow";
    }
    [self setNavgationTitleViewWithButtonTitle:titleItemTitle buttonImageName:imageName];
    _titleItemTitle = titleItemTitle;
}
#pragma mark - NetworkingChange


#pragma mark - applicationBecomeActive
-(void)applicationDidBecomeActive
{
    
}


@end
