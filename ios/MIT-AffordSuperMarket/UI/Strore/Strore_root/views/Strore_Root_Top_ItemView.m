//
//  Strore_Root_Top_ItemView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/25.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Strore_Root_Top_ItemView.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
@interface Strore_Root_Top_ItemView()
{
    NSInteger  _seleteCount;
}
@end
@implementation Strore_Root_Top_ItemView
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //
        _seleteCount =0;
        self.backgroundColor =[UIColor whiteColor];
        [self initViewsWithFrame:frame];
    }
    return self;
}
-(void)initViewsWithFrame:(CGRect)frame{
    //筛选
    UIView *filterView = [[UIView alloc] initWithFrame:frame];
    filterView.backgroundColor = [UIColor whiteColor];
    [self addSubview:filterView];
    
    NSArray *filterName = @[@"全部",@"销量",[NSString stringWithFormat:@"%@     ",kDaShiHuiStoreName],@"价格   "];
    //筛选
    for (int i = 0; i <filterName.count; i++) {
        //文字
        UIButton *filterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        filterBtn.tag = 100+i;
        filterBtn.titleLabel.font = [UIFont systemFontOfSize:HightScalar(16)];
        [filterBtn setTitle:filterName[i] forState:UIControlStateNormal];
        [filterBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [filterBtn setTitleColor:[UIColor colorWithHexString:@"#db3b34"] forState:UIControlStateSelected];
        [filterBtn addTarget:self action:@selector(OnFilterBtn:) forControlEvents:UIControlEventTouchUpInside];
        [filterView addSubview:filterBtn];
        //分割线
        UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake((i+1)*VIEW_WIDTH/5, filterView.bounds.size.height/4, 1, filterView.bounds.size.height/2)];
        imageView.image =[UIImage createImageWithColor:[UIColor colorWithHexString:@"#cccccc"] frame:CGRectMake(0, 0, 0.7, filterView.bounds.size.height/2)];
        
        //三角
        UIButton *sanjiaoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [sanjiaoBtn addTarget:self action:@selector(sanjiaoBtn:) forControlEvents:UIControlEventTouchUpInside];
        sanjiaoBtn.tag = 120+i;
        if (i==0) {
            filterBtn.frame = CGRectMake(i*VIEW_WIDTH/5, 0, VIEW_WIDTH/5-4+20, filterView.bounds.size.height);
            imageView.frame = CGRectMake((i+1)*VIEW_WIDTH/5+24, filterView.bounds.size.height/4, 1, filterView.bounds.size.height/2);
            [filterView addSubview:imageView];
            sanjiaoBtn.frame = CGRectMake((i+1)*VIEW_WIDTH/filterName.count-12, (filterView.bounds.size.height - 10)/2, 8, 10);
            [sanjiaoBtn setImage:[UIImage imageNamed:@"cvs_btn_nav_down_black_nor"] forState:UIControlStateNormal];
            [sanjiaoBtn setImage:[UIImage imageNamed:@"cvs_btn_nav_top_red_sel"] forState:UIControlStateSelected];
            [filterView addSubview:sanjiaoBtn];
        }else if(i==1){
            filterBtn.frame = CGRectMake(i*VIEW_WIDTH/5+20+4, 0, VIEW_WIDTH/5-4-10, filterView.bounds.size.height);
            imageView.frame = CGRectMake((i+1)*VIEW_WIDTH/5+10, filterView.bounds.size.height/4, 1, filterView.bounds.size.height/2);
            [filterView addSubview:imageView];
        }else if(i==2){
            filterBtn.frame = CGRectMake(i*VIEW_WIDTH/5+10, 0, VIEW_WIDTH/5*2-14, filterView.bounds.size.height);
            imageView.frame = CGRectMake((i+2)*VIEW_WIDTH/5, filterView.bounds.size.height/4, 1, filterView.bounds.size.height/2);
            [filterView addSubview:imageView];
            sanjiaoBtn.frame = CGRectMake((i+1)*VIEW_WIDTH/filterName.count-15, (filterView.bounds.size.height - 20)/2, 20, 20);
            [sanjiaoBtn setImage:[UIImage imageNamed:@"fxk"] forState:UIControlStateNormal];
            [sanjiaoBtn setImage:[UIImage imageNamed:@"fxk_choose"] forState:UIControlStateSelected];
            [filterView addSubview:sanjiaoBtn];
        }else if(i==3){
            filterBtn.frame = CGRectMake((i+1)*VIEW_WIDTH/5, 0, VIEW_WIDTH/5, filterView.bounds.size.height);
            sanjiaoBtn.frame = CGRectMake((i+1)*VIEW_WIDTH/filterName.count-20, (filterView.bounds.size.height - 10)/2, 8, 10);
            [sanjiaoBtn setImage:[UIImage imageNamed:@"cvs_btn_nav_down_black_nor"] forState:UIControlStateNormal];
            [filterView addSubview:sanjiaoBtn];
            
        }
        
    }
    //下划线
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, filterView.bounds.size.height-1, VIEW_WIDTH, 0.7)];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#dddddd"];
    [filterView addSubview:lineView];

}
-(void)OnFilterBtn:(UIButton *)sender{
    for (int i = 1; i < 4; i++) {
        if (i ==0 ||i ==1||i==3) {
            UIButton *btn = (UIButton *)[self viewWithTag:100+i];
            UIButton *sanjiaoBtn = (UIButton *)[self viewWithTag:120+i];
            btn.selected = NO;
            sanjiaoBtn.selected = NO;
        }    }
    if (sender.tag==kStroreGoodSGroup) {
        UIButton *quanbu=(UIButton *)sender;
        quanbu.selected = !sender.selected;
        UIButton *sanjiaoBtn = (UIButton *)[self viewWithTag:120];
        if (quanbu.selected ==YES) {
            sanjiaoBtn.selected = YES;
        }else{
            sanjiaoBtn.selected = NO;
            [quanbu setTitleColor:[UIColor colorWithHexString:@"#db3b34"] forState:UIControlStateNormal];
        }
        if (_delegate && [_delegate respondsToSelector:@selector(kStroreGoodSGroupClickWithSelectedState:)]) {
            [_delegate kStroreGoodSGroupClickWithSelectedState:quanbu.selected];
        }

    }else if(sender.tag == kStroreSalesVolume){
        sender.selected = YES;
        UIButton *btn = (UIButton *)[self viewWithTag:100];
        UIButton *sanjiaoBtn = (UIButton *)[self viewWithTag:120];
        btn.selected = NO;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        sanjiaoBtn.selected = NO;
        if (_delegate && [_delegate respondsToSelector:@selector(kStroreSalesVolumeClick)]) {
            [_delegate kStroreSalesVolumeClick];
        }
    }else if(sender.tag == kStroreProprietaryBtn){
        UIButton *ziYing=(UIButton *)sender;
        ziYing.selected = !sender.selected;
        [ziYing setTitleColor:[UIColor colorWithHexString:@"#db3b34"] forState:UIControlStateSelected];
        UIButton *sanjiaoBtn = (UIButton *)[self viewWithTag:122];
        if (ziYing.selected ==YES) {
            sanjiaoBtn.selected = YES;
        }else if(ziYing.selected ==NO){
            sanjiaoBtn.selected = NO;
        }
        UIButton *btn = (UIButton *)[self viewWithTag:100];
        UIButton *sanjiaoBtnOne = (UIButton *)[self viewWithTag:120];
        btn.selected = NO;
        sanjiaoBtnOne.selected = NO;
        if (_delegate && [_delegate respondsToSelector:@selector(kStroreProprietaryBtnWithSelectedState:)]) {
            [_delegate kStroreProprietaryBtnWithSelectedState:ziYing.selected];
        }
    }else if(sender.tag == kStrorePriceBtn){
        sender.selected = YES;
        UIButton *sjBtn = (UIButton *)[self viewWithTag:sender.tag+20];
        _seleteCount ++;
        NSInteger a =_seleteCount%2;
         NSString *state=@"";
        if (a == 0) {
            [sjBtn setImage:[UIImage imageNamed:@"cvs_btn_nav_top_red_sel"] forState:UIControlStateSelected];
            state =@"1";
        }else{
            [sjBtn setImage:[UIImage imageNamed:@"cvs_btn_nav_down_red_sel"] forState:UIControlStateSelected];
             state =@"2";
        }
        if (_delegate && [_delegate respondsToSelector:@selector(kStrorePriceBtnClickWithSort:)]) {
            [_delegate kStrorePriceBtnClickWithSort:state];
        }
        sjBtn.selected = YES;
        UIButton *btn = (UIButton *)[self viewWithTag:100];
        UIButton *sanjiaoBtn = (UIButton *)[self viewWithTag:120];
        btn.selected = NO;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        sanjiaoBtn.selected = NO;
}
    
    
}
-(void)sanjiaoBtn:(UIButton *)sender{
    if (sender.tag == 122) {
        UIButton *sanjiaoBtn=(UIButton *)sender;
        sanjiaoBtn.selected = !sender.selected;
        UIButton *ziYing = (UIButton *)[self viewWithTag:102];
        if (sanjiaoBtn.selected ==YES) {
            ziYing.selected = YES;
        }else if(sanjiaoBtn.selected ==NO){
            ziYing.selected = NO;
        }
        if (_delegate && [_delegate respondsToSelector:@selector(kStroreProprietaryBtnWithSelectedState:)]) {
            [_delegate kStroreProprietaryBtnWithSelectedState:ziYing.selected];
        }
    }
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
