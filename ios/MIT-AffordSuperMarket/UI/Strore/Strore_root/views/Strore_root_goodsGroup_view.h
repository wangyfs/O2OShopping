//
//  Strore_root_goodsGroup_view.h
//  buttom
//
//  Created by apple on 16/1/18.
//  Copyright © 2016年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Strore_root_goodsGroupDelegate <NSObject>

@optional
/**
 *  点击tableview，过滤id
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withFirstCode:(NSString *)firstCode withSecondCode:(NSString *)SecondCode Name:(NSString *)name;

@end
@interface Strore_root_goodsGroup_view : UIView

@property(nonatomic, strong) UITableView *tableViewOfGroup;
@property(nonatomic, strong) UITableView *tableViewOfDetail;
@property(nonatomic, strong)NSMutableArray *goodsGroupArr;//商品分类数据
@property (nonatomic,strong)NSMutableArray *rightGroupArray;//右边分组tableview的数据源
@property (nonatomic,strong)NSMutableArray *leftGroupArray;//左边分组tableview的数据源

@property (nonatomic,assign)NSInteger leftSelectedIndex;
@property (nonatomic,assign)NSInteger rightSelectedIndex;
@property (nonatomic,assign)NSInteger rememberLeftSelectedIndex;
@property(nonatomic,strong)NSString *firstCode;
@property(nonatomic, assign) id<Strore_root_goodsGroupDelegate> delegate;

@end
