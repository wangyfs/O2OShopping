//
//  Strore_root_goodsGroup_view.m
//  buttom
//
//  Created by apple on 16/1/18.
//  Copyright © 2016年 apple. All rights reserved.
//
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r, g, b)     RGBA(r, g, b, 1.0f)
#define screen_width [UIScreen mainScreen].bounds.size.width
#import "Strore_root_goodsGroup_view.h"
#import "ManagerHttpBase.h"
#import "ManagerGlobeUntil.h"
#import "Global.h"
//View
#import "NSString+Conversion.h"
#import "UIColor+Hex.h"
//model
#import "Strore_root_goodsGroup_model.h"
@interface Strore_root_goodsGroup_view ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)NSMutableDictionary *quanBu;
@end
@implementation Strore_root_goodsGroup_view

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _leftGroupArray  = [[NSMutableArray alloc] init];
        _rightGroupArray = [[NSMutableArray alloc] init];
        _goodsGroupArr   = [[NSMutableArray alloc] init];
        _quanBu =[[NSMutableDictionary alloc]init];
        //初始化控件
        [self initViews];
        
        
    }
    return self;
}
//更新rightTableView数据
-(void)setRightTableViewDataSourceWithArray{
    [_rightGroupArray removeAllObjects];
    NSDictionary *dic = [_goodsGroupArr objectAtIndex:_leftSelectedIndex];
   
    for (NSInteger i=0; i<[[dic objectForKey:@"CHILDREN"] count]+1; i++) {
    Strore_root_goodsGroup_model *model =[[Strore_root_goodsGroup_model alloc]init];
    
        if (i == 0) {
            model.groupsName =@"全部";
            model.groupsCount =[NSString stringTransformObject:[dic objectForKey:@"GOODSCOUNT"]];
            model.groupsCode =@"";
        }else{
        NSDictionary *dicdata =[[dic objectForKey:@"CHILDREN"] objectAtIndex:i-1];
        model.groupsName =[NSString stringTransformObject:[dicdata objectForKey:@"NAME"]];
        model.groupsCount =[NSString stringTransformObject:[dicdata objectForKey:@"GOODSCOUNT"]];
        model.groupsCode =[NSString stringTransformObject:[dicdata objectForKey:@"CODE"]];
        }
         [_rightGroupArray addObject:model];
    }
    

}
#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == 10) {
        return _goodsGroupArr.count;
    }else{
        return _rightGroupArray.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HightScalar(50);
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == 10) {
        NSDictionary *dic =[_goodsGroupArr objectAtIndex:indexPath.row];
        static NSString *cellIndentifier = @"filterCell1";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIndentifier ];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.detailTextLabel.text = [NSString stringTransformObject:[dic objectForKey:@"GOODSCOUNT"]];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:FontSize(13)];
        cell.detailTextLabel.textColor=[UIColor colorWithHexString:@"#999999"];
        cell.textLabel.text =[NSString stringTransformObject: [dic objectForKey:@"NAME"]];
        cell.backgroundColor= [UIColor colorWithHexString:@"#eeeeee"];
        cell.textLabel.font =[UIFont systemFontOfSize:FontSize(15)];
        cell.textLabel.highlightedTextColor = [UIColor colorWithHexString:@"#000000"];
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor whiteColor];
        if ([cell.textLabel.text isEqualToString:@"生鲜蔬果"]) {
            cell.imageView.image = [UIImage imageNamed:@"store_img_classify_one"];
        }else if([cell.textLabel.text isEqualToString:@"酒水饮料"])
        {
            cell.imageView.image = [UIImage imageNamed:@"store_img_classify_four"];
        }else if([cell.textLabel.text isEqualToString:@"粮油调料"])
        {
            cell.imageView.image = [UIImage imageNamed:@"store_img_classify_three"];
        }else
        {
            cell.imageView.image = [UIImage imageNamed:@"store_img_classify_two"];
        }
        return cell;
    }else{
         Strore_root_goodsGroup_model *model =[_rightGroupArray objectAtIndex:indexPath.row];
       static  NSString *cellIndentifier = @"filterCell2";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIndentifier];
//            //下划线
            UIView *lineView = [[UILabel alloc] initWithFrame:CGRectMake(15, HightScalar(50)-1, cell.frame.size.width, 0.7)];
            lineView.backgroundColor = [UIColor colorWithHexString:@"#999999"];
            [cell.contentView addSubview:lineView];
        }
        cell.textLabel.text = model.groupsName;
        cell.textLabel.font = [UIFont systemFontOfSize:FontSize(15)];
        cell.detailTextLabel.text = model.groupsCount;
        cell.detailTextLabel.font = [UIFont systemFontOfSize:FontSize(14)];
        if (model.isChecked) {
            cell.textLabel.textColor = [UIColor colorWithHexString:@"#db3b34"];
            cell.detailTextLabel.textColor=[UIColor colorWithHexString:@"#db3b34"];
        } else {
            cell.textLabel.textColor = [UIColor colorWithHexString:@"#000000"];
            cell.detailTextLabel.textColor=[UIColor colorWithHexString:@"#999999"];
        }
        return cell;
    }
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
    if (tableView.tag == 10) {
        _leftSelectedIndex = indexPath.row;
        [self setRightTableViewDataSourceWithArray];
        if (_rememberLeftSelectedIndex == indexPath.row) {
            [self rightTableViewSelectedCell];
        }
        NSDictionary *dic =[_goodsGroupArr objectAtIndex:_leftSelectedIndex];
       _firstCode =[NSString stringTransformObject:[dic objectForKey:@"CODE"]];
      [self.tableViewOfDetail reloadData];
    }else{

        [self rightTableViewSelectedCellWithIndexPath:indexPath];
         _rightSelectedIndex = indexPath.row;
       
    }
}
//恢复右边tableView cell的选中 状态
-(void)rightTableViewSelectedCell
{
  
        Strore_root_goodsGroup_model *oldModel =[_rightGroupArray objectAtIndex:_rightSelectedIndex];
        for (Strore_root_goodsGroup_model *model in _rightGroupArray) {
            if ([model isEqual:oldModel]) {
                model.isChecked = YES;
            } else {
                model.isChecked = NO;
            }
        }
    
   
}
//右边tableView的 选中状态 
-(void)rightTableViewSelectedCellWithIndexPath:(NSIndexPath *)indexPath
{
    
    Strore_root_goodsGroup_model *nowModel =[_rightGroupArray objectAtIndex:indexPath.row];
    for (Strore_root_goodsGroup_model *model in _rightGroupArray) {
        if ([model isEqual:nowModel]) {
            model.isChecked = YES;
        } else {
            model.isChecked = NO;
        }
    }
    NSString *SecondCode=nowModel.groupsCode;
    _rememberLeftSelectedIndex =_leftSelectedIndex;
    NSString *name =nowModel.groupsName;
    [self.tableViewOfDetail reloadData];
    [self.delegate tableView:self.tableViewOfDetail didSelectRowAtIndexPath:indexPath withFirstCode:_firstCode withSecondCode:SecondCode Name:name];
    
}
#pragma mark -- 初始化控件
-(void)initViews{
    //分组
    self.tableViewOfGroup = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width/2, self.frame.size.height) style:UITableViewStylePlain];
    self.tableViewOfGroup.tag = 10;
    self.tableViewOfGroup.delegate = self;
    self.tableViewOfGroup.dataSource = self;
    self.tableViewOfGroup.backgroundColor = [UIColor colorWithHexString:@"#eeeeee"];
    self.tableViewOfGroup.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:self.tableViewOfGroup];
    
    //详情
    self.tableViewOfDetail = [[UITableView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 0, self.frame.size.width/2, CGRectGetHeight(_tableViewOfGroup.bounds)) style:UITableViewStylePlain];
    self.tableViewOfDetail.tag = 20;
    self.tableViewOfDetail.dataSource = self;
    self.tableViewOfDetail.delegate = self;
    self.tableViewOfDetail.backgroundColor = [UIColor whiteColor];
    self.tableViewOfDetail.separatorStyle =  UITableViewCellSeparatorStyleNone;
    [self addSubview:self.tableViewOfDetail];
    self.userInteractionEnabled = YES;

    

}

@end
