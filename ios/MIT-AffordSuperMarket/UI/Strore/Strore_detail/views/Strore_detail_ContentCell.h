//
//  Strore_detail_ContentCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Strore_detail_ContentCell
 Created_Date： 20160415
 Created_People： GT
 Function_description： 商品详情内容cell
 ***************************************/

#import <UIKit/UIKit.h>


@interface Strore_detail_ContentCell : UITableViewCell


-(void)updateViewWithData:(NSDictionary *)dataDic;
- (void)updateMarkCount:(NSString*)markCount;
- (CGFloat)cellFactHight;
@end
