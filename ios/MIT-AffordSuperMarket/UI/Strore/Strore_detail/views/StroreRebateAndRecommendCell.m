//
//  StroreRebateAndRecommendCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "StroreRebateAndRecommendCell.h"
//view
#import "KZLinkLabel.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#define kleftSpace 15
#define ktopSpace 10
#define kbottomSpace 10
#define kvSpace 10 //垂直间隙
#define khSpace 10
#define ksepLineHight 0.5
@interface StroreRebateAndRecommendCell() {
    BOOL _bothRebateAndRecommend;
}
@property(nonatomic, strong)UIImageView *rebateLogoView;
@property(nonatomic, strong)UILabel *rebateLabel;
@property(nonatomic, strong)UIImageView *recommendLogoView;
@property(nonatomic, strong)UILabel *recommendLabel;
@property(nonatomic, strong)UIImageView *sepLineImageView;
@end
@implementation StroreRebateAndRecommendCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSubview];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setUpSubview {
    _rebateLogoView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"[fan]"]];
    _rebateLogoView.frame = CGRectMake(kleftSpace
                                       , ktopSpace
                                       , HightScalar(17)
                                       , HightScalar(17));
    [self.contentView addSubview:_rebateLogoView];
    
    _rebateLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_rebateLogoView.frame) + khSpace
                                                               , CGRectGetMinY(_rebateLogoView.frame)
                                                               ,VIEW_WIDTH - 2*kleftSpace - CGRectGetWidth(_rebateLogoView.bounds) - khSpace
                                                               , HightScalar(17))];
    _rebateLabel.font = [UIFont systemFontOfSize:FontSize(13)];
    _rebateLabel.textAlignment = NSTextAlignmentLeft;
    _rebateLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    [self.contentView addSubview:_rebateLabel];
    
     _recommendLogoView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"[jian]"]];
    _recommendLogoView.frame = CGRectMake(CGRectGetMinX(_rebateLogoView.frame)
                                          , CGRectGetMaxY(_rebateLogoView.frame) + kvSpace
                                          , CGRectGetWidth(_rebateLogoView.bounds)
                                          , CGRectGetHeight(_rebateLogoView.bounds));
    [self.contentView addSubview:_recommendLogoView];
    
    _recommendLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_rebateLabel.frame)
                                                                , CGRectGetMinY(_recommendLogoView.frame)
                                                                ,CGRectGetWidth(_rebateLabel.bounds)
                                                                ,CGRectGetHeight(_rebateLabel.bounds))];
    _recommendLabel.font = [UIFont systemFontOfSize:FontSize(13)];
    _recommendLabel.textAlignment = NSTextAlignmentLeft;
    _recommendLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    [self.contentView addSubview:_recommendLabel];
    UIImage *sepLineImage = [UIImage imageNamed:@"gray_line"];
    _sepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
    _sepLineImageView.frame = CGRectMake(0
                                         , CGRectGetMaxY(_recommendLabel.frame) + kvSpace - ksepLineHight
                                         , VIEW_WIDTH
                                         , ksepLineHight);
    [self.contentView addSubview:_sepLineImageView];
}
-(void)updateViewWithData:(NSDictionary *)dataDic {
    if ([dataDic count] == 2) {//包含返现和推荐
        _bothRebateAndRecommend = YES;
        self.rebateLabel.text =[dataDic objectForKey:@"rebate"];
        
        self.recommendLabel.text =[dataDic objectForKey:@"recommend"];
    }else if ([dataDic count] == 1) {//只包含推荐
        self.rebateLogoView.image =  [UIImage imageNamed:@"[jian]"];
        self.rebateLabel.text = [dataDic objectForKey:@"recommend"] ;
        CGRect rebateLogoViewFrame = self.rebateLogoView.frame;
        rebateLogoViewFrame.origin.y = HightScalar(15);
        self.rebateLogoView.frame = rebateLogoViewFrame;
        CGRect rebateLabelFrame = self.rebateLabel.frame;
        rebateLabelFrame.origin.y = CGRectGetMinY(self.rebateLogoView.frame);
        self.rebateLabel.frame = rebateLabelFrame;
        CGRect seplineImageFrame = _sepLineImageView.frame;
        seplineImageFrame.origin.y = CGRectGetMaxY(self.rebateLabel.frame) + HightScalar(15) - ksepLineHight;
        self.sepLineImageView.frame = seplineImageFrame;
        self.recommendLogoView.hidden = YES;
        self.recommendLabel.hidden = YES;
    }
}
- (CGFloat)cellFactHight {
    return _bothRebateAndRecommend ? CGRectGetMaxY(_recommendLabel.frame)  + kbottomSpace : CGRectGetMaxY(_rebateLabel.frame) + HightScalar(15);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
