//
//  Strore_detailView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
#define Font(F) [UIFont systemFontOfSize:(F)]
#define kWindowWidth                        ([[UIScreen mainScreen] bounds].size.width)
#define kWindowHeight                       ([[UIScreen mainScreen] bounds].size.height)
#import "Strore_detailView.h"
#import "UIImage+ColorToImage.h"
#import "UIColor+Hex.h"
#import "FMDBManager.h"
#import "FL_Button.h"
#import "ManagerGlobeUntil.h"
@interface Strore_detailView ()

//购物车
@property(retain,nonatomic)UIButton *shopCarBtn;
//加入购物车按钮
@property(retain,nonatomic)UIButton *inToShopCar;
//数量标签
@property(retain,nonatomic)UILabel *totalSingular;
//数量
@property(assign,nonatomic) NSInteger totalSingularInt;
//动画路径
@property (nonatomic,strong) UIBezierPath *path;

@end
@implementation Strore_detailView

{
    CALayer     *layer;
    DetailBottomType _bottomType;
}
#pragma mark --初始化控件
-(id)initWithFrame:(CGRect)frame bottomType:(DetailBottomType)bottomType
{
    self=[super initWithFrame:frame];
    if (self) {
        _bottomType=bottomType;
        [self setUpSubViewWithBottomType:bottomType];
    }
    return self;
}
-(void)setUpSubViewWithBottomType:(DetailBottomType)bottomType
{
    NSString *firstButtonTitle=@"";
    NSString *secondButtonTitle=@"";
    NSString *buttonImageName=@"";
    if (bottomType ==kgoodDetailBottomType) {
        firstButtonTitle=@"购物车";
        secondButtonTitle=@"加入购物车";
        buttonImageName=@"shopcart";
    }else if(bottomType ==kserviceDetailBottomType)
    {
        firstButtonTitle=@"预约清单";
        secondButtonTitle=@"加入预约";
        buttonImageName=@"servicedetails_btn_serviceorder";
    }
    self.backgroundColor=[UIColor blackColor];
    //关注按钮
    _collectionBtn=[[FL_Button alloc]initWithAlignmentStatus:FLAlignmentStatusTop];
    [_collectionBtn setTag:100];
    _collectionBtn.frame =CGRectMake(0, 0, kWindowWidth/3 , 49);
    
    UIImage *imageNormal=[UIImage imageNamed:@"follow_normal"];
    UIImage *imageSelected=[UIImage imageNamed:@"follow_selected"];
    
    [_collectionBtn addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
    [_collectionBtn setImage:imageNormal forState:UIControlStateNormal];
    [_collectionBtn setImage:imageSelected forState:UIControlStateSelected];
    [_collectionBtn setTitle:@"关注" forState:UIControlStateNormal];
    // [_collectionBtn setTitle:@"已关注" forState:UIControlStateSelected];
    _collectionBtn.titleLabel.font=[UIFont systemFontOfSize:12.0f];
    [self addSubview:_collectionBtn];
    
    
    //购物车按钮
    _shopCarBtn=[[FL_Button alloc]initWithAlignmentStatus:FLAlignmentStatusTop];;
    _shopCarBtn.frame=CGRectMake(CGRectGetMaxX(self.collectionBtn.frame),CGRectGetMinY(self.collectionBtn.frame), CGRectGetWidth(self.collectionBtn.bounds), CGRectGetHeight(self.collectionBtn.bounds));
    [_shopCarBtn addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
    [_shopCarBtn setTag:101];
    [_shopCarBtn setImage:[UIImage imageNamed:buttonImageName] forState:UIControlStateNormal];
    [self addSubview:_shopCarBtn];
    [_shopCarBtn setTitle:firstButtonTitle forState:UIControlStateNormal];
    
    _shopCarBtn.titleLabel.font=[UIFont systemFontOfSize:12.0f];
    _inToShopCar=[[UIButton alloc]init];
    
    _inToShopCar.frame=CGRectMake(kWindowWidth/3*2, 0, kWindowWidth/3, CGRectGetHeight(self.bounds));
    UIImage *jieSuanBackgroundImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#ff3333"]
                                                              frame:CGRectMake(0, 0, CGRectGetWidth(_inToShopCar.bounds), CGRectGetHeight(_inToShopCar.bounds))];
    [_inToShopCar setBackgroundImage:jieSuanBackgroundImage forState:UIControlStateNormal];
    _inToShopCar.titleLabel.font=[UIFont systemFontOfSize:16.0f];
    [_inToShopCar setTitle:secondButtonTitle forState:UIControlStateNormal];
    [_inToShopCar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_inToShopCar addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
    [_inToShopCar setTag:102];
    [self addSubview:_inToShopCar];
    
    //圆标
    _totalSingular=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.shopCarBtn.frame)-CGRectGetWidth(self.shopCarBtn.bounds)/2+10, CGRectGetMinY(self.shopCarBtn.frame), 20, 20)];
    
    _totalSingular.text=@"0";
    _totalSingular.hidden=YES;
    _totalSingular.layer.masksToBounds=YES;
    _totalSingular.layer.cornerRadius=11;
    _totalSingular.textAlignment=NSTextAlignmentCenter;
    _totalSingular.backgroundColor=[UIColor redColor];
    _totalSingular.textColor=[UIColor whiteColor];
    _totalSingular.font=Font(12);
    _totalSingular.adjustsFontSizeToFitWidth=YES;
    _totalSingular.minimumScaleFactor=0.6;
    [self addSubview:_totalSingular];
    
    
    //弧线路径
    self.path = [UIBezierPath bezierPath];
    [_path moveToPoint:CGPointMake(CGRectGetMaxX(self.inToShopCar.frame), 0)];
    
    [_path addQuadCurveToPoint:CGPointMake(CGRectGetMidX(self.shopCarBtn.frame), 5) controlPoint:CGPointMake(250, -150)];

}
-(void)setUpyuanbaio
{
    XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSMutableArray *array=[[FMDBManager sharedManager]QueryDataWithshopID:mode.storeID];
    NSInteger totalNum = 0;
    for (NSDictionary *dic in array) {
        NSString *buynum =[dic objectForKey:@"buynum"];
        
        totalNum=totalNum+buynum.integerValue;
    }
    //圆标数量
    _totalSingularInt=totalNum;
    if (_totalSingularInt>0) {
        _totalSingular.hidden=NO;
    }else
    {
        _totalSingular.hidden=YES;
    }
    _totalSingular.text = [NSString stringWithFormat:@"%ld",(long)_totalSingularInt];
    
}
#pragma mark-- 按钮操作
-(void)btnClike:(id)sender
{
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
       
        UIButton *btn =(UIButton*)sender;
        if (_delegate && [_delegate respondsToSelector:@selector(selectedItemWithTag:)]) {
            [_delegate selectedItemWithTag:btn.tag - 100];
        }
        if (btn.tag==102) {//点击加入购物车时
            if (_totalSingularInt == 0) {
                _totalSingular.hidden = YES;
            }
            
            if (!layer) {
                _inToShopCar.enabled = NO;
                layer = [CALayer layer];
                layer.contents = (__bridge id)_goodImage.CGImage;
                layer.contentsGravity = kCAGravityResizeAspectFill;
                layer.bounds = CGRectMake(0, 0, 16, 16);
                [layer setCornerRadius:CGRectGetHeight([layer bounds]) / 2];
                layer.masksToBounds = YES;
                layer.position =CGPointMake(50, 150);
                [self.layer addSublayer:layer];
                
            }
            
            [self groupAnimation];
            
        }

}
    
    
}
#pragma mark --动画
-(void)groupAnimation
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.path = _path.CGPath;
    animation.rotationMode = kCAAnimationRotateAuto;
    CABasicAnimation *expandAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    expandAnimation.duration = 0.3f;
    
    //关键帧动画 从小到大
    expandAnimation.fromValue = [NSNumber numberWithFloat:1];
    expandAnimation.toValue = [NSNumber numberWithFloat:2.0f];
    expandAnimation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    CABasicAnimation *narrowAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    narrowAnimation.beginTime = 0.3;
    //从大到小
    narrowAnimation.fromValue = [NSNumber numberWithFloat:2.0f];
    narrowAnimation.duration = 0.7f;
    narrowAnimation.toValue = [NSNumber numberWithFloat:0.5f];
    
    narrowAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CAAnimationGroup *groups = [CAAnimationGroup animation];
    groups.animations = @[animation,expandAnimation,narrowAnimation];
    groups.duration = 1.0f;
    groups.removedOnCompletion=NO;
    groups.fillMode=kCAFillModeForwards;
    groups.delegate = self;
    [layer addAnimation:groups forKey:@"group"];
}
#pragma mark-- 圆标数字和购物车动画
-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    //    [anim def];
    if (anim == [layer animationForKey:@"group"]) {
        _inToShopCar.enabled = YES;
        [layer removeFromSuperlayer];
        layer = nil;
        _totalSingularInt++;
        if (_totalSingularInt) {
            _totalSingular.hidden = NO;
        }
        
        _totalSingular.text = [NSString stringWithFormat:@"%ld",(long)_totalSingularInt];
        
        //购物车按钮动画
        CABasicAnimation *shakeAnimation = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
        shakeAnimation.duration = 0.3f;
        shakeAnimation.fromValue = [NSNumber numberWithFloat:-1];
        shakeAnimation.toValue = [NSNumber numberWithFloat:1];
        shakeAnimation.autoreverses = YES;
        [_shopCarBtn.layer addAnimation:shakeAnimation forKey:nil];
    }
}


@end
