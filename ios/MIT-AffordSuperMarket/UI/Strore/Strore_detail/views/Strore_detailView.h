//
//  Strore_detailView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DetailBottomType) {
    kgoodDetailBottomType = 0,//商品详情底部栏
    kserviceDetailBottomType //服务详情底部栏
};

@protocol Strore_detailViewDelegate<NSObject>
-(void)selectedItemWithTag:(NSInteger)tag;
@end
@interface Strore_detailView : UIView
@property(retain,nonatomic)UIButton *collectionBtn;//收藏按钮

//数量标签
@property(copy,nonatomic)NSString*shopID;
@property(copy,nonatomic)NSString *goodID;
@property(nonatomic,strong)UIImage *goodImage;
-(void)setUpyuanbaio;
@property(nonatomic,unsafe_unretained)id<Strore_detailViewDelegate>delegate;
-(id)initWithFrame:(CGRect)frame bottomType:(DetailBottomType)bottomType;
@end
