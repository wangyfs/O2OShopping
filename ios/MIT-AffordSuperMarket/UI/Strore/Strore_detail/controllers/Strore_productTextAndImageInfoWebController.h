//
//  Strore_productTextAndImageInfoWebController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Strore_TeHui_CStrore_productTextAndImageInfoWebControllerontroller
 Created_Date： 20151106
 Created_People： GT
 Function_description： 商品图文详情
 ***************************************/

#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, ImageAndTextDetailType) {
    kproductImageAndTextDetail = 0,//商品图文详情
    kserviceImageAndTextDetail //服务商家图文详情
};
@interface Strore_productTextAndImageInfoWebController : BaseViewController
@property(nonatomic)ImageAndTextDetailType imageAndTextDetailType;
@property(nonatomic, strong)NSString *navgationTitle;
@property(nonatomic, strong)NSString *goodsId;//商品ID
@end
