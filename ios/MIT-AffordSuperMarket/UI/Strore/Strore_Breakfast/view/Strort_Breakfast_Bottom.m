//
//  Strort_Breakfast_Bottom.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/25.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Strort_Breakfast_Bottom.h"
#import "Strort_Breakfast_RightCell.h"
#import "UIColor+Hex.h"
@interface Strort_Breakfast_Bottom()



@property (nonatomic ,strong) NSMutableDictionary *dic;

@property (nonatomic ,strong) NSMutableArray *key;
@end

@implementation Strort_Breakfast_Bottom

-(id)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if (self) {
        [self setSuberView];
    }
    
    return self;
}
//跳转结算页面
-(void)shopBtnClick:(UIButton*)btn
{
    if (_delegate && [_delegate respondsToSelector:@selector(gotoShoppingCar)]) {
        [_delegate gotoShoppingCar];
    }
   
    
}


#pragma mark--初始化视图
-(void)setSuberView
{
    _shopCarBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    
    UIImage *image =[UIImage imageNamed:@"breakfast_bottom_left"];
    [self.shopCarBtn setImage:image forState:UIControlStateNormal];
    //        [self.shopCarBtn setImageEdgeInsets:UIEdgeInsetsMake((self.shopCarBtn.frame.size.height-image.size.height)/2, (self.shopCarBtn.frame.size.width-image.size.width)/2, (self.shopCarBtn.frame.size.height-image.size.height)/2, (self.shopCarBtn.frame.size.width-image.size.width)/2)];
    [self.shopCarBtn addTarget:self action:@selector(shopBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_shopCarBtn];
    
    
    
    _imageView =[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.shopCarBtn.frame)-6, CGRectGetMinY(self.shopCarBtn.frame)+5, CGRectGetWidth(self.bounds)/2+30, CGRectGetHeight(self.shopCarBtn.bounds)-10)];
    
    _imageView.image=[UIImage imageNamed:@"breakfast_bottom_right"];
    
    [self addSubview:_imageView];
    
    
    _totalSingular =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.shopCarBtn.frame)-10, CGRectGetMinY(self.shopCarBtn.frame), 20, 20)];
    _totalSingular.text=@"0";
    _totalSingular.hidden=YES;
    _totalSingular.layer.masksToBounds=YES;
    _totalSingular.layer.cornerRadius=10;
    _totalSingular.textAlignment=NSTextAlignmentCenter;
    _totalSingular.textColor=[UIColor whiteColor];
    _totalSingular.font=[UIFont systemFontOfSize:13.0f];
    _totalSingular.backgroundColor=[UIColor colorWithHexString:@"#da251d"];
    [self addSubview:_totalSingular];
    
    _totalPrice =[[UILabel alloc]initWithFrame:CGRectMake(13, CGRectGetHeight(self.imageView.bounds)/4, CGRectGetWidth(self.imageView.bounds)-6, CGRectGetHeight(self.imageView.bounds)/2)];
    [_totalPrice setText:@"总金额￥0.0"];
    _totalPrice.font =[UIFont systemFontOfSize:11.0f];
    [_totalPrice setTextColor:[UIColor whiteColor]];
    
    [_imageView addSubview:_totalPrice];
    
}


@end
