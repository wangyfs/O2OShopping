//
//  ManagerGlobeUntil.h
//  MIT_itrade
//
//  Created by wanc on 14-4-24.
//  Copyright (c) 2014年 ___ZhengDaXinXi___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "XiaoQuAndStoreInfoModel.h"
#import "UserInfoModel.h"
@interface ManagerGlobeUntil : NSObject
{
    MBProgressHUD *HUD;
}

//单人登陆
@property (nonatomic) BOOL singleLogin;
//标记超时
@property (nonatomic) BOOL isOutOfTimeLimit;
//设备识别码
@property (nonatomic,strong) NSString *signature;
//登录状态
@property (nonatomic) BOOL transactionLogin;//登录状态
//登录账号
@property (strong,nonatomic) NSString *tradeLoginID;
//登录密码
@property (strong,nonatomic) NSString *tradeLoginPwd;


//alert是否已经显示
@property (nonatomic) BOOL alertIsShow;
//程序在前台
@property (nonatomic) BOOL     appIsWake;
@property(nonatomic)BOOL isNetworkReachability;//网络连接状态
/*****************TabBar*********************/
@property(nonatomic)NSInteger currentTabBarIndex;//当前选中下标
@property(nonatomic)NSInteger oldTabBarIndex;//之前选中下标
//首页大类标题及ID
@property(nonatomic, strong)NSString *bigClassTitle;
@property(nonatomic, strong)NSString *bigClassID;
@property(nonatomic)BOOL isHomeToStore;// 从首页进便利店 为 YES 其他为NO
+ (instancetype)sharedManager;
//清空登录信息
- (void)clearUserInfoData;

/*    对个人信息进行处理         */
- (void)updataUserInfoWithModel:(UserInfoModel*)model;
- (UserInfoModel*)getUserInfo;

/*  对小区和商店信息处理        */

- (void)updataXiaoquAndStoreInfoWithModel:(XiaoQuAndStoreInfoModel*)model;
- (XiaoQuAndStoreInfoModel*)getxiaoQuAndStoreInfo;
/*  对服务端返回的楼层信息重新组装        */
- (NSArray*)modifiedServerXiaoQuInfoDatasoruce:(NSArray*)datasource;
/* 加载框视图 */
- (void)showAlertWithMsg:(NSString*)str delegate:(id)delegate;
- (void)showHUDWithMsg:(NSString*)str inView:(id)View;
- (void)showTRHUDWithMsg:(NSString*)str inView:(id)View;
- (void)showTextHUDWithMsg:(NSString*)str inView:(id)View;
- (void)hideHUD;
- (void)hideAllHUDFormeView:(id)View;
- (void)hideHUDFromeView:(UIView*)view;
/* 字体大小 */
-(CGFloat)setDifferenceScreenFontSizeWithFontOfSize:(CGFloat)size;
/*不同屏幕 视图缩放比例  */
-(CGFloat)setCurrentViewHightWithBaseViewHight:(CGFloat)hight;
/*lable宽度  */
-(CGSize)setLabelWidthWithNSString:(NSString*)lableText Font:(CGFloat)font;
@end
