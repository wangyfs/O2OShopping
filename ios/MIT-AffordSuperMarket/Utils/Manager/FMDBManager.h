//
//  FMDBManager.h
//  sqlite
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FMDBtableTableName) {
    kShopCartTable = 0,//关注页面
    kServiceTable //浏览历史
};

@interface FMDBManager : NSObject

+ (instancetype)sharedManager;
//创建库
-(void)initWithCreateSqlite;
//删除全部
-(void)deleteAll:(NSString *)tableName;
#pragma mark--历史
//创建历史表
-(void)initWithHistoryCreateTable;
//插入
-(void)HistoryInsertWithshopID:(NSString *)shopID goodID:(NSString *)goodID Picture:(NSData *)picture name:(NSString *)name price:(NSString *)price oldprice:(NSString *)oldprice serviceshopID:(NSString *)serviceshopID serciceshopdetail:(NSString *)serciceshopdetail sercvicenum:(NSString *)sercvicenum isProprietary:(NSString *)isProprietary;
//查询
-(id)HistoryQueryDataWithShopID:(NSString *)ShopID;
//查询单个数据
-(id)HistoryQueryDataWithShopID:(NSString *)ShopID goodID:(NSString *)goodID serviceshopID:(NSString *)serviceshopID;
//修改
-(void)HistorymodifyDataWithShopID:(NSString *)shopID goodID:(NSString *)goodID name:(NSString *)name price:(NSString *)price oldprice:(NSString *)oldprice serviceshopID:(NSString *)serviceshopID serciceshopdetail:(NSString *)serciceshopdetail sercvicenum:(NSString *)sercvicenum isProprietary:(NSString *)isProprietary;

#pragma mark --购物车
//创建购物车表
-(void)initWithShoppingCartTable;
//插入
-(void)InsertWithshopID:(NSString *)shopID goodID:(NSString *)goodID name:(NSString *)name Picture:(NSData *)picture type:(NSString *)type price:(NSString *)price oldprice:(NSString *)oldprice buynum:(NSString *)buynum ischoose:(NSString *)ischoose isProprietary:(NSString *)isProprietary;
//查询当前表 所有数据
-(id)QueryDataWithshopID:(NSString *)shopID;
// 查非自营商品
-(NSMutableArray *)QueryIsNoProprietaryDataWithShopID:(NSString *)shopID;
//查询自营 商品
-(NSMutableArray *)QueryDataWithIsProprietary:(NSString *)isProprietary;
//查询所有选中数据
-(NSMutableArray *)QueryIsChoosedDataWithShopID:(NSString *)ShopID;
//根据便利店选中商品
-(NSMutableArray *)QueryIsChoosedDataInShopWithShopID:(NSString *)ShopID;
//自营选中商品
-(NSMutableArray *)QueryisProprietaryIsChoosedData;
//查询单个数据
-(id)QueryDataWithShopID:(NSString *)ShopID goodID:(NSString *)goodID;
//更新数据
-(void)modifyShopCarDataWithshopID:(NSString *)shopID goodID:(NSString *)goodID name:(NSString *)name type:(NSString *)type price:(NSString *)price oldprice:(NSString *)oldprice;
//更新数量
-(void)modifyDataWithshopID:(NSString *)shopID goodID:(NSString *)goodID buynum:(NSString *)buynum;
//自营更新数量
-(void)modifyDataWithgoodID:(NSString *)goodID buynum:(NSString *)buynum isProprietary:(NSString *)isProprietary;
//更新选中状态
-(void)modifyDataWithshopID:(NSString *)shopID goodID:(NSString *)goodID ischoose:(NSString *)ischoose;
//自营更新选中状态
-(void)modifyDataWithgoodID:(NSString *)goodID ischoose:(NSString *)ischoose isProprietary:(NSString *)isProprietary;
//删除单条数据
-(void)deleteOneDataWithShopID:(NSString *)shopID goodID:(NSString *)goodID;
//自营单条删除
-(void)deleteOneDataWithgoodID:(NSString *)goodID;
@end
