///Users/apple/Desktop/WorkSpace/20160118/MIT-AffordSuperMarket.xcodeproj
//  Global.h
//  MIT_itrade
//
//  Created by wanc on 14-4-8.
//  Copyright (c) 2014年 ___ZhengDaXinXi___. All rights reserved.
//

/*********************************
 文件名: Global.h
 功能描述: 全局用到的宏定义
 创建人: GT
 修改日期: 2015.10.30
 *********************************/
#import "ManagerGlobeUntil.h"

//URL
//#define baseServerUrl @"http://192.168.1.111:8080/dashihui-api/" //服务器地址
//#define baseServerUrl @"http://www.91dashihui.com/dashihui-api/" //旧外网服务器地址
#define baseServerUrl @"http://api.91dashihui.com/" //外网服务器地址

//#define baseServerUrl @"http://192.168.1.100:8080/" //吕恒飞 服务器地址
//
//#define baseServerUrl @"http://192.168.1.113:8080/"// 杜哥 服务器地址

//#define baseImageUrl @"http://192.168.1.111:8082" //图片拼接地址

#define baseImageUrl @"http://static.91dashihui.com" //图片拼接地址
//#define baseImageUrl @"http://114.112.52.12:8082" //旧外网图片拼接地址


//必须配置
#define serverIpAddress @"192.168.1.111"
// 服务端版本号
#define serviceVersionNumber @"1.3.3"
//大实惠商店ID
#define kDaShiHuiStoreID 1

//大实惠便利店名字
#define kDaShiHuiStoreName @"大实惠直营"
//百度定位服务端key 和编号 关键字 范围
#define BaiDuDingWeiServerKey @"S2kGRESvR7jHYhPI8XEfZylv"
#define BaiDuDingWeiTableID 125217
#define BaiDuDingWeiKeyword @"dashihui"
#define BaiDuDingWeiRadius 900000


//ios6和ios7的适配
#define IsIOS9 ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=9)
#define IsIOS8 ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=8)
#define IsIOS7 ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)
#define IsIOS6 ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=6)
//手机型号

#define kDevice_Is_iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define kDevice_Is_iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define kDevice_Is_iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define kDevice_Is_iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1080, 1920), [[UIScreen mainScreen] currentMode].size) : NO)

#define kDevice_iPhone6_Width 375
#define kDevice_iPhone6Plus_Width 414
//屏幕尺寸
#define VIEW_WIDTH [UIScreen mainScreen].bounds.size.width//屏幕宽度
#define VIEW_HEIGHT [UIScreen mainScreen].bounds.size.height//屏幕高度
//常用颜色
#define WHITE_HEX_COLOR @"c1c1c1"
#define RED_HEX_COLOR @"#ff4746"
#define YELLOW_HEX_COLOR @"ffff00"
#define GREEN_HEX_COLOR @"77d850"
#define SELECT_HEX_COLOR @"0f2439"
#define GRAY_HEX_COLOR @"#888888"
#define BG_HEX_COLOR @"#232527"

//通知key
#define TR_LOGIN_SUCCEED @"LOGIN_SUCCEED"
#define TR_LOGIN_QUIT @"LOGIN_QUIT"
#define TR_OUTOFLIMITTIME @"OUTOFLIMITTIME"

#define DINGWEISUCCESS @"DingWeiSuccess" //定位成功
#define DINGWEIFAILED @"dingWeiFailed"//定位失败
#define REACHABILITY_STATUS @"Reachability_Status"//网络状态通知
#define End_IntroductionPage @"End_IntroductionPage"//点击立即体验通知

#define PRODUCTCOUNTCHANGE @"product_count_change"// 产品数量改变发送通知

#define WEIXINPAYRESULT @"weixin_pay_result"//微信支付返回通知

//NSUserDefaults key
#define XIAOQU_AND_STORE__INFO @"xiaoqu_and_store_info"//小区和商店信息

#define User_INFO @"user_info"//用户信息
#define Phone_Signature @"Phone_Signature"//手机签名key

#define App_Account @"app_account" //应用账号

//账号密码保存钥匙串的key
#define APP_ISAUTOLOGIN @"APP_ISAUTOLOGIN"//判断app是否自动登录
#define  KEYCHAIN_USERNAME_KEY  @"com.dashihui.app.username"

#define  KEYCHAIN_SIGNATURE_KEY  @"com.zd.app.signature"
#define  KEYCHAIN_UUID_KEY  @"com.zd.app.uuid"
//超时时间设置
#pragma mark -  transactionLoginTimeLimit
#define MAX_TRANSACTIONLOGINTIME_KEY 600


//搜索数据存储key
#define ksearchHistory  @"KSEARCHHISTORY"
#define ksearchKeyWord @"ksearchKeyWord"
#define ksearchKeyWordId @"ksearchKeyWordId"
#define khideSearchKeyboard @"khideSearchKeyboard"

//分页请求数据条数
#define kRequestLimit 30//交易分页

//分享 对应的key 和 ID
//新浪微博
#define kAppKey         @"2742708329"
#define kRedirectURI    @"http://www.sina.com"

//QQ
#define __TencentDemoAppid_  @"1104682074"
#define PreDefM_APPID @"1104682074"
//订单正常
#define KOrderStateNormal @"1"
//订单完成
#define KOrderStateFinish @"2"
//订单取消
#define KOrderStateCancel @"3"
//订单删除
#define KOrderStateDelete @"4"
//订单过期
#define KOrderStateBeOverdue @"5"

//订单付款类型
#define KOrderPayTypeIsPayPal @"1"  //在线支付
#define KOrderPayTypeIsCOD @"2"  //货到付款
//订单支付状态
#define KOrderPayStateIsNo @"1" //待支付
#define KOrderPayStateIsYes @"2" //已支付
//订单送货方式
#define KOrderDeliverTypeIsDelivery @"1" //送货上门
#define KOrderDeliverTypeIsSelfUp @"2"   //自取
//订单送货状态
#define KOrderDeliverStateIsNo @"1"  //待发货
#define KOrderDeliverStateIsYes @"2"  //已发货
//打包状态
#define KOrderPackState1 @"1"//1.未接单，
#define KOrderPackState2 @"2"//2：已接单
#define KOrderPackState3 @"3"//3：打包中，
#define KOrderPackState4 @"4"//4：打包完成
//评价状态
#define KOrderEvalStateIsNo @"0"// 未评价
#define KOrderEvalStateIsYse @"1"//已评价
//字体缩放比例
#define FontSize(fontSize) [[ManagerGlobeUntil sharedManager] setDifferenceScreenFontSizeWithFontOfSize:fontSize]
//视图高度缩放比例
#define HightScalar(baseHight) [[ManagerGlobeUntil sharedManager]setCurrentViewHightWithBaseViewHight:baseHight]
