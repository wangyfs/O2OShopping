package com.dashihui.afford.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;

import com.lidroid.xutils.util.LogUtils;

/**
 * Created by Administrator on 2015/12/19.
 */
public class WgtGridViewServer extends GridView {


    public WgtGridViewServer(Context context) {
        super(context);
    }

    public WgtGridViewServer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WgtGridViewServer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //控制gridview的item不上下滑动，一次全部显示
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        LogUtils.e("WgtGridViewServer=========widthMeasureSpec============>" + widthMeasureSpec);
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);

        // 创建测量参数
        int cellWidthSpec = MeasureSpec.makeMeasureSpec(widthMeasureSpec, MeasureSpec.AT_MOST);
        int cellHeightSpec = MeasureSpec.makeMeasureSpec(heightMeasureSpec, MeasureSpec.AT_MOST);
        LogUtils.e("WgtGridViewServer=========cellWidthSpec============>" + cellWidthSpec);
        LogUtils.e("WgtGridViewServer=========cellHeightSpec============>" + cellHeightSpec);

       int count = getChildCount();
        LogUtils.e("WgtGridViewServer=========count============>" + count);
        boolean istrue = false;

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
                if (child != null) {
                    LogUtils.e("WgtGridViewServer=========childView============>" + child.getMeasuredWidth());
                    LogUtils.e("WgtGridViewServer=========childView============>" + cellWidthSpec);
                    child.measure(child.getMeasuredWidth(), cellHeightSpec);
                }

            }

            // 设置容器控件所占区域大小
            // 注意setMeasuredDimension和resolveSize的用法
        setMeasuredDimension(resolveSize(widthMeasureSpec * count, widthMeasureSpec),
                resolveSize(heightMeasureSpec * count, heightMeasureSpec));
            setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);

            // 不需要调用父类的方法
            // super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            //处理最后一个子View的情况

            super.onMeasure(widthMeasureSpec, expandSpec);
        }


    }







